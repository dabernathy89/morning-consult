<?php
/**
 * Template name: Client Landing Page 1
 *
 * A template to use for client landing pages. Areas for banner image, main content, and three columns of sub-content
 *
 * @package Morning Consult 2015
 */

get_template_part('inc/partials/header','opening'); ?>

    <div class="page client-landing-1">

        <?php if (has_post_thumbnail()) : ?>
            <header class="banner">
                <?php the_post_thumbnail('full'); ?>
            </header>
        <?php endif; ?>

        <div class="container">

            <?php while ( have_posts() ) : the_post(); ?>

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <div class="entry-content">
                            <?php the_content(); ?>
                        </div><!-- .entry-content -->
                    </article><!-- #post-## -->

                    <?php if( have_rows('client_landing_1_columns') ): ?>
                        <div class="row columns">
                            <?php while ( have_rows('client_landing_1_columns') ) : the_row(); ?>
                                <div class="col-sm-4">
                                    <div class="inner">
                                        <?php the_sub_field('content'); ?>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    <?php endif ?>

                </div>
            </div>

            <?php endwhile; // end of the loop. ?>

        </div>
    </div><!-- #primary -->

<?php wp_footer(); ?>

</body>
</html>
