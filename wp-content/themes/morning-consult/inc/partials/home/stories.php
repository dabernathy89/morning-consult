<?php

global $paged;

$temp_paged = $paged;
$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;

$cats_to_exclude = get_field('home_categories_to_exclude');

$home_stories_args = array (
    'post_type'              => array('post','mc_opinion'),
    'posts_per_page'         => 15,
    'paged'                  => $paged,
    'post__not_in'           => array($home_featured_post_id)
);

if (is_array($cats_to_exclude) && !empty($cats_to_exclude)) {
    $home_stories_args['category__not_in'] = $cats_to_exclude;
}

$home_stories = new WP_Query($home_stories_args);

if( $home_stories->have_posts() ): ?>

  <div class="articles load-more-wrapper" id="home-articles-wrapper">

  <?php $featured_img_left = false; ?>

  <?php $ad_counter = 1; ?>

  <?php while( $home_stories->have_posts() ): $home_stories->the_post(); ?>

    <?php if (get_post_type() === "post") {
      $featured_img_left = !$featured_img_left;
    }?>

    <?php
      if (!$featured_img_left && get_post_type() !== 'mc_opinion') :
          $post_layout = "content-title";
      elseif ($featured_img_left || get_post_type() === 'mc_opinion') :
          $post_layout = "title-content";
      endif;
    ?>
    <?php
      if($home_stories->current_post % 2 === 0 && $ad_counter < 6) :

				echo "<div id='ad-story-${ad_counter}' data-ad-type='morningconsult_newsticker_s${ad_counter}_mobile_Control' data-ad-lazy='morningconsult_newsticker_s${ad_counter}_mobile_Control' data-ad-display='mobile' class='ad-container ad-story'></div>";

      $ad_counter = ($ad_counter + 1);
      endif;
    ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class("excerpt default clearfix layout-" . $post_layout); ?> >
        <div class="content-wrap left-of-title">
          <div class="entry-content">
						<?php echo mc_custom_excerpt( get_the_ID(), 30 ); ?>
          </div>
          <div class="category-tags clearfix">
              <?php morning_consult_category_links_square(); ?>
          </div>
        </div>

        <?php if (get_post_type() === "post" && has_post_thumbnail() ) : ?>
          <?php $thumb = morning_consult_get_featured_image_by_size('mc-morning-brief'); ?>
            <div class="thumbnail-title-wrap" data-bg="<?php echo $thumb; ?>">
          <?php else : ?>
            <div class="thumbnail-title-wrap">
        <?php endif; ?>
            <header class="entry-header">
              <?php if (get_post_type() === 'mc_opinion') : ?>
                <span class="opinion">Opinion</span>
              <?php endif; ?>
              <?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '">', '</a></h3>' ); ?>
              <span class="author vcard">
              <em>
                <?php $all_authors = get_coauthors(); ?>
                By <?php foreach ($all_authors as $authorkey => $author) : ?>
                  <?php include get_stylesheet_directory() . "/inc/partials/single/meta_author.php"; ?>
                <?php endforeach; ?>
              </em>
              </span>
            </header>
          </div>

        <div class="content-wrap right-of-title">
          <div class="entry-content">
					<?php echo mc_custom_excerpt( get_the_ID(), 30 ); ?>
          </div>
          <div class="category-tags">
            <?php morning_consult_category_links_square(); ?>
          </div>
        </div>

      </article>

    <?php endwhile; ?>

    <a class="load-more" href="<?php echo next_posts( $home_stories->max_num_pages, false ); ?>">Load More <i class="fa fa-chevron-down"></i></a>

    </div>

<?php endif; ?>
<?php $paged = $temp_paged; ?>
<?php wp_reset_postdata(); ?>
