<?php /**
* Template Name: Trump/Clinton MRP Election
* @package Morning Consult 2016
*/
?>

<?php get_header(); ?>

<script type="text/javascript" src="<?php echo get_template_directory_uri() . '/js/highmaps/js/highmaps.src.js'; ?>"></script>
<script src="https://code.highcharts.com/mapdata/countries/us/us-all.js"></script>
<!-- <script type="text/javascript" src="<?php echo get_template_directory_uri() . '/js/fifty-states.js?v=20160412'; ?>"></script> -->
<script src="<?php echo get_template_directory_uri() .'/js/mrp-election.js'; ?>"></script>

<?php
$tablepress_tables = json_decode( get_option( 'tablepress_tables', array() ), true );
$table_id = get_field('data_source');
$table_data = get_post_field('post_content', $tablepress_tables['table_post'][$table_id], 'raw');

$formatted_table_data = mc_mrp_presidential_candidate_data($table_data);

wp_localize_script('morning-consult-all-js', 'mc_mrp_presidential_candidate_data', $formatted_table_data);

?>

<?php while(have_posts()) : the_post(); ?>

<container class="container">
	<row class="row">
		<main id="main-col" class="col-xs-12">
			<h1 class="page-title"><?php the_title(); ?></h1>
			<row class="row">
				<div class="entry-meta">
					<span class="author vcard">
						<?php the_author(); ?>
					</span>&nbsp;&nbsp;<span class="separator">|</span>&nbsp;&nbsp;
					<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
						<time class="entry-date published updated" datetime="<?php echo get_the_date( 'c' ); ?>">
							<?php the_date(); ?>
						</time>
					</a>
				</div><!-- .entry-meta -->
				<div class="main-body col-xs-12 col-md-8"><?php the_content(); ?></div>
				<div class="img-body col-xs-12 col-md-4"><img src="https://morningconsult.com/wp-content/uploads/2016/07/MRPsidebarupdated.png" alt=""></div>
			</row>
			<div class="electoral-votes-container">
				<div class="midpoint-label">270 electoral votes to win</div>
				<div class="electoral-votes-bar clearfix" id="electoral-votes-bar">
					<div class="votes votes-d">
						<span class="totals totals-d"></span>
					</div>
					<div class="votes votes-r">
						<span class="totals totals-r"></span>
					</div>
					<div class="midpoint"></div>
				</div>
				<div id="mrp-legend" class="hidden-xs">
					<?php
						$fav_colors = array(
							'#0c4a6b' => 'Clinton',
							'#8fbadb' => 'Lean Clinton',
							'#e9d971' => 'Toss-up',
							'#d99c86' => 'Lean Trump',
							'#d33f2f' => 'Trump',

						);
						$fav_legend = create_html_legend($fav_colors);
						echo $fav_legend;
					?>
				</div>
			</div>
			<div id="map-container"></div>

			<p class="table-label visible-xs">&laquo; Scroll to View More Columns &raquo;</p>
			<div class="table-container">
				<?php tablepress_print_table('id='.$table_id) ?>
			</div>
		</main>

	</row>
</container>


<?php endwhile; ?>

<?php get_footer(); ?>
