var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sass = require( 'gulp-sass' );
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var browserify = require('browserify');
var fs = require('fs');
var realFavicon = require('gulp-real-favicon');

// File where the favicon markups are stored
var FAVICON_DATA_FILE = 'faviconData.json';

var paths = {
  "scripts": [
    "js/bootstrap.js",
    "js/jquery.backstretch.min.js",
    "js/velocity.js",
    "js/velocity.ui.js",
    "js/jquery.fitvids.js",
    "js/slick.js",
    "js/main.js"
  ]
};

// Watch Files For Changes
gulp.task('watch', function() {

  gulp.watch(
    paths['scripts'],
    ['scripts-dev']
  );

  gulp.watch(
    ['sass/*.scss','sass/**/*.scss','sass/**/**/*.scss'],
    ['sass-dev']
  );
});

gulp.task('scripts-dev', function() {
  return gulp.src(paths['scripts'])
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest('js/build'));
});

gulp.task('watch-ads', function() {
  gulp.watch(
    ['./js/ads/**/*.js'],
    ['concat-ads', 'minify-ads']
  );
});

gulp.task('concat-ads', function() {
  return browserify('./js/ads/ad-main.js')
    .transform('babelify', { presets: ['es2015'] })
    .bundle()
    .pipe(fs.createWriteStream('./js/ads/min/mc-ads.js'));
});

gulp.task('minify-ads', ['concat-ads'], function(){
  return gulp.src('./js/ads/min/mc-ads.js')
  .pipe(uglify())
  .pipe(concat('mc-ads.min.js'))
  .pipe(gulp.dest('./js/ads/min'));
});

gulp.task('scripts', function() {
  return gulp.src(paths.scripts)
    .pipe(uglify())
    .pipe(concat('scripts.min.js'))
    .pipe(gulp.dest('js/build'));
});

gulp.task( 'sass-dev', function(){
  console.log('Compiling SASS (dev)');
  return gulp.src('sass/style.scss')
    .pipe(sass({
      outputStyle: 'expanded',
      precision: 8,
    }).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(rename({
      suffix: "-dev"
    }))
    .pipe(gulp.dest(''));
});

gulp.task( 'sass', function(){
  console.log('Compiling SASS (production)');
  return gulp.src('sass/style.scss')
    .pipe(sass({
      outputStyle: 'compressed',
      precision: 8,
    }).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(gulp.dest(''));
});

// Generate the icons. This task takes a few seconds to complete.
// You should run it at least once to create the icons. Then,
// you should run it whenever RealFaviconGenerator updates its
// package (see the check-for-favicon-update task below).
// NOTE - this iconsPath must be relative to webroot
// wp-content/themes/morning-consult/
gulp.task('generate-favicon', function(done) {
	realFavicon.generateFavicon({
		masterPicture: 'img/favicon/MC-favicon-master.png',
		dest: 'img/favicon/',
		iconsPath: 'wp-content/themes/morning-consult/img/favicon/',
		design: {
			ios: {
				pictureAspect: 'noChange',
				assets: {
					ios6AndPriorIcons: false,
					ios7AndLaterIcons: false,
					precomposedIcons: false,
					declareOnlyDefaultIcon: true
				}
			},
			desktopBrowser: {},
			windows: {
				pictureAspect: 'noChange',
				backgroundColor: '#da532c',
				onConflict: 'override',
				assets: {
					windows80Ie10Tile: false,
					windows10Ie11EdgeTiles: {
						small: false,
						medium: true,
						big: false,
						rectangle: false
					}
				}
			},
			androidChrome: {
				pictureAspect: 'noChange',
				themeColor: '#ffffff',
				manifest: {
					name: 'Morning Consult',
					display: 'standalone',
					orientation: 'notSet',
					onConflict: 'override',
					declared: true
				},
				assets: {
					legacyIcon: false,
					lowResolutionIcons: false
				}
			},
			safariPinnedTab: {
				pictureAspect: 'silhouette',
				themeColor: '#5bbad5'
			}
		},
		settings: {
			scalingAlgorithm: 'Mitchell',
			errorOnImageTooSmall: false
		},
		markupFile: FAVICON_DATA_FILE
	}, function() {
		done();
	});
});

// Inject the favicon markups in your HTML pages. You should run
// this task whenever you modify a page. You can keep this task
// as is or refactor your existing HTML pipeline.
// ** NOTE - doesnt work properly because file already exists, also breaks the formatting.
// So you have to write to a separate file, then manually merge all the meta tags.
gulp.task('inject-favicon-markups', function() {
	gulp.src([ 'inc/partials/header-opening.php' ])
		.pipe(realFavicon.injectFaviconMarkups(JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).favicon.html_code))
		.pipe(gulp.dest( 'inc/partials/header-opening-v2.php' ));
});

// Check for updates on RealFaviconGenerator (think: Apple has just
// released a new Touch icon along with the latest version of iOS).
// Run this task from time to time. Ideally, make it part of your
// continuous integration system.
gulp.task('check-for-favicon-update', function(done) {
	var currentVersion = JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).version;
	realFavicon.checkForUpdates(currentVersion, function(err) {
		if (err) {
			throw err;
		}
	});
});


gulp.task('default', ['sass-dev','sass', 'scripts', 'scripts-dev', 'minify-ads']);
