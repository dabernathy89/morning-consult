<?php
/**
 * Template name: Senate Approval Ratings
 *
 * Q1 2016 Report on Senate Approval ratings
 *
 * @package Morning Consult 2015
 */

get_header(); ?>

<?php #print_r(is_page_template('senate-rankings.php')); ?>
<script src="<?php echo get_template_directory_uri() . '/js/handlebars/handlebars-v4.0.5.js'; ?>"></script>
<script src="<?php echo get_template_directory_uri() . '/js/senate-rankings.js'; ?>"></script>
<?php while(have_posts()) : the_post(); ?>

<div class="container">
  <div class="row">
    <div class="section-content-all col-md-8 col-xs-12">
      <div class="row">

      <section class="approval-intro col-xs-12">
        <h1 class="page-title"><?php the_title(); ?></h1>
        <?php the_field('intro_paragraph'); ?>
      </section>

      <?php get_template_part('content', 'senate-rankings'); ?>

      </div>
    </div>
    <div class="news-ticker col-md-4 col-xs-12">
      <?php get_template_part( 'inc/partials/alerts' ); ?>
    </div>

  </div>
</div>

<?php endwhile; ?>

<?php get_footer(); ?>
