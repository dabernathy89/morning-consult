<?php
/**
 * The template for displaying a race rankings table.
 *
 * @package Morning Consult 2015
 */

get_header(); ?>

    <div class="page rankings-table">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">

                    <?php while ( have_posts() ) : the_post(); ?>

                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <header class="entry-header">
                                <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                            </header><!-- .entry-header -->

                            <div class="entry-content">
                                <?php echo do_shortcode('[mc_rankings_table id="' . get_the_ID() . '"]'); ?>
                            </div><!-- .entry-content -->
                        </article><!-- #post-## -->

                    <?php endwhile; // end of the loop. ?>

                </div>
            </div>
        </div>
    </div><!-- #primary -->

<?php get_footer(); ?>
