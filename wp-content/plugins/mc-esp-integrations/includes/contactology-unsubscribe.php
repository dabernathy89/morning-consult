<?php

class MCUnsubscribe {

    protected $api_key;

    protected $contactology;

    protected $mc_contactology_addon;

    protected $unsubscribes = array();

    protected $can_do_unsubs = true;

    public function __construct($mc_contactology_addon) {
        $this->mc_contactology_addon = $mc_contactology_addon;
        add_action( 'gform_after_submission', array($this,'handle_unsubscribes'), 10, 2 );
    }

    public function handle_unsubscribes($entry, $form) {
        $campaign = false;

        if (strpos($form['cssClass'], 'unsubscribe') === false) {
            return;
        }

        $this->api_key = $this->mc_contactology_addon->get_api_key();

        if (!$this->api_key) {
            return;
        } else {
            $this->contactology = new MCESPI_Campaigner_Api($this->api_key);
        }

        foreach ($form['fields'] as $key => $value) {
            if (strpos($value->cssClass, 'lists') !== false) {
                $lists_field = $value;
            }

            if ($value->label === "Campaign") {
                $campaign = $entry[$value->id];
            }

            if ($value->type === "email") {
                $email = $entry[$value->id];
            }
        }

        if (!isset($lists_field) || !isset($email)) {
            return;
        }

        foreach ($lists_field['inputs'] as $input_key => $input_value) {
            if (array_key_exists($input_value['id'], $entry) && $entry[$input_value['id']] === "all") {
                $this->do_suppress(array('email' => $email, 'campaign' => $campaign));
                $this->can_do_unsubs = false;
                break;
            }

            if (array_key_exists($input_value['id'], $entry) && $entry[$input_value['id']] !== "") {
                $this->unsubscribes[] = array('email' => $email, 'list' => $entry[$input_value['id']], 'campaign' => $campaign);
            }
        }

        if (!empty($this->unsubscribes) && $this->can_do_unsubs) {
            foreach ($this->unsubscribes as $unsubscribe) {
                $this->do_unsubscribe($unsubscribe);
            }
        }
    }

    protected function do_unsubscribe($unsub) {
        if (!empty($unsub['campaign'])) {
            $optionalParameters = array('campaignId' => $unsub['campaign']);
        } else {
            $optionalParameters = array();
        }

        try {
            $response = $this->contactology->List_Unsubscribe(
                (int)$unsub['list'],
                $unsub['email'],
                $optionalParameters
            );
        } catch (Exception $e) {
            // can't echo out here because it's too early.
            // echo '<p>Error: ',  $e->getMessage(), "</p>";
        }
    }

    protected function do_suppress($suppress) {
        if (!empty($suppress['campaign'])) {
            $optionalParameters = array('campaignId' => $suppress['campaign']);
        } else {
            $optionalParameters = array();
        }

        $response = $this->contactology->Contact_Suppress(
            $suppress['email'],
            $optionalParameters
        );
    }

}