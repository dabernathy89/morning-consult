(function(w, d, $) {
	var videoIDs = w.mc_youtube_video_ids;
	console.log(videoIDs);
	var tag = document.createElement('script');
	tag.src = "https://www.youtube.com/iframe_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

	// YT api fires this function when ready
	w.onYouTubeIframeAPIReady = function() {
		setupAllPlayers();
	}

	function setupAllPlayers() {
		w.mcplayers = {};
		w.mcplayers.video1 = loadSinglePlayer(videoIDs.video1, 'video1');
		w.mcplayers.video2 = loadSinglePlayer(videoIDs.video2, 'video2');
	}

	function setupAnimation(event) {
		// var wat = $('#' + event.target.a.nextElementSibling.id);
	}

	function startAnimation(target) {
		var playerID = target.id,
		$ticker = $('#' + target.nextElementSibling.children[1].id),
		duration = w.mcplayers[playerID].getDuration();

		w.mcplayers[playerID + 'Interval'] = w.setInterval(function() {
			var elapsedTime = w.mcplayers[playerID].getCurrentTime();
			var pct = (elapsedTime / duration)*100;
			$ticker.width(pct.toFixed(2).toString() + '%');
		}, 500);

	}

	function pauseAnimation(target) {
		var playerID = target.id;
		w.clearInterval(w.mcplayers[playerID + 'Interval']);
	}

	function loadSinglePlayer(videoID, div) {
		var player = new YT.Player(div, {
			height: '400',
			width: '934',
			videoId: videoID,
			playerVars: {
				showinfo: 0,
				rel: 0,
				modestBranding: 1
			},
			events: {
				onReady: setupAnimation,
				onStateChange : handleStateChange
			}
		});
		return player;
	}

	function handleStateChange(event) {
		var state = event.data;
		//video playing
		if(state === 1) {
			startAnimation(event.target.a);
		}
		//video paused
		else if(state === 2) {
			pauseAnimation(event.target.a);
		}
		//video cued
		// else if(state === 5) {
		// 	console.log('video ready');
		// }
		//video ended
		else if(state === 0) {
			pauseAnimation(event.target.a);

		}
	}

})(window, document, jQuery);
