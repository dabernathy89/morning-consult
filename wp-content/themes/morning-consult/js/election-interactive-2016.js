(function(w, d, $) {
	var electionGlobals = {};


	function registerPartials() {
		$('[id^="partial"]').each(function() {
			Handlebars.registerPartial(this.id, $(this).html());
		})
	}

	function registerHelpers() {
		Handlebars.registerHelper('removeAge', function(age) {
			return age.split(" ")[1];
		});
	}

	function initGlobalChartConfig() {
		Chart.defaults.global.defaultFontColor = '#777777';
		Chart.defaults.global.defaultFontFamily = 'proxima-nova';
		Chart.defaults.global.tooltips.enabled = false;
		Chart.defaults.global.maintainAspectRatio = false;
		Chart.defaults.global.legend.display = false;
		Chart.defaults.global.hover.mode = false;
		Chart.defaults.global.animation.duration = 800;
	}

	function parseRawData(data) {
		// real list
		electionGlobals.stateList = [
			'Arizona', 
			'Colorado', 
			'Florida', 
			'Georgia',
			'Michigan',
			'North Carolina',
			'Ohio', 
			'Pennsylvania'
			];
		electionGlobals.genderNational = {};
		electionGlobals.genderState = {};
		electionGlobals.partyNational = {};
		electionGlobals.partyState = {};
		electionGlobals.ageNational = {};
		electionGlobals.ageState = {};
		electionGlobals.stateList.forEach(function(cv) {
			electionGlobals.genderState[cv] = {};
			electionGlobals.partyState[cv] = {};
			electionGlobals.ageState[cv] = {};
		});

		data.forEach(function(cv, i) {
			//gender national
			if(cv.Sample === 'Overall' && cv.Demographic === 'demGender') {
				if(cv.Value === 'Male') {
					electionGlobals.genderNational.male = {
						'title': cv.Value,
						'clinton': cv.Clinton,
						'trump' : cv.Trump,
						'dkno' : cv.Unknown
					};
				}
				else if (cv.Value === 'Female') {
					electionGlobals.genderNational.female = {
						'title': cv.Value,
						'clinton': cv.Clinton,
						'trump' : cv.Trump,
						'dkno' : cv.Unknown
					};
				}
			}
			
			//gender by state
			if(electionGlobals.stateList.indexOf(cv.Sample) >= 0 && cv.Demographic === 'demGender') {
				if(cv.Value === 'Male') {
					electionGlobals.genderState[cv.Sample].male = {
						'clinton': cv.Clinton,
						'trump': cv.Trump,
						'state': cv.Sample,
						'dkno': cv.Unknown
					}
				}
				if(cv.Value === 'Female') {
					electionGlobals.genderState[cv.Sample].female = {
					'clinton': cv.Clinton,
					'trump': cv.Trump,
					'state': cv.Sample,
					'dkno': cv.Unknown
					}
				} 
			}

			//party national
			if(cv.Sample === 'Overall' && cv.Demographic === 'xpid3') {
				if(cv.Value === 'PID: Dem (no lean)') {
					electionGlobals.partyNational.dem = {
						'clinton' : cv.Clinton,
						'trump': cv.Trump,
						'dkno' : cv.Unknown,
						'title': cv.Value
					}
				}
				if(cv.Value === 'PID: Rep (no lean)') {
					electionGlobals.partyNational.rep = {
						'clinton' : cv.Clinton,
						'trump': cv.Trump,
						'dkno' : cv.Unknown,
						'title': cv.Value
					}
				}
				if(cv.Value === 'PID: Ind (no lean)') {
					electionGlobals.partyNational.ind = {
						'clinton' : cv.Clinton,
						'trump': cv.Trump,
						'dkno' : cv.Unknown,
						'title': cv.Value
					}
				}
			}

			//party state
			if(electionGlobals.stateList.indexOf(cv.Sample) >= 0 && cv.Demographic === 'xpid3') {
				if(cv.Value === 'PID: Dem (no lean)') {
					electionGlobals.partyState[cv.Sample].dem = {
						'clinton' : cv.Clinton,
						'trump': cv.Trump,
						'dkno' : cv.Unknown,
						'title': cv.Value
					}
				}
				if(cv.Value === 'PID: Rep (no lean)') {
					electionGlobals.partyState[cv.Sample].rep = {
						'clinton' : cv.Clinton,
						'trump': cv.Trump,
						'dkno' : cv.Unknown,
						'title': cv.Value
					}
				}
				if(cv.Value === 'PID: Ind (no lean)') {
					electionGlobals.partyState[cv.Sample].ind = {
						'clinton' : cv.Clinton,
						'trump': cv.Trump,
						'dkno' : cv.Unknown,
						'title': cv.Value
					}
				}
			}

			//age national 
			if(cv.Sample === 'Overall' && cv.Demographic === 'age') {
				if(cv.Value === 'Age: 18-34') {
					electionGlobals.ageNational[cv.Value] = {
						'clinton' : cv.Clinton,
						'trump': cv.Trump,
						'dkno' : cv.Unknown,
						'title': cv.Value
					}
				}
				if(cv.Value === 'Age: 35-44') {
					electionGlobals.ageNational[cv.Value] = {
						'clinton': cv.Clinton,
						'trump': cv.Trump,
						'dkno' : cv.Unknown,
						'title': cv.Value
					}
				}
				if(cv.Value === 'Age: 45-64') {
					electionGlobals.ageNational[cv.Value] = {
						'clinton': cv.Clinton,
						'trump': cv.Trump,
						'dkno' : cv.Unknown,
						'title': cv.Value
					}
				}

				if(cv.Value === 'Age: 65+') {
					electionGlobals.ageNational[cv.Value] = {
						'clinton': cv.Clinton,
						'trump': cv.Trump,
						'dkno' : cv.Unknown,
						'title': cv.Value
					}
				}
			}

			//age state
			if(electionGlobals.stateList.indexOf(cv.Sample) >= 0 && cv.Demographic === 'age') {
				if(cv.Value === 'Age: 18-34') {
					electionGlobals.ageState[cv.Sample][cv.Value] = {
						'clinton': cv.Clinton,
						'trump': cv.Trump,
						'dkno' : cv.Unknown,
						'state': cv.Sample,
						'title': cv.Value
					}
				}
				if(cv.Value === 'Age: 35-44') {
					electionGlobals.ageState[cv.Sample][cv.Value] = {
						'clinton': cv.Clinton,
						'trump': cv.Trump,
						'dkno' : cv.Unknown,
						'state': cv.Sample,
						'title': cv.Value
					}
				}
				if(cv.Value === 'Age: 45-64') {
					electionGlobals.ageState[cv.Sample][cv.Value] = {
						'clinton': cv.Clinton,
						'trump': cv.Trump,
						'dkno' : cv.Unknown,
						'state': cv.Sample,
						'title': cv.Value
					}
				}
				if(cv.Value === 'Age: 65+') {
					electionGlobals.ageState[cv.Sample][cv.Value] = {
						'clinton': cv.Clinton,
						'trump': cv.Trump,
						'dkno' : cv.Unknown,
						'title': cv.Value,
						'state': cv.Sample,
					}
				}
			}
		});

	}



	function drawHorizontalBarChart(element, dataOptions) {
		var elm = document.getElementById(element);
		var clintonData = dataOptions.clinton;
		var trumpData = dataOptions.trump;
		// var dkno = dataOptions.dkno;
		var title = dataOptions.title || '';

		if(dataOptions.formatNumbers === true) {
			clintonData = (clintonData * 100) + '%';
			trumpData = (trumpData * 100) + '%';
		}

		var chart = new Chart(elm, {
			type: 'horizontalBar',
			data: {
				labels: [],
				datasets: [{
					label: "Clinton",
					data: [clintonData],
					backgroundColor: ['#79b7c6']
				},
				// {
				// 	label: "Don't Know / No Opinion",
				// 	data: [dkno],
				// 	backgroundColor: ['#e2e2e2']
				// },
				{
					label: "Trump",
					data: [trumpData],
					backgroundColor: ['#d65348']
				}
				]
			},
			options: {
				animation: {
					onComplete: function() { 
						var ctx = this.chart.ctx;
						ctx.textAlign = 'center';
						ctx.textBaseline = 'bottom';
						ctx.font = 'bold 16px proxima-nova';
						ctx.fillStyle = '#FFFFFF';
						this.data.datasets.forEach(function(ds, i) {
							if(ds.label.indexOf("Don") >= 0) { return; } // skip label for dkno
							var model = ds._meta[Object.keys(ds._meta)[0]].data[0]._model;
							if(ds.label === 'Clinton') {
								ds.data.forEach(function(value) {
									value = Math.round(value * 100) + '%';
									ctx.fillText(value, 25, model.y + 10);
								})
							}
							if(ds.label === 'Trump') {
								ds.data.forEach(function(value) {
									value = Math.round(value * 100) + '%';
									ctx.fillText(value, model.x - 25, model.y + 10);
								})
							}
							
						})
					},
				},
				hover: {
						animationDuration: 0
				},
				scales: {
					xAxes: [{
						display: false,
						gridLines: {
							display: false,
						},
						stacked: true
					}],
					yAxes: [{
						display: false,
						gridLines: {
							display: false
						},
						stacked: true
					}]
			}
			}
		});
	}

	function drawVerticalBarChart(element, dataOptions) {
		var elm = document.getElementById(element);
		var femaleData = dataOptions.female;
		var maleData = dataOptions.male;
		var state = dataOptions.male.state || '';

		var chart = new Chart(elm, {
			type: 'bar',
			data: {
				labels: ['Female', 'Male'],
				datasets: [{
					label: "Clinton",
					data: [femaleData.clinton, maleData.clinton],
					backgroundColor: ['#79b7c6','#79b7c6'],
					xAxisId: 'f'
				},
				{
					label: "Trump",
					data: [femaleData.trump, maleData.trump],
					backgroundColor: ['#d65348','#d65348'],
					xAxisId: 'm' 
				}
				]
			},
			options: {
				maintainAspectRatio: false,
				title: {
					display: true,
					text: state.toUpperCase(),
					fontSize: 16,
					fontColor: '#2b2b2b',
					fontWeight: 'bold',
					padding: 40 
				},
				animation: {
					onComplete: function() { 
						var ctx = this.chart.ctx;
						ctx.textAlign = 'center';
						ctx.textBaseline = 'bottom';
						ctx.font = 'bold 14px proxima-nova';
						ctx.fillStyle = '#2b2b2b';
						this.data.datasets.forEach(function(ds, i) {
							if(ds.label.indexOf("Don") >= 0) { return; } // skip label for dkno
							for(i = 0; i < ds.data.length; i++) {
								var model = ds._meta[Object.keys(ds._meta)[0]].data[i]._model;
								ctx.fillText(Math.round(ds.data[i] * 100), model.x, model.y);
							}
						}, this.data)
					},
				},
				hover: {
						animationDuration: 0
				},
				scales: {
					xAxes: [{
						display: true,
						gridLines: {
							display: false,
							drawBorder: false
						},
						id: 'f',
						ticks: {
							fontSize: 14,
						},
					},{
						id: 'm',
						display: false,
						gridLines : {
							display: false,
							drawBorder: false
						}
					}
					],
					yAxes: [{
						display: false,
						gridLines: {
							display: false,
							drawBorder: false
						},
						ticks: {
							fontSize: 14,
						},
					}]
			}
			}
		});
	}

	function drawVerticalBarChartParty(element, dataOptions) {
		var elm = document.getElementById(element);
		var repData = dataOptions.rep;
		var demData = dataOptions.dem;
		var indData = dataOptions.ind;
		var state = dataOptions.rep.state || '';

		var chart = new Chart(elm, {
			type: 'bar',
			data: {
				labels: ['Dem.','Rep.','Ind.'],
				datasets: [{
					label: "Clinton",
					data: [repData.clinton, demData.clinton, indData.clinton],
					backgroundColor: ['#79b7c6','#79b7c6','#79b7c6']
				},
				{
					label: "Trump",
					data: [repData.trump, demData.trump, indData.trump],
					backgroundColor: ['#d65348','#d65348','#d65348']
				}
				]
			},
			options: {
				maintainAspectRatio: false,
				title: {
					display: true,
					text: state.toUpperCase(),
					fontSize: 16,
					fontColor: '#2b2b2b',
					fontWeight: 'bold',
					padding: 40 
				},
				animation: {
					onComplete: function() { 
						var ctx = this.chart.ctx;
						ctx.textAlign = 'center';
						ctx.textBaseline = 'bottom';
						ctx.font = 'bold 14px proxima-nova';
						ctx.fillStyle = '#2b2b2b';
						this.data.datasets.forEach(function(ds, i) {
							for(i = 0; i < ds.data.length; i++) {
								var model = ds._meta[Object.keys(ds._meta)[0]].data[i]._model;
								ctx.fillText(Math.round(ds.data[i] * 100), model.x, model.y);
							}
						}, this.data)
					},
				},
				hover: {
					animationDuration: 0
				},
				scales: {
					xAxes: [{
						display: true,
						gridLines: {
							display: false,
							drawBorder: false,
						},
						ticks: {
							fontSize: 14,
						},
						stacked: false
					}],
					yAxes: [{
						display: false,
						drawBorder: false,
						gridLines: {
							display: false
						},
						stacked: false
					}]
			}
			}
		});

	}

	function drawVerticalBarChartAge(element, dataOptions) {
		var elm = document.getElementById(element);
		var data1 = dataOptions['Age: 18-34'];
		var data2 = dataOptions['Age: 35-44'];
		var data3 = dataOptions['Age: 45-64'];
		var data4 = dataOptions['Age: 65+'];
		var state = data1.state || '';

		var chart = new Chart(elm, {
			type: 'bar',
			data: {
				labels: ['18-34','35-44','45-64','65+'],
				datasets: [{
					label: "Clinton",
					data: [data1.clinton, data2.clinton, data3.clinton, data4.clinton],
					backgroundColor: ['#79b7c6','#79b7c6','#79b7c6','#79b7c6','#79b7c6']
				},
				{
					label: "Trump",
					data: [data1.trump, data2.trump, data3.trump, data4.trump],
					backgroundColor: ['#d65348','#d65348','#d65348','#d65348','#d65348']
				}
				]
			},
			options: {
				maintainAspectRatio: false,
				title: {
					display: true,
					text: state.toUpperCase(),
					fontSize: 16,
					fontColor: '#2b2b2b',
					fontWeight: 'bold',
					padding: 40 
				},
				animation: {
					onComplete: function() { 
						var ctx = this.chart.ctx;
						ctx.textAlign = 'center';
						ctx.textBaseline = 'bottom';
						ctx.font = 'bold 14px proxima-nova';
						ctx.fillStyle = '#2b2b2b';
						this.data.datasets.forEach(function(ds, i) {
							for(i = 0; i < ds.data.length; i++) {
								var model = ds._meta[Object.keys(ds._meta)[0]].data[i]._model;
								ctx.fillText(Math.round(ds.data[i] * 100), model.x, model.y);
							}
						}, this.data)
					},
				},
				hover: {
					animationDuration: 0
				},
				scales: {
					xAxes: [{
						display: true,
						gridLines: {
							display: false,
							drawBorder: false,
						},
						ticks: {
							fontSize: 14,
						},
						stacked: false
					}],
					yAxes: [{
						display: false,
						drawBorder: false,
						gridLines: {
							display: false
						},
						stacked: false
					}]
			}
			}
		});
	}



	function initCharts() {
		initGlobalChartConfig();

		var partialsToWatch = [
			{
				elm: 'partial-race-national',
				func: delayed1
			},
			{
				elm: 'partial-race-state',
				func:	delayed2
			},
			{
				elm: 'partial-age-national',
				func:	delayed3
			},
			{
				elm: 'partial-age-state',
				func:	delayed4
			}
		];

		var drawInterval = w.setInterval(function() {
			partialsToWatch[0].func.call();
			partialsToWatch.splice(0,1);
			if(partialsToWatch.length === 0) {
				w.clearInterval(drawInterval);
			}
		}, 800);

		function delayed1() {
			Object.keys(electionGlobals.partyNational).forEach(function(cv, i) {
				drawHorizontalBarChart('party-national-'+i, this[cv]);
			}, electionGlobals.partyNational)
		}
		function delayed2() {
			Object.keys(electionGlobals.partyState).forEach(function(cv, i) {
				drawVerticalBarChartParty('party-state-'+i, this[cv]);
			}, electionGlobals.partyState)
		}
		function delayed3() {
			Object.keys(electionGlobals.ageNational).forEach(function(cv, i) {
				drawHorizontalBarChart('age-national-'+i, this[cv]);
			}, electionGlobals.ageNational)
		}
		function delayed4() {
			Object.keys(electionGlobals.ageState).forEach(function(cv, i) {
				drawVerticalBarChartAge('age-state-'+i, this[cv]);
			}, electionGlobals.ageState)
		}

		Object.keys(electionGlobals.genderNational).forEach(function(cv, i) {
			drawHorizontalBarChart('gender-national-'+i, this[cv]);
		}, electionGlobals.genderNational);

		Object.keys(electionGlobals.genderState).forEach(function(cv, i) {
			drawVerticalBarChart('gender-state-'+i, this[cv]);
		}, electionGlobals.genderState);

	}
	
	function initHandlebars() {
		// console.log(electionGlobals);
		var electionTabSource = $('#interactive-template').html();
		var electionTab = Handlebars.compile(electionTabSource);
		electionTabHTML = electionTab(electionGlobals);
		$('#interactive').html(electionTabHTML);
	}

  function init() {
		electionGlobals.electionData = JSON.parse(w.mc_election_interactive_data);
		parseRawData(electionGlobals.electionData);
		registerHelpers();
		registerPartials();
		initHandlebars();
		initCharts();
  }

  $(d).ready(function() {
    init();
  });

})(window, document, jQuery);
