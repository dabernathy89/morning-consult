<?php
/**
 * Morning Consult ESP Integrations Create Campaign Interface
 * @version 0.1.0
 * @package Morning Consult ESP Integrations
 */

interface MCESPI_Create_Campaign_Interface {
	public function create_campaign( $post_id, $sender_email, $sender_name, $email_content, $meta_prefix );
}
