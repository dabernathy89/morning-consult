(function(w,d,$) {

  function initHandlebarsHelpers() {
    Handlebars.registerHelper('indexPlusOne', function(index) {
      return index +1;
    });
    Handlebars.registerHelper('trimPartyName', function(party) {
      return party.substring(0,1).toUpperCase();
    });
    Handlebars.registerHelper('getMCILink', function(name) {
      var nameToLink = {
        "Bernard Sanders" : "https://morningconsultintelligence.com/examine?v=TEVHMl9CZXJuYXJkX1NhbmRlcnM%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Susan Collins" : "https://morningconsultintelligence.com/examine?v=TEVHMl9TdXNhbl9Db2xsaW5z&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "John Hoeven" : "https://morningconsultintelligence.com/examine?v=TEVHMl9Kb2huX0hvZXZlbg%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Angus King" : "https://morningconsultintelligence.com/examine?v=TEVHMl9Bbmd1c19LaW5n&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Patrick Leahy" : "https://morningconsultintelligence.com/examine?v=TEVHMl9QYXRyaWNrX0xlYWh5&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Thomas Carper" : "https://morningconsultintelligence.com/examine?v=TEVHMl9UaG9tYXNfQ2FycGVy&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Amy Klobuchar" : "https://morningconsultintelligence.com/examine?v=TEVHMl9BbXlfS2xvYnVjaGFy&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "John Barrasso" : "https://morningconsultintelligence.com/examine?v=TEVHMl9Kb2huX0JhcnJhc3Nv&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Alan Franken" : "https://morningconsultintelligence.com/examine?v=TEVHMl9BbGFuX0ZyYW5rZW4%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Chris Coons" : "https://morningconsultintelligence.com/examine?v=TEVHMl9DaHJpc19Db29ucw%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Mitch McConnell" : "https://morningconsultintelligence.com/examine?v=TEVHMl9NaXRjaF9NY0Nvbm5lbGw%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Pat Roberts" : "https://morningconsultintelligence.com/examine?v=TEVHMl9QYXRfUm9iZXJ0cw%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "John McCain" : "https://morningconsultintelligence.com/examine?v=TEVHMl9Kb2huX01jQ2Fpbg%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Orrin Hatch" : "https://morningconsultintelligence.com/examine?v=TEVHMl9PcnJpbl9IYXRjaA%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Harry Reid" : "https://morningconsultintelligence.com/examine?v=TEVHMl9IYXJyeV9SZWlk&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Marco Rubio" : "https://morningconsultintelligence.com/examine?v=TEVHMl9NYXJjb19SdWJpbw%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "David Vitter" : "https://morningconsultintelligence.com/examine?v=TEVHMl9EYXZpZF9WaXR0ZXI%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Jon Tester" : "https://morningconsultintelligence.com/examine?v=TEVHMl9Kb25fVGVzdGVy&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Claire McCaskill" : "https://morningconsultintelligence.com/examine?v=TEVHMl9DbGFpcmVfTWNDYXNraWxs&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Tammy Baldwin" : "https://morningconsultintelligence.com/examine?v=TEVHMl9UYW1teV9CYWxkd2lu&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Gary Peters" : "https://morningconsultintelligence.com/examine?v=TEVHMl9HYXJ5X1BldGVycw%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "John Cornyn" : "https://morningconsultintelligence.com/examine?v=TEVHMl9Kb2huX0Nvcm55bg%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Robert Portman" : "https://morningconsultintelligence.com/examine?v=TEVHMl9Sb2JlcnRfUG9ydG1hbg%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Tim Scott" : "https://morningconsultintelligence.com/examine?v=TEVHMl9UaW1fU2NvdHQ%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Dean Heller" : "https://morningconsultintelligence.com/examine?v=TEVHMl9EZWFuX0hlbGxlcg%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Edward Markey" : "https://morningconsultintelligence.com/examine?v=TEVHMl9FZHdhcmRfTWFya2V5&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Mark Kirk" : "https://morningconsultintelligence.com/examine?v=TEVHMl9NYXJrX0tpcms%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Roger Wicker" : "https://morningconsultintelligence.com/examine?v=TEVHMl9Sb2dlcl9XaWNrZXI%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Bill Cassidy" : "https://morningconsultintelligence.com/examine?v=TEVHMl9CaWxsX0Nhc3NpZHk%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "James Lankford" : "https://morningconsultintelligence.com/examine?v=TEVHMl9KYW1lc19MYW5rZm9yZA%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0"
      }
      return nameToLink[name];
    });
    Handlebars.registerHelper('nickname', function(name) {
      var nameToNickname = {
        "Alan Franken" : "Al Franken",
        "Bernard Sanders" : "Bernie Sanders",
        "Edward Markey" : "Ed Markey"
      }
      var returnedName = nameToNickname[name];
      if(typeof returnedName === "undefined") {
        return name;
      }
      else return returnedName;
    })

  }

  function initTables() {

    var top10Data = w.mc_senate_ranking_data.alltop;
    var source = $('#hb-top10-template').html();
    var template = Handlebars.compile(source, {strict: true});
    var top10Html = template(top10Data.top10);
    var bottom10Html = template(top10Data.bottom10);
    var unknown10Html = template(top10Data.unknown10);
    $('#top10').html(top10Html);
    $('#bottom10').html(bottom10Html);
    $('#most-unknown').html(unknown10Html);

  }


  function init() {
    console.log('init');
    console.log(w.mc_senate_ranking_data.alltop.top10);
    initHandlebarsHelpers();
    initTables();
    $('#tab-top10').trigger('shown.bs.tab');
  }


  $(d).ready(function() {
    init();
  })

})(window, document, jQuery);
