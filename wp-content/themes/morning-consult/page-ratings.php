<?php
/**
 * Template name: Ratings
 *
 * @package Morning Consult 2015
 */

get_header(); 

$args = array( 		'posts_per_page' => -1,  // Get all posts 
					'post_type' => 'state');  // The Custom Post Type we named 'products'

$state_rating = get_posts( $args );

$color = "";
$state_rate_array = [];
$state_color_array = [];
$state_desc_array = [];
$state_rank_array = [];
$state_sen_array = [];
$state_senappr_array = [];
$arr = [];

$totalDem = 0;
$totalRep = 0;
$totalToss = 0;

$demSeats = 0;
$repSeats = 0;

//Loop through each 'States' post
foreach ( $state_rating as $rating ) : setup_postdata( $rating );
	$stateID = $rating->ID;
	$state = get_field( "state_name", $stateID );
	$rating = get_field( "rating", $stateID);
	$desc = get_field("description", $stateID);	
	$rank = get_field("competitive_ranking", $stateID);	
	$incName = get_field("inc_sen_name", $stateID);	
	$incAppr = get_field("inc_sen_approval", $stateID);	
	
	//Democrat ratings are 1,2,3 while Republican ratings are 5,6,7
	if($rating <= 3){
		$totalDem++;
	}else if($rating == 4){
		$totalToss++;
	}else if($rating >= 4){
		$totalRep++;
	}
	else{
		//do nothing
	}

	$s = array(
			"state" => $state,
			"rating" => $rating,
			"desc" => $desc,
			"rank" => $rank,
			"incName" => $incName,
			"incAppr" => $incAppr
	);
	array_push($arr, $s);																					

	//Create rating, desc, rank, sen name, sen appr arrays
	$state_rate_array[$state] = $rating;
	$state_desc_array[$state] = $desc;
	$state_rank_array[$state] = $rank;
	$state_sen_array[$state] = $incName;
	$state_senappr_array[$state] = $incAppr;
	
endforeach;
wp_reset_postdata();

$demSeats = 26 - $totalDem;
$repSeats = 26 - $totalCount;

//echo json_encode($arr);

//Sort the rank array
asort($state_rank_array);

//Sort the rating array for correct color coding
asort($state_rate_array);

//For each rating, assign the appropriate color
foreach($state_rate_array as $key=>$value){
	//Assign color
	$color = "";
	if($value == 1)
		$color = "#001F5C";
	else if($value == 2)
		$color = "#003399";
	else if($value == 3)
		$color = "#0b5a88";
	else if($value == 4)
		$color = "#B2B2B2";
	else if($value == 5)
		$color = "#A34747";
	else if($value == 6)
		$color = "#A31919";
	else if($value == 7)
		$color = "#800000";
	else
		$color = "#4E4E4E";	
	
	$state_color_array[$key] = $color;
	
	//showSenate();
}
?>

<div class="container">
	<div id="ratingsInfo">
		<div class="row">
			<div class="title">Battle for the Senate</div>
			<div class="subtitle">A Morning Consult Weekly Analysis of Senate Race Ratings for the 33 Seats Up for Election.</div><br/>
		</div>
	
		<div class="row">
			<!-- Labels bar made with CSS -->
			<div id="barDetails" class="clearfix">
					<div id="demCount" class="count"><font size="75px"><?php echo $demSeats?></font> Democrats </div>
					<div id="gopCount" class="count">Republicans <font size="75px"><?php echo $repSeats ?></font></div>

					<div class="cntr" class="count">
						<div class="mjrneed">51 NEEDED FOR MAJORITY </div>
						<i class="fa fa-arrow-down"></i>
					</div>
			</div>
	
			<!-- Ratings bar made with highcharts -->
			<div id="stackedBar" style="min-width: 310px; height: 100px; margin: 0 auto"></div>
		</div>

		<br/><br/><br/>
		<div class="row">
			<div class="col-sm-4">
				<div id="clicked-state">
					<h2  style="font-family: Times New Roman;">State Ratings</h2><hr/>
					Welcome to the Morning Consult senate race ratings.<br/><br/>
				
					1. Browse the horizontal bar at the top for a state-by-state breakdown of race ratings.<br/><br/>
				
					2. Click on colored states on the map to the right for more additional race information.<br/><br/>
				
					3. View state stats in the Competitive Rankings table below.
				</div>
			</div>
		
			<div class="col-sm-8">
				<div id="map" style="width: 900px; height: 500px;"></div>
			</div>
		</div>

		<div class="row">
			<h2>Competitive Rankings</h2>
			<div class="col-sm-12">
				<table id="example" class="table table-striped table-bordered pretty" width="100%"></table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<link src="https://cdn.datatables.net/1.10.8/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<script>
jQuery(document).ready(function($){

//Filling out the DataTables
var tableData = <?php echo json_encode($arr); ?>;

var tblarr = [];
for(i = 0; i < tableData.length; i++){
	var tmp = [tableData[i].rank, tableData[i].state, tableData[i].desc, tableData[i].incAppr+'%'];
	tblarr.push(tmp);
}

$('#example').DataTable( {
	"data": tblarr,
	autoWidth: false,
	columns: [
		{ title: "No.",
		  width: "5%"
		},
		{ title: "State",
		  width: "5%"
		  },
		{ title: "Update",
		  width: "80%"
		  },
		{ title: "Approval",
		width: "10%"
		}
	],
	paging: false,
	searching: false,
	bInfo : false
});

/**** Get the color, desc, rank, sen name, and sen approv arrays from PHP ****/   
var colorarray = <?php echo json_encode($state_color_array); ?>;
var descarray = <?php echo json_encode($state_desc_array); ?>;	
var rankarray = <?php echo json_encode($state_rank_array); ?>;
var senNamearray = <?php echo json_encode($state_sen_array); ?>;	
var senApprarray = <?php echo json_encode($state_senappr_array); ?>;

//Map fill color initializers
var AKfill, ALfill, AZfill, ARfill, CAfill, COfill, CTfill, DEfill, FLfill, GAfill, HIfill, IDfill, ILfill, INfill, IAfill, KSfill, KYfill, LAfill, MEfill, MDfill, MAfill, MIfill, MNfill, MSfill, MOfill, MTfill, NEfill, NVfill, NHfill, NJfill, NMfill, NYfill, NCfill, NDfill, OHfill, OKfill, ORfill, PAfill, RIfill, SCfill, SDfill, TNfill, TXfill, UTfill, VTfill, VAfill, WAfill, WVfill, WIfill, WYfill = "grey";

//Assign map fill values
$.each(colorarray, function(index, value) {
	if(index === "AK")
		AKfill = value;
	if(index === "AL")		
		ALfill = value;
	if(index === "AZ")
		AZfill = value;
	if(index === "AR")
		ARfill = value;
	if(index === "CA")
		CAfill = value;
	if(index === "CO")
		COfill = value;
	if(index === "CT")
		CTfill = value;
	if(index === "DE")
		DEfill = value;
	if(index === "FL")
		FLfill = value;
	if(index === "GA")
		GAfill = value;
	if(index === "HI")
		HIfill = value;
	if(index === "ID")
		IDfill = value;
	if(index === "IL")
		ILfill = value;
	if(index === "IN")
		INfill = value;
	if(index === "IA")
		IAfill = value;
	if(index === "KS")
		KSfill = value;
	if(index === "KY")
		KYfill = value;
	if(index === "LA")
		LAfill = value;
	if(index === "ME")
		MEfill = value;
	if(index === "MD")
		MDfill = value;
	if(index === "MA")
		MAfill = value;
	if(index === "MI")
		MIfill = value;
	if(index === "MN")
		MNfill = value;
	if(index === "MS")
		MSfill = value;
	if(index === "MO")
		MOfill = value;
	if(index === "MT")
		MTfill = value;
	if(index === "NE")
		NEfill = value;
	if(index === "NV")
		NVfill = value;
	if(index === "NH")
		NHfill = value;
	if(index === "NJ")
		NJfill = value;
	if(index === "NM")
		NMfill = value;
	if(index === "NY")
		NYfill = value;
	if(index === "NC")
		NCfill = value;
	if(index === "ND")
		NDfill = value;
	if(index === "OH")
		OHfill = value;
	if(index === "OK")
		OKfill = value;
	if(index === "OR")
		ORfill = value;
	if(index === "PA")
		PAfill = value;
	if(index === "RI")	
		RIfill = value;
	if(index === "SC")
		SCfill = value;
	if(index === "SD")
		SDfill = value;
	if(index === "TN")
		TNfill = value;
	if(index === "TX")
		TXfill = value;
	if(index === "UT")
		UTfill = value;
	if(index === "VT")
		VTfill = value;
	if(index === "VA")
		VAfill = value;
	if(index === "WA")
		WAfill = value;
	if(index === "WV")
		WVfill = value;
	if(index === "WI")
		WIfill = value;
	if(index === "WY")
		WYfill = value;
});

//Actually fill in the map
$('#map').usmap({
showLabels: true,
"labelWidth": 40,
'stateSpecificStyles': {
	'AK': {fill: AKfill},
	'AL': {fill: ALfill},
	'AZ': {fill: AZfill},
	'AR': {fill: ARfill},
	'CA': {fill: CAfill},
	'CO': {fill: COfill},
	'CT': {fill: CTfill},
	'DE': {fill: DEfill},
	'FL': {fill: FLfill},
	'GA': {fill: GAfill},
	'HI': {fill: HIfill},
	'ID': {fill: IDfill},
	'IL': {fill: ILfill},
	'IN': {fill: INfill},
	'IA': {fill: IAfill},
	'KS': {fill: KSfill},
	'KY': {fill: KYfill},
	'LA': {fill: LAfill},
	'ME': {fill: MEfill},
	'MD': {fill: MDfill},
	'MA': {fill: MAfill},
	'MI': {fill: MIfill},
	'MN': {fill: MNfill},
	'MS': {fill: MSfill},
	'MO': {fill: MOfill},
	'MT': {fill: MTfill},
	'NE': {fill: NEfill},
	'NV': {fill: NVfill},
	'NH': {fill: NHfill},
	'NJ': {fill: NJfill},
	'NM': {fill: NMfill},
	'NY': {fill: NYfill},
	'NC': {fill: NCfill},
	'ND': {fill: NDfill},
	'OH': {fill: OHfill},
	'OK': {fill: OKfill},
	'OR': {fill: ORfill},
	'PA': {fill: PAfill},
	'RI': {fill: RIfill},
	'SC': {fill: SCfill},
	'SD': {fill: SDfill},
	'TN': {fill: TNfill},
	'TX': {fill: TXfill},
	'UT': {fill: UTfill},
	'VT': {fill: VTfill},
	'VA': {fill: VAfill},
	'WA': {fill: WAfill},
	'WV': {fill: WVfill},
	'WI': {fill: WIfill},
	'WY': {fill: WYfill}
},
// The click action
'click': myCallBack,
stateHoverStyles: {
	"stroke" : "white",
	"stroke-width": 2
},
'mouseover': function(event, data) {
	//console.log('Mouse over: ' + data.name);
	var title = data.name;
	$('<p class="tooltip"></p>').text(title).appendTo('body').fadeIn('slow');
	$('#map').mousemove(function(e) {
		var mousex = e.pageX - 10; //Get X coordinates
		var mousey = e.pageY - 30; //Get Y coordinates
		$('.tooltip').css({ top: mousey, left: mousex })
	});
},
'mouseout': function(event, data) {
	$('.tooltip').remove();
},
showLabels: true,
enableToolTips: true
});

//When clicking on a state, get the desc, rank, sen name, sen approval and show in the #clicked-state div
function myCallBack(event, data){
	var stateName = convert_state(data.name, 'name');

	var msg = "";
	
	var noinfo = "No election";
	var custom = "";
	var rank = "";
	var incSen = "";
	var incSenAppr = "";
	   
	//Get the description 	
	for(i = 0; i < myDescData.length; i++){
		if(myDescData[i].name === data.name){
			custom = myDescData[i].desc;
			break;
		}
	}
	
	//Get the ranking
	for(i = 0; i < myRankData.length; i++){
		if(myRankData[i].name === data.name){
			rank = myRankData[i].rank;
			break;
		}
	}
	
	//Get the incumbent senator name
	for(i = 0; i < mySenData.length; i++){
		if(mySenData[i].name === data.name){
			incSen = mySenData[i].sen;
			break;
		}
	}
	
	//Get the incumbent senator approval rating.
	for(i = 0; i < mySenApprData.length; i++){
		if(mySenData[i].name === data.name){
			incSenAppr = mySenApprData[i].appr;
			break;
		}
	}
	if(custom === ""){
		msg = '<h2 style="font-family: Times New Roman;">'+stateName+'</h2><hr/>' + noinfo;
	}
	else{
		msg = '<h2 style="font-family: Times New Roman;">'+stateName+'</h2><hr/>' + custom + '<br/><b>Competitive Ranking: </b> #' + rank + '<br/><b>Incumbent Senator: </b>' + incSen + '<br/><b>Incumbent Senator Approval: </b>' + incSenAppr + '%';
	}
	$('#clicked-state').html(msg);
}
	
var myColorData = [];
	$.each(colorarray, function(index, value){
		var obj = { 
			name: index,
			data: [1],
			color: value,
			showInLegend: false
		};
		myColorData.push(obj);
});


/**** The following convert the PHP->JSON to arrays for looking up state info ***/
var myDescData = [];
	$.each(descarray, function(index, value){
		var obj = { 
			name: index,
			desc: value,
		};
		myDescData.push(obj);
});

var myRankData = [];
	$.each(rankarray, function(index, value){
		var obj = { 
			name: index,
			rank: value,
		};
		myRankData.push(obj);
});

var mySenData = [];
	$.each(senNamearray, function(index, value){
		var obj = { 
			name: index,
			sen: value,
		};
		mySenData.push(obj);
});

var mySenApprData = [];
	$.each(senApprarray, function(index, value){
		var obj = { 
			name: index,
			appr: value,
		};
		mySenApprData.push(obj);
});
	

/*** Create the staked bar chart in high charts plugin ***/	

var chart; 

chart = new Highcharts.Chart({
	chart: {
		renderTo: 'stackedBar',
		type: 'bar',
		margin: 0
	},
	title: {
		text: ''
	},
	xAxis: {
		categories: ['Rating'],
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		 },
		minorTickLength: 0,
		tickLength: 0,
		gridLineWidth: 0,
		minorGridLineWidth: 0,
		minPadding: 0,
        maxPadding: 0
	},
	yAxis: {
		min: 0,
		title: {
			text: ''
		},
		gridLineWidth: 0,
		minorGridLineWidth: 0,
		labels: {
			enabled: false
		},
		reversedStacks: false
	},
	legend: {
		reversed: false
	},
	plotOptions: {
		series: {
			stacking: 'percent'
		},
		bar : {
			dataLabels: {
				enabled: false
			}
		}
	},
	credits: {
      enabled: false
  	},
	tooltip: {
		useHTML: true,
		formatter: function () {
			var custom = "";
			var rank = "";
			var incSec = "";
			var incSenAppr = "";
			var stateName = convert_state(this.series.name, 'name');
			
			for(i = 0; i < myDescData.length; i++){
				if(myDescData[i].name === this.series.name){
					custom = myDescData[i].desc;
					break;
				}
			}
			
			//Get the ranking
			for(i = 0; i < myRankData.length; i++){
				if(myRankData[i].name === this.series.name){
					rank = myRankData[i].rank;
					break;
				}
			}
	
			//Get the incumbent senator name
			for(i = 0; i < mySenData.length; i++){
				if(mySenData[i].name === this.series.name){
					incSen = mySenData[i].sen;
					break;
				}
			}
	
			//Get the incumbent senator approval rating.
			for(i = 0; i < mySenApprData.length; i++){
				if(mySenData[i].name === this.series.name){
					incSenAppr = mySenApprData[i].appr;
					break;
				}
			}
			return "<div style='width: 250px; white-space:normal;'><b style='text-transform:uppercase'>" + stateName + "</b><br/><br/><b>Competitive Ranking: </b> #" + rank + "<br/><b>Incumbent Senator: </b>" + incSen +"<br/><b>Approval Rating: </b>" + incSenAppr + "%</div>";
		},
		positioner: function(boxWidth, boxHeight, point) {         
				return {x:point.plotX + 20,
						y:point.plotY + 5};         
		}
	},
	series: myColorData
});

//Convert abbr to names
function convert_state(name, to) {
    var name = name.toUpperCase();
    var states = new Array(                         {'name':'Alabama', 'abbrev':'AL'},          {'name':'Alaska', 'abbrev':'AK'},
        {'name':'Arizona', 'abbrev':'AZ'},          {'name':'Arkansas', 'abbrev':'AR'},         {'name':'California', 'abbrev':'CA'},
        {'name':'Colorado', 'abbrev':'CO'},         {'name':'Connecticut', 'abbrev':'CT'},      {'name':'Delaware', 'abbrev':'DE'},
        {'name':'Florida', 'abbrev':'FL'},          {'name':'Georgia', 'abbrev':'GA'},          {'name':'Hawaii', 'abbrev':'HI'},
        {'name':'Idaho', 'abbrev':'ID'},            {'name':'Illinois', 'abbrev':'IL'},         {'name':'Indiana', 'abbrev':'IN'},
        {'name':'Iowa', 'abbrev':'IA'},             {'name':'Kansas', 'abbrev':'KS'},           {'name':'Kentucky', 'abbrev':'KY'},
        {'name':'Louisiana', 'abbrev':'LA'},        {'name':'Maine', 'abbrev':'ME'},            {'name':'Maryland', 'abbrev':'MD'},
        {'name':'Massachusetts', 'abbrev':'MA'},    {'name':'Michigan', 'abbrev':'MI'},         {'name':'Minnesota', 'abbrev':'MN'},
        {'name':'Mississippi', 'abbrev':'MS'},      {'name':'Missouri', 'abbrev':'MO'},         {'name':'Montana', 'abbrev':'MT'},
        {'name':'Nebraska', 'abbrev':'NE'},         {'name':'Nevada', 'abbrev':'NV'},           {'name':'New Hampshire', 'abbrev':'NH'},
        {'name':'New Jersey', 'abbrev':'NJ'},       {'name':'New Mexico', 'abbrev':'NM'},       {'name':'New York', 'abbrev':'NY'},
        {'name':'North Carolina', 'abbrev':'NC'},   {'name':'North Dakota', 'abbrev':'ND'},     {'name':'Ohio', 'abbrev':'OH'},
        {'name':'Oklahoma', 'abbrev':'OK'},         {'name':'Oregon', 'abbrev':'OR'},           {'name':'Pennsylvania', 'abbrev':'PA'},
        {'name':'Rhode Island', 'abbrev':'RI'},     {'name':'South Carolina', 'abbrev':'SC'},   {'name':'South Dakota', 'abbrev':'SD'},
        {'name':'Tennessee', 'abbrev':'TN'},        {'name':'Texas', 'abbrev':'TX'},            {'name':'Utah', 'abbrev':'UT'},
        {'name':'Vermont', 'abbrev':'VT'},          {'name':'Virginia', 'abbrev':'VA'},         {'name':'Washington', 'abbrev':'WA'},
        {'name':'West Virginia', 'abbrev':'WV'},    {'name':'Wisconsin', 'abbrev':'WI'},        {'name':'Wyoming', 'abbrev':'WY'}
        );
    var returnthis = false;
    $.each(states, function(index, value){
        if (to == 'name') {
            if (value.abbrev == name){
                returnthis = value.name;
                return false;
            }
        } else if (to == 'abbrev') {
            if (value.name.toUpperCase() == name){
                returnthis = value.abbrev;
                return false;
            }
        }
    });
    return returnthis;
}

});

</script>

<?php get_footer(); ?>

<style>
.title {
	font-size: 40px;
	color: #2B2B2B;
	font-family: "Avenir Next";
	text-transform: uppercase;
	text-align: center;
	letter-spacing: 100%;
}
.subtitle {
	font-size: 20px;
	color: #2B2B2B;
	font-family: "Avenir Next";
	text-align: center;
	letter-spacing: 100%;
}
#clicked-state {
	font-size: 16px;
	color: #2B2B2B;
	font-family: "Avenir Next";
	letter-spacing: 100%;
	padding-bottom: 50px;
}
.tableStyle {
	font-size: 14px;
	color: #2B2B2B;
	font-family: "Avenir Next";
	letter-spacing: 100%;
}
.highcharts-container { overflow: visible !important; }
.highcharts-container svg {
    overflow: visible !important;
}
#barDetails {
    padding-right: 5px;
    padding-left: 5px;
}
.clearfix {
    display: block;
    clear: both;
}
.mjrneed {
	font-family: "Avenir Next";
	font-size: 14px;
	color: grey;
	text-align: center;
}
#demCount {
    color: #0b5a88;
    float: left;
    margin-right: 8px;
    font-size: 15px;
	font-family: "Avenir Next";
    padding-top: 2px;
    display: inline-block;
}
#gopCount {
    color: #a83d42;
    text-align: right;
    float: right;
    margin-left: 8px;
	font-size: 15px;
	font-family: "Avenir Next";
    padding-top: 2px;
    display: inline-block;
}
.controlBar {
	height: 20px;
	width: 100%;
    background-color: red;
}
.demText{
	text-align: left;
    color: #0b5a88;
    font-size: 30px;
	font-family: "Avenir Next";
    padding-top: 2px;
    display: inline-block;
    font-weight: bold;
}
.repText{
	position: relative;
	float: right;
	color: #a83d42;
	font-size: 60px;
	font-family: "Avenir Next";
    padding-top: 2px;
    display: inline-block;
    font-weight: bold;
}
.cntr {
	text-align: center;
}
.mjrneed {
	font-family: "Avenir Next";
	font-size: 14px;
	color: grey;
	text-align: center;
	padding-top: 20px;
}
#ratingsInfo {
	padding-top: 100px;
}
 
div.dataTables_length,
div.dataTables_filter,
div.dataTables_paginate,
div.dataTables_info {
    padding: 6px;
}
table.pretty {
	font-family: "Avenir Next";
    width: 100%;
    clear: both;
}

</style>