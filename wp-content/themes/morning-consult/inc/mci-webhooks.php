<?php /**
*
* Webhooks that fire on posts being saved/updated 
* Each action makes an HTTP request to MCI server to keep search indexes up to date.
*
* @package Morning Consult 2016
*
*/

define('MCI_ENDPOINT', 'https://morningconsultintelligence.com/api/v2/posts/');
define('MCI_SECRET', get_field('mci_secret_key', 'option'));

add_action('post_updated', 'mci_on_update_post', 10, 1);
add_action('draft_to_publish', 'mci_on_publish_post', 10, 1);
add_action('publish_to_trash', 'mci_on_delete_post', 10, 1);
add_action('publish_to_draft', 'mci_on_delete_post', 10, 1);

function mci_on_update_post($post_id) {
	$status = get_post_status($post_id);
	if($status === 'publish') {
		//if published, PUT
		make_mci_request($post_id, 'PUT');
	}
}

function mci_on_publish_post($post_id) {
	make_mci_request($post_id, 'POST');
}

function mci_on_delete_post($post_id) {
	make_mci_request($post_id, "DELETE");
}

function make_mci_request($post_id, $type) {
	$url = MCI_ENDPOINT . MCI_SECRET;
	if(gettype($post_id) !== 'integer') {
		$post_id = $post_id->ID;
	}
	
	$req_args = array(
		'method' => strtoupper($type),
		'httpversion' => '1.0',
		'headers' => array(
			'Content-Type' => 'application/json'
		),
		'response' => array(),
		'body' => '{"id":"'.$post_id.'"}'
	);
	$response = wp_remote_request($url, $req_args);

	// error_log(print_r($response['body'], true));

}
?>