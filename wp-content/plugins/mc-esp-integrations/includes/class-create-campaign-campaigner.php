<?php
/**
 * Morning Consult ESP Integrations Create Campaign Campaigner
 * @version 0.1.0
 * @package Morning Consult ESP Integrations
 */

class MCESPI_Create_Campaign_Campaigner implements MCESPI_Create_Campaign_Interface {
    /**
     * Push an HTML email up to Campaigner
     *
     * @since  0.1.0
     * @return void
     */
    public function create_campaign( $post_id, $sender_email, $sender_name, $email_content, $meta_prefix ) {
        $api_key = get_field('brief_contactology_api_key','option');

        if (!$api_key) {
            wp_send_json_error( 'API key not set' );
        }

        $campaigner = new MCESPI_Campaigner_Api($api_key, true);

        $list_id = get_field($meta_prefix . 'list_id',$post_id);

        if (!$list_id) {
            wp_send_json_error( 'No List ID specified' );
        }

        try {
            $result = $campaigner->Campaign_Create_Standard(
                array(
                    'list' => $list_id
                ),
                get_the_title($post_id), // campaign name
                get_the_title($post_id), // subject
                $sender_email,
                $sender_name,
                array(
                    'html' => $email_content
                ),
                array(
                    'generateTextFromHtml' => true
                )
            );
        } catch (Exception $e) {
            wp_send_json_error($e->getMessage());
        }

        wp_send_json_success( $result );
    }
}
