<?php
/**
 * Plugin Name: Morning Consult ESP Integrations
 * Plugin URI:  https://morningconsult.com
 * Description: Email service provider integrations for morningconsult.com
 * Version:     0.2.8
 * Author:      dabernathy89
 * Author URI:  https://www.danielabernathy.com
 * Donate link: https://morningconsult.com
 * License:     GPLv2
 */

/**
 * Copyright (c) 2016 dabernathy89 (email : daniel@danielabernathy.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2 or, at
 * your discretion, any later version, as published by the Free
 * Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * Built using generator-plugin-wp
 */

/**
 * Autoloads files with classes when needed
 *
 * @since  0.1.0
 * @param  string $class_name Name of the class being requested.
 * @return void
 */
function mc_esp_integrations_autoload_classes( $class_name ) {
	if ( 0 !== strpos( $class_name, 'MCESPI_' ) ) {
		return;
	}

	$filename = strtolower( str_replace(
		'_', '-',
		substr( $class_name, strlen( 'MCESPI_' ) )
	) );

	MC_ESP_Integrations::include_file( $filename );
}
spl_autoload_register( 'mc_esp_integrations_autoload_classes' );


/**
 * Main initiation class
 *
 * @since  0.1.0
 * @var  string $version  Plugin version
 * @var  string $basename Plugin basename
 * @var  string $url      Plugin URL
 * @var  string $path     Plugin Path
 */
class MC_ESP_Integrations {

	/**
	 * Current version
	 *
	 * @var  string
	 * @since  0.1.0
	 */
	const VERSION = '0.1.0';

	/**
	 * URL of plugin directory
	 *
	 * @var string
	 * @since  0.1.0
	 */
	protected $url = '';

	/**
	 * Path of plugin directory
	 *
	 * @var string
	 * @since  0.1.0
	 */
	protected $path = '';

	/**
	 * Plugin basename
	 *
	 * @var string
	 * @since  0.1.0
	 */
	protected $basename = '';

	/**
	 * Singleton instance of plugin
	 *
	 * @var MC_ESP_Integrations
	 * @since  0.1.0
	 */
	protected static $single_instance = null;

	/**
	 * Instance of MCESPI_Create_Campaign_Campaigner
	 *
	 * @since 0.1.0
	 * @var MCESPI_Create_Campaign_Campaigner
	 */
	protected $create_campaign_campaigner;

	/**
	 * Instance of MCESPI_Create_Campaign_Ajax
	 *
	 * @since 0.1.0
	 * @var MCESPI_Create_Campaign_Ajax
	 */
	protected $create_campaign_ajax;

	/**
	 * Instance of MCESPI_Create_Campaign_Inboxfirst
	 *
	 * @since 0.1.0
	 * @var MCESPI_Create_Campaign_Inboxfirst
	 */
	protected $create_campaign_inboxfirst;

	/**
	 * Instance of MCESPI_Create_Campaign_Sfmc
	 *
	 * @since NEXT
	 * @var MCESPI_Create_Campaign_Sfmc
	 */
	protected $create_campaign_sfmc;

	/**
	 * Creates or returns an instance of this class.
	 *
	 * @since  0.1.0
	 * @return MC_ESP_Integrations A single instance of this class.
	 */
	public static function get_instance() {
		if ( null === self::$single_instance ) {
			self::$single_instance = new self();
		}

		return self::$single_instance;
	}

	/**
	 * Sets up our plugin
	 *
	 * @since  0.1.0
	 */
	protected function __construct() {
		$this->basename = plugin_basename( __FILE__ );
		$this->url      = plugin_dir_url( __FILE__ );
		$this->path     = plugin_dir_path( __FILE__ );
	}

	/**
	 * Attach other plugin classes to the base plugin class.
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public function plugin_classes() {
		// Attach other plugin classes to the base plugin class.
		include_once( self::dir( 'includes/sfmc-fuel-sdk/ET_Client.php' ) );

		$this->create_campaign_campaigner = new MCESPI_Create_Campaign_Campaigner();
		$this->create_campaign_ajax = new MCESPI_Create_Campaign_Ajax( $this );
		$this->create_campaign_inboxfirst = new MCESPI_Create_Campaign_Inboxfirst();
		$this->create_campaign_sfmc = new MCESPI_Create_Campaign_Sfmc( $this );

		/**
		 * Gravity Forms Contactology Add-On
		 */
		require self::dir( 'includes/contactology-gravity-forms.php' );
		require self::dir( 'includes/contactology-unsubscribe.php' );
		$mc_contactology = new MorningConsultContactologyGravityForms();
		$mc_contactology_unsubscribe = new MCUnsubscribe($mc_contactology);

		/**
		 * Gravity Forms InboxFirst Add-On
		 */
		require self::dir( 'includes/inboxfirst-gravity-forms.php' );
		require self::dir( 'includes/inboxfirst-unsubscribe.php' );
		$mc_inboxfirst = new MorningConsultInboxFirstGravityForms();
		$mc_inboxfirst_unsubscribe = new MCInboxfirstUnsubscribe($mc_inboxfirst);

		/**
		 * Gravity Forms SFMC Add-On
		 */
		require self::dir( 'includes/sfmc-gravity-forms.php' );
		$mc_sfmc = new MorningConsultSFMCGravityForms();
	} // END OF PLUGIN CLASSES FUNCTION

	/**
	 * Activate the plugin
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public function _activate() {
		// Make sure any rewrite functionality has been loaded.
		flush_rewrite_rules();
	}

	/**
	 * Deactivate the plugin
	 * Uninstall routines should be in uninstall.php
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public function _deactivate() {}

	/**
	 * Init hooks
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public function init() {
		if ( $this->check_requirements() ) {
			$this->plugin_classes();
		}
	}

	/**
	 * Check if the plugin meets requirements and
	 * disable it if they are not present.
	 *
	 * @since  0.1.0
	 * @return boolean result of meets_requirements
	 */
	public function check_requirements() {
		if ( ! $this->meets_requirements() ) {

			// Add a dashboard notice.
			add_action( 'all_admin_notices', array( $this, 'requirements_not_met_notice' ) );

			// Deactivate our plugin.
			add_action( 'admin_init', array( $this, 'deactivate_me' ) );

			return false;
		}

		return true;
	}

	/**
	 * Deactivates this plugin, hook this function on admin_init.
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public function deactivate_me() {
		deactivate_plugins( $this->basename );
	}

	/**
	 * Check that all plugin requirements are met
	 *
	 * @since  0.1.0
	 * @return boolean True if requirements are met.
	 */
	public static function meets_requirements() {
		// Do checks for required classes / functions
		// function_exists('') & class_exists('').
		// We have met all requirements.
		try {
			require self::dir('includes/sfmc-fuel-sdk/ET_Client.php');
			if(class_exists('ET_Client')) {
				return true;
			}
		}
		catch(Exception $e) {
			throw New Exception('Unable to load ET_Client.php: ', $e->getMessage());
			return false;
		}
	}

	/**
	 * Adds a notice to the dashboard if the plugin requirements are not met
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public function requirements_not_met_notice() {
		// Output our error.
		echo '<div id="message" class="error">';
		echo '<p>' . sprintf( __( 'Morning Consult ESP Integrations is missing requirements and has been <a href="%s">deactivated</a>. Please make sure all requirements are available.', 'mc-esp-integrations' ), admin_url( 'plugins.php' ) ) . '</p>';
		echo '</div>';
	}

	/**
	 * Magic getter for our object.
	 *
	 * @since  0.1.0
	 * @param string $field Field to get.
	 * @throws Exception Throws an exception if the field is invalid.
	 * @return mixed
	 */
	public function __get( $field ) {
		switch ( $field ) {
			case 'version':
				return self::VERSION;
			case 'basename':
			case 'url':
			case 'path':
			case 'create_campaign_campaigner':
			case 'create_campaign_ajax':
			case 'create_campaign_inboxfirst':
			case 'create_campaign_sfmc':
				return $this->$field;
			default:
				throw new Exception( 'Invalid '. __CLASS__ .' property: ' . $field );
		}
	}

	/**
	 * Include a file from the includes directory
	 *
	 * @since  0.1.0
	 * @param  string $filename Name of the file to be included.
	 * @return bool   Result of include call.
	 */
	public static function include_file( $filename ) {
		$file = self::dir( 'includes/class-'. $filename .'.php' );
		if ( file_exists( $file ) ) {
			return include_once( $file );
		}

		$file_no_prefix = self::dir( 'includes/'. $filename .'.php' );
		if ( file_exists( $file_no_prefix ) ) {
			return include_once( $file_no_prefix );
		}

		return false;
	}

	/**
	 * This plugin's directory
	 *
	 * @since  0.1.0
	 * @param  string $path (optional) appended path.
	 * @return string       Directory and path
	 */
	public static function dir( $path = '' ) {
		static $dir;
		$dir = $dir ? $dir : trailingslashit( dirname( __FILE__ ) );
		return $dir . $path;
	}

	/**
	 * This plugin's url
	 *
	 * @since  0.1.0
	 * @param  string $path (optional) appended path.
	 * @return string       URL and path
	 */
	public static function url( $path = '' ) {
		static $url;
		$url = $url ? $url : trailingslashit( plugin_dir_url( __FILE__ ) );
		return $url . $path;
	}
}

/**
 * Grab the MC_ESP_Integrations object and return it.
 * Wrapper for MC_ESP_Integrations::get_instance()
 *
 * @since  0.1.0
 * @return MC_ESP_Integrations  Singleton instance of plugin class.
 */
function mc_esp_integrations() {
	return MC_ESP_Integrations::get_instance();
}

// Kick it off.
add_action( 'plugins_loaded', array( mc_esp_integrations(), 'init' ) );

register_activation_hook( __FILE__, array( mc_esp_integrations(), '_activate' ) );
register_deactivation_hook( __FILE__, array( mc_esp_integrations(), '_deactivate' ) );
