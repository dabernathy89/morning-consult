<?php

function mc_save_post_content( $post_id ) {
	$post_type = get_post_type($post_id);
	$calendar_section_printed = false;

	if( empty($_POST['acf']) ) {
			return;
	}

	if( !empty($GLOBALS['wp_embed']) ) {
			remove_filter( 'acf_the_content', array( $GLOBALS['wp_embed'], 'run_shortcode' ), 8 );
			remove_filter( 'acf_the_content', array( $GLOBALS['wp_embed'], 'autoembed' ), 8 );
	}
	remove_filter( 'acf_the_content', 'capital_P_dangit', 11 );
	remove_filter( 'acf_the_content', 'wptexturize' );
	remove_filter( 'acf_the_content', 'convert_smilies' );
	remove_filter( 'acf_the_content', 'convert_chars' );
	remove_filter( 'acf_the_content', 'wpautop' );
	remove_filter( 'acf_the_content', 'shortcode_unautop' );
	remove_filter( 'acf_the_content', 'prepend_attachment' );
	remove_filter( 'acf_the_content', 'do_shortcode', 11);
	remove_filter( 'acf_the_content', 'wp_make_content_images_responsive');

	// For sponsored content things and such. 'Template Emails'
	if($post_type === "template_email") :
		$new_template_content = "";

		if(have_rows('template_email_content_section')) :
			while(have_rows('template_email_content_section')) : the_row();
				if(get_row_layout() === 'single_column_layout') :
					$single_image = get_sub_field('image_ad');
					$single_image_url = get_sub_field('url_for_image_ad');
					$sizes = morning_consult_calc_image_ratio($single_image['width'], $single_image['height'], 600);
					//mandatory inline width setting for Outlook clients
					$new_template_content .= '<a href="'.$single_image_url.'" target="_blank">';
					$new_template_content .= '<img width="'.$sizes['width'].'" height="'.$sizes['height'].'" src="'.$single_image['url'].'" alt="'.$single_image['alt'].'"/>';
					$new_template_content .= '</a>';
				endif;
				if(get_row_layout() === 'paragraph_section'):
					$text = get_sub_field('text_paragraph');
					$new_template_content .= '<div class="paragraph-text">' . $text . '</div>';
				endif;
			endwhile;
		endif;
		wp_update_post( array(
				'ID' => $post_id,
				'post_content' => $new_template_content
		) );
	endif;

	// Daily Alert Emails
	if ($post_type === "mc_daily_alerts") :
			$new_email_content = "";

			$intro_content = get_field('daily_alerts_intro_content');
			if ($intro_content) {
					$new_email_content .= "<table align='center'><tr><td class='article'>";
					$new_email_content .= "<p>" . $intro_content . "</p>";
					$new_email_content .= "</td></tr><tr><td class='article-spacer'></td></tr></table>";
			}

			if (have_rows('daily_alerts_email_alerts')) :
				while ( have_rows('daily_alerts_email_alerts') ) : the_row();
					if (get_sub_field('section_type') === "Article") :
						$new_email_content .= "<table class='article-wrapper'><tr><td class='article'>";
						if (get_sub_field('title')) :
							$new_email_content .= '<h2>';
								if (get_sub_field('url')) :
									$new_email_content .= '<a href="' . get_sub_field('url') . '">';
								endif;
								$new_email_content .= get_sub_field('title');
								if (get_sub_field('url')) :
									$new_email_content .= "</a>";
								endif;
							$new_email_content .= "</h2>";
						endif;
						if (get_sub_field('date')) :
							$new_email_content .= "<p>" . get_sub_field('date') . "</p>";
						endif;
						if (get_sub_field('excerpt')) :
							$new_email_content .= get_sub_field('excerpt');
						endif;
						$new_email_content .= "</td></tr><tr><td class='article-spacer'></td></tr></table>";
					elseif (get_sub_field('section_type') === "HTML") :
						$new_email_content .= "<table><tr><td class='article-spacer'></td><td>";
						$new_email_content .= get_sub_field('html');
						$new_email_content .= "</td></tr><tr><td class='article-spacer'></td></tr></table>";
					endif;
				endwhile;
			endif;

			if(have_rows('dual_image_ad')) {
				while(have_rows('dual_image_ad')) : the_row();
					$left_image = get_sub_field('left_image');
					$left_link = get_sub_field('left_image_link');
					$right_image = get_sub_field('right_image');
					$right_link = get_sub_field('right_image_link');
					$left_image_sizes = morning_consult_calc_image_ratio($left_image['width'], $left_image['height'], 300);
					$right_image_sizes = morning_consult_calc_image_ratio($right_image['width'], $right_image['height'], 300);

					$new_email_content .= "<table class='mc-dual-image-ad'><tr><td>";
					$new_email_content .= "<a href='" . $left_link . "' target='_blank'><img src='" . $left_image['url'] . "' width='".$left_image_sizes['width']."' height='".$left_image_sizes['height']."' /></a></td>";
					$new_email_content .= "<td><a href='" . $right_link . "' target='_blank'><img src='" . $right_image['url'] . "' width='".$right_image_sizes['width']."' height='".$right_image_sizes['height']."' /></a>";
					$new_email_content .= "</td></tr></table>";
				endwhile;
			}

			wp_update_post( array(
					'ID' => $post_id,
					'post_content' => $new_email_content
			) );
	endif;

	// Morning Briefs
	if ($post_type === "mc_brief" && have_rows('morning_brief_content_sections')) :
		$new_brief_content = "";
		$is_first_title = true;
		$is_inside_article = false;

		while ( have_rows('morning_brief_content_sections') ) : the_row();
			$brief_layout = get_row_layout();

			if ($brief_layout === "conventions_outline") :
				$new_brief_content .= "<table align='center'><tr><td class='article'>";
				$new_brief_content .= "<h2>Conventions Brief Outline</h2>\n";
				if (have_rows('outline_paragraphs')) : while (have_rows('outline_paragraphs')) : the_row();
					$new_brief_content .= "<p>" . get_sub_field('paragraph') . "</p>\n";
				endwhile; endif;
				$new_brief_content .= "</td></tr><tr><td class='article-spacer'></td></tr></table>";
			endif;

			if ($brief_layout === "todays_washington_brief") :
				$new_brief_content .= "<table align='center'><tr><td class='article'>";
				$new_brief_content .= "<h2>Today's Washington Brief</h2>\n";
				if (have_rows('washington_paragraphs')) : while (have_rows('washington_paragraphs')) : the_row();
						$new_brief_content .= "<p>" . get_sub_field('paragraph') . "</p>\n";
				endwhile; endif;
				$new_brief_content .= "</td></tr><tr><td class='article-spacer'></td></tr></table>";
			endif;

			if ($brief_layout === "todays_presidential_brief") :
				$new_brief_content .= "<table align='center'><tr><td class='article'>";
				$new_brief_content .= "<h2>Today's Presidential Brief</h2>\n";
				if (have_rows('presidential_paragraphs')) : while (have_rows('presidential_paragraphs')) : the_row();
					$new_brief_content .= "<p>" . get_sub_field('paragraph') . "</p>\n";
				endwhile; endif;
				$new_brief_content .= "</td></tr><tr><td class='article-spacer'></td></tr></table>";
			endif;

			if ($brief_layout === "todays_congress_brief") :
				$new_brief_content .= "<table align='center'><tr><td class='article'>";
				$new_brief_content .= "<h2>Today's Congress Brief</h2>\n";
				if (have_rows('congress_paragraphs')) : while (have_rows('congress_paragraphs')) : the_row();
					$new_brief_content .= "<p>" . get_sub_field('paragraph') . "</p>\n";
				endwhile; endif;
				$new_brief_content .= "</td></tr><tr><td class='article-spacer'></td></tr></table>";
			endif;

			if ($brief_layout === "todays_business_brief") :
				$new_brief_content .= "<table align='center'><tr><td class='article'>";
				$new_brief_content .= "<h2>Today's Business Brief</h2>\n";
				if (have_rows('business_paragraphs')) : while (have_rows('business_paragraphs')) : the_row();
					$new_brief_content .= "<p>" . get_sub_field('paragraph') . "</p>\n";
				endwhile; endif;
				$new_brief_content .= "</td></tr><tr><td class='article-spacer'></td></tr></table>";
			endif;

			if ($brief_layout === "todays_chart_review") :
				$new_brief_content .= "<table align='center' class='chart-review-wrapper'><tr><td class='article chart-review'>";
				$chart_image = get_sub_field('chart_image');
				$ci_sizes = morning_consult_calc_image_ratio($chart_image['width'], $chart_image['height'], 600);
				$chart_image_url = $chart_image['url'];

				$new_brief_content .= "<h2>" . get_sub_field('chart_title') . "</h2>\n";
				$new_brief_content .= "<div>" . get_sub_field('chart_headline') . "</div>\n";

				$new_brief_content .= "<div style='margin-top: 10px; margin-bottom: 10px;'>";
				$new_brief_content .= "<img width='".$ci_sizes['width']."' height='".$ci_sizes['height']."' src='" . $chart_image_url . "' class='chart-review-img'></div>\n";
				$new_brief_content .= "<div style='clear: both;'></div>\n";
				$new_brief_content .= "</td></tr><tr><td class='article-spacer'></td></tr></table>";
			endif;

			if ($brief_layout === "calendar") :
				$calendar_section_printed = true;
				$new_brief_content .= "<table class='events'><tr><td class='article calendar'>";
				$new_brief_content .= "<h2>" . get_sub_field('calendar_title') . "</h2>";
				if (have_rows('dates')) : while (have_rows('dates')) : the_row();
					$new_brief_content .= "<table class='event-date'>";
						$new_brief_content .= "<tr><td class='event-day' colspan='2'><strong>" . get_sub_field('day') . "</strong></td></tr>";
						if (have_rows('events')) : while (have_rows('events')) : the_row();
							$new_brief_content .= "<tr class='event'>";
							$new_brief_content .= "<td class='event-description'><a href='" . get_sub_field('url') . "'>";
							$new_brief_content .= get_sub_field('description');
							$new_brief_content .= "</a></td>";
							$new_brief_content .= "<td class='event-time'>". get_sub_field('time') . "</td>";
							$new_brief_content .= "</tr>";
						endwhile; endif;
					$new_brief_content .= "</table>";
				endwhile; endif;
				$new_brief_content .= "</td></tr><tr><td class='article-spacer'></td></tr></table>";
			endif;

			if ($brief_layout === "general_section_title") :
					if (!$is_first_title && $is_inside_article) {
							$new_brief_content .= "<table class='general-section-spacer'><tr><td class='top'></td></tr><tr><td class='middle'></td></tr><tr><td class='bottom'></td></tr></table>";
					}

					$new_brief_content .= "<h2>" . get_sub_field('section_title') . "</h2>\n";

					$is_first_title = false;
			endif;

			if ($brief_layout === "general_section_paragraph") :
				$new_brief_content .= "<p>" . get_sub_field('text') . "</p>";
			endif;

			if ($brief_layout === "html_content") :
				$new_brief_content .= get_sub_field('content');
			endif;

			if ($brief_layout === "dual_image_ad") {
				$left_image = get_sub_field('left_image');
				$left_link = get_sub_field('left_image_link');
				$right_image = get_sub_field('right_image');
				$right_link = get_sub_field('right_image_link');

				// temporarily close article section so image can not be inside white box
				if ($is_inside_article) {
					$new_brief_content .= "</td></tr><tr><td class='article-spacer'></td></tr></table>";
				}

				$new_brief_content .= "<table class='mc-dual-image-ad'><tr><td>";
				$new_brief_content .= "<a href='" . $left_link . "' target='_blank'><img src='" . $left_image['url'] . "' /></a></td>";
				$new_brief_content .= "<td><a href='" . $right_link . "' target='_blank'><img src='" . $right_image['url'] . "' /></a>";
				$new_brief_content .= "</td></tr></table>";
			}

			if ($brief_layout === "image_ad") :
				$ad_image = get_sub_field('ad_image');
				$ad_sizes = morning_consult_calc_image_ratio($ad_image['width'], $ad_image['height'], 600);

				// temporarily close article section so image can not be inside white box
				if ($is_inside_article) {
					$new_brief_content .= "</td></tr><tr><td class='article-spacer'></td></tr></table>";
				}

				if (get_sub_field('ad_link')) {
					$new_brief_content .= '<a href="' . get_sub_field('ad_link') . '">';
					$new_brief_content .= '<img src="' . $ad_image['url'] . '" width="'.$ad_sizes['width'].'" height="'.$ad_sizes['height'].'">';
					$new_brief_content .= '</a>';
				} else {
					$new_brief_content .= '<img src="' . $ad_image_url . '" width="'.$ad_sizes['width'].'" height="'.$ad_sizes['height'].'">';
				}
				$new_brief_content .= "<p>&nbsp;</p>\n";

				// reopen article section if it was open before image ad
				if ($is_inside_article) {
					$new_brief_content .= "<table><tr><td class='article'>";
				}
			endif;

			if ($brief_layout === "open_section") :
				$new_brief_content .= "<table class='open-section'><tr><td class='article'>";
				$is_inside_article = true;
				$is_first_title = true;
			endif;

			if ($brief_layout === "close_section") :
				$new_brief_content .= "</td></tr><tr><td class='article-spacer'></td></tr></table>";
				$is_inside_article = false;
			endif;

		endwhile;

		wp_update_post( array(
				'ID' => $post_id,
				'post_content' => $new_brief_content
		) );
	endif;
	wp_reset_postdata();

	// Articles, Opinions, Polls
	if( have_rows('post_content_sections') ):

			$new_content = "";

			while ( have_rows('post_content_sections') ) : the_row();
					$layout = get_row_layout();
					// TODO: change content layout for emails
					switch ($layout) {
							default:
									ob_start();
									locate_template('inc/partials/single/' . $layout . '.php', true, false);
									$new_content .= ob_get_contents();
									ob_end_clean();
									break;
					}

			endwhile;

			wp_update_post( array(
					'ID' => $post_id,
					'post_content' => $new_content
			) );

	endif;
	wp_reset_postdata();

	if( !empty($GLOBALS['wp_embed']) ) {
			add_filter( 'acf_the_content', array( $GLOBALS['wp_embed'], 'run_shortcode' ), 8 );
			add_filter( 'acf_the_content', array( $GLOBALS['wp_embed'], 'autoembed' ), 8 );
	}
	add_filter( 'acf_the_content', 'capital_P_dangit', 11 );
	add_filter( 'acf_the_content', 'wptexturize' );
	add_filter( 'acf_the_content', 'convert_smilies' );
	add_filter( 'acf_the_content', 'convert_chars' );
	add_filter( 'acf_the_content', 'wpautop' );
	add_filter( 'acf_the_content', 'shortcode_unautop' );
	add_filter( 'acf_the_content', 'prepend_attachment' );
	add_filter( 'acf_the_content', 'do_shortcode', 11);
	add_filter( 'acf_the_content', 'wp_make_content_images_responsive');

	remove_filter( 'the_content', 'wp_make_content_images_responsive');

	// Morning Brief Email HTML
	$overwrite_html = false;
	if (($post_type === "mc_brief" || $post_type === "post") && get_field('brief_overwrite_email')) {
		$overwrite_html = true;
	}
	else if ($post_type === "mc_daily_alerts" && get_field('daily_alerts_overwrite_email')) {
		$overwrite_html = true;
	}
	else if ($post_type === "template_email" && get_field('template_email_overwrite_email')) {
		$overwrite_html = true;
	}
	if ($overwrite_html) {

			$brief_content = get_post( $post_id, 'OBJECT', 'display' );
			$brief_content = apply_filters('the_content', $brief_content->post_content);
			$html_content = '';
			// $hubspot_html_content = get_field('brief_hubspot_email_template','option');
			$category_slug = morning_consult_which_category($post_id);

			if ($post_type === "mc_daily_alerts") {
					$html_content = get_field('daily_alerts_email_template','option');
					$hubspot_html_content = get_field('daily_alerts_hubspot_email_template','option');
			}

			if ($post_type === "mc_brief") {
				$html_content = get_field('brief_email_template','option');
				$brief_byline = get_field('morning_brief_byline');
				if ($brief_byline) {
						$brief_byline = "<p>" . $brief_byline . "</p>";
				};
				$title = "";
			} else {
				$brief_byline = "<p>By " . get_the_author() . "</p>";
				$title = "<h2>" . get_the_title($post_id) . "</h2>";
			}

			if ($post_type === "mc_brief") {
				$html_content = str_replace('{{byline}}', $brief_byline, $html_content);
				$html_content = str_replace('{{content}}', $brief_content, $html_content);
				$html_content = str_replace('{{title}}', $title, $html_content);
				$html_content = str_replace('{{mc_category_slug}}', $category_slug, $html_content);
				$html_content = str_replace('{{mc_category_name}}', ucwords($category_slug), $html_content);
				$html_content = str_replace('{{mc_theme_url}}', get_template_directory_uri(), $html_content);
				$html_content = str_replace('{{site_url}}', get_site_url(), $html_content);

				update_field('brief_email_content', $html_content);
			}

			if ($post_type === "mc_daily_alerts") {
				$html_content = str_replace('{{content}}', $brief_content, $html_content);
				$html_content = str_replace('{{title}}', $title, $html_content);
				$html_content = str_replace('{{mc_category_slug}}', $category_slug, $html_content);
				$html_content = str_replace('{{mc_category_name}}', '', $html_content);
				$html_content = str_replace('{{mc_theme_url}}', get_template_directory_uri(), $html_content);
				$html_content = str_replace('{{site_url}}', get_site_url(), $html_content);

				update_field('daily_alerts_email_content', $html_content);
			}
			if($post_type === "template_email") {
				$template_email_type = get_field('template_name');
				$template_email = get_field( $template_email_type, 'option', false);
				$html_content = str_replace('{{content}}', $brief_content, $template_email);

				update_field('template_email_email_content', $html_content);
			}
	}

}
add_action('acf/save_post', 'mc_save_post_content', 100);
