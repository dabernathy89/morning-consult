<?php
/**
 * @package Morning Consult 2015
 */
?>

<?php
	global $post;
	$author_id = get_the_author_meta('ID');
	$post_for_ad = $post;
	$all_authors = get_coauthors();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">

		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<div class="entry-meta">
			<span class="author vcard">
				<?php foreach ($all_authors as $authorkey => $author) : ?>
					<?php include "inc/partials/single/meta_author.php"; ?>
				<?php endforeach; ?>
			</span>&nbsp;&nbsp;<span class="separator">|</span>&nbsp;&nbsp;
			<time class="entry-date published updated" datetime="<?php echo get_the_date( 'c' ); ?>">
				<?php the_date(); ?>
			</time>
			<div class="share-icons">
				<a class="facebook" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(get_the_permalink()); ?>">
					<i class="fa fa-facebook"></i>
				</a>
				<a class="twitter" href="http://twitter.com/share?url=<?php echo urlencode(get_the_permalink()); ?>&text=<?php the_title(); ?>">
					<i class="fa fa-twitter"></i>
				</a>
				<a class="linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_the_permalink()); ?>">
					<i class="fa fa-linkedin"></i>
				</a>
			</div>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<?php if (get_post_type() === "mc_alert") : ?>
        <div class="feature">
					<?php $featured_alert = get_field('alert_feature');
            	if ($featured_alert) : ?>
                <div class="embed-container">
                    <?php the_field('alert_feature'); ?>
                </div>
	          <?php elseif (has_post_thumbnail() && !isset($featured_alert) ) :
                the_post_thumbnail('medium');
		            endif; ?>
        </div>
	<?php endif; ?>

	<div class="entry-content">
		<?php
			if( have_rows('post_content_sections') ):

			    while ( have_rows('post_content_sections') ) : the_row();

					locate_template('inc/partials/single/' . get_row_layout() . '.php', true, false);

			    endwhile;

			else :
				the_content();
			endif;
		?>
	</div><!-- .entry-content -->

	<footer class="single-footer">
		<div class="row share">
			<div class="col-xs-12 single-category-tags">
				<span class="single-category-label">Tags: </span>
				<?php morning_consult_category_links_square(); ?>
			</div>
		</div>

		<?php
			if ($all_authors) : ?>
				<div class="authors clearfix">
				<?php 
					foreach ($all_authors as $author) {
						$author_type = get_class($author);
						if ($author_type === "WP_User") {
							include "inc/partials/single/author_user.php";
						} elseif ($author_type === "stdClass") {
							include "inc/partials/single/author_guest.php";
						}
					}
				?>
				</div>
			<?php endif; ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
