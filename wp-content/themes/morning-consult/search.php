<?php
/**
 * Template name: Search
 *
 * The template for displaying search results pages.
 *
 * @package Morning Consult 2015
 */

get_header(); ?>

	<div class="page archive search">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">

					<header class="page-header">
						<h1 class="page-title">Search</h1>
					</header><!-- .page-header -->

					<?php get_search_form(); ?>

					<?php if ( have_posts() && is_search() ) : ?>

							<h2><?php printf( __( 'Search Results for: %s', 'morning-consult' ), '<span>' . get_search_query() . '</span>' ); ?></h2>

							<?php while ( have_posts() ) : the_post(); ?>

								<?php get_template_part( 'content', 'search' ); ?>

							<?php endwhile; ?>

							<?php mc_the_posts_navigation(); ?>

					<?php elseif (is_search()) : ?>

						<h2><?php printf( __( 'Search Results for: %s', 'morning-consult' ), '<span>' . get_search_query() . '</span>' ); ?></h2>

						<section class="no-results not-found">
							<h3>Nothing Found</h3>
						</section><!-- .no-results -->


					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
