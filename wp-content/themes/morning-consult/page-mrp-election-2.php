<?php /**
* Template Name: Trump/Clinton/Johnson/Stein MRP
* @package Morning Consult 2016
*/
?>

<?php get_header(); ?>

<script type="text/javascript" src="<?php echo get_template_directory_uri() . '/js/highmaps/js/highmaps.src.js'; ?>"></script>
<script src="https://code.highcharts.com/mapdata/countries/us/us-all.js"></script>
<!-- <script type="text/javascript" src="<?php echo get_template_directory_uri() . '/js/fifty-states.js?v=20160412'; ?>"></script> -->
<script src="<?php echo get_template_directory_uri() .'/js/mrp-election-2.js'; ?>"></script>

<?php
$tablepress_tables = json_decode( get_option( 'tablepress_tables', array() ), true );
$table_id_2way = get_field('q3_2016_2way');
$table_id_4way = get_field('q3_2016_4way');
$table_data_2way = get_post_field('post_content', $tablepress_tables['table_post'][$table_id_2way], 'raw');
$table_data_4way = get_post_field('post_content', $tablepress_tables['table_post'][$table_id_4way], 'raw');

$formatted_table_data_2way = mc_mrp_presidential_candidate_data($table_data_2way, 2);
$formatted_table_data_4way = mc_mrp_presidential_candidate_data_4way($table_data_4way, 4);

wp_localize_script('morning-consult-all-js', 'mc_mrp_presidential_candidate_data_2way', $formatted_table_data_2way);
wp_localize_script('morning-consult-all-js', 'mc_mrp_presidential_candidate_data_4way', $formatted_table_data_4way);

global $post;
$author_id = get_the_author_meta('ID');
$post_for_ad = $post;
$all_authors = get_coauthors();

?>

<?php while(have_posts()) : the_post(); ?>

<container class="container">
	<row class="row">
		<main id="main-col" class="col-xs-12 col-sm-6 col-md-7 col-lg-8">
			<img src="https://morningconsult.com/wp-content/uploads/2016/09/HEADERIMAGE.png" alt="" class="heroimage">
			<h1 class="page-title"><?php the_title(); ?></h1>
			<h2 class="page-subhead"><?php the_field('subheading'); ?></h2>

			<div class="entry-meta">
				<span class="author vcard">
					<?php foreach ($all_authors as $authorkey => $author) : ?>
						<?php include "inc/partials/single/meta_author.php"; ?>
					<?php endforeach; ?>
				</span>&nbsp;&nbsp;<span class="separator">|</span>&nbsp;&nbsp;
				<time class="entry-date published updated" datetime="<?php echo get_the_date( 'c' ); ?>">
					<?php the_date(); ?>
				</time>
			</div>

			<div class="main-body"><?php the_content(); ?></div>
			<section id="tabs">
				<ul class="nav nav-tabs" role="tablist">
					<li id="2waytab" role="presentation" class="active"><a href="#2way" data-toggle="tab">Trump v Clinton</a></li>
					<li id="4waytab" role="presentation"><a href="#4way" data-toggle="tab">Trump v Clinton v Johnson v Stein</a></li>
				</ul>
				<div class="tab-content">
				<div id="2way" role="tabpanel" class="tab-pane fade in active">
					<div class="electoral-votes-container">
						<div class="midpoint-label">270 electoral votes to win</div>
						<div class="electoral-votes-bar clearfix" id="electoral-votes-bar-2">
							<div class="votes votes-d">
								<span class="totals totals-d"></span>
							</div>
							<div class="votes votes-r">
								<span class="totals totals-r"></span>
							</div>
							<div class="midpoint"></div>
						</div>
						<div id="mrp-legend" class="hidden-xs">
							<?php
								$fav_colors = array(
									'#0c4a6b' => 'Clinton',
									'#8fbadb' => 'Lean Clinton',
									'#e9d971' => 'Toss-up',
									'#d99c86' => 'Lean Trump',
									'#d33f2f' => 'Trump',
								);
								$fav_legend = create_html_legend($fav_colors);
								echo $fav_legend;
							?>
						</div>
					</div>
					<div id="map-container-2"></div>
					<p class="table-label visible-xs">&laquo; Scroll to View More Columns &raquo;</p>
					<div class="table-container">
						<?php tablepress_print_table('id='.$table_id_2way) ?>
					</div>
				</div>

				<div id="4way" role="tabpanel" class="tab-pane fade in">
					<div class="electoral-votes-container">
						<div class="midpoint-label">270 electoral votes to win</div>
						<div class="electoral-votes-bar clearfix" id="electoral-votes-bar-4">
							<div class="votes votes-d">
								<span class="totals totals-d"></span>
							</div>
							<div class="votes votes-r">
								<span class="totals totals-r"></span>
							</div>
							<div class="midpoint"></div>
						</div>
						<div id="mrp-legend" class="hidden-xs">
							<?php
								$fav_colors = array(
									'#0c4a6b' => 'Clinton',
									'#8fbadb' => 'Lean Clinton',
									'#e9d971' => 'Toss-up',
									'#d99c86' => 'Lean Trump',
									'#d33f2f' => 'Trump',
								);
								$fav_legend = create_html_legend($fav_colors);
								echo $fav_legend;
							?>
						</div>
					</div>
					<div id="map-container-4"></div>
					<p class="table-label visible-xs">&laquo; Scroll to View More Columns &raquo;</p>
					<div class="table-container">
						<?php tablepress_print_table('id='.$table_id_4way) ?>
					</div>
				</div>
			</div>
		</section>
	</main>

	<div class="ticker-container col-xs-12 col-sm-6 col-md-5 col-lg-4">
		<?php get_template_part('/inc/partials/alerts'); ?>
	</div>

	</row>
</container>


<?php endwhile; ?>

<?php get_footer(); ?>
