<?php $tablepress_tables = json_decode( get_option( 'tablepress_tables', array() ), true ); ?>
<?php $matchups = array(); ?>
<?php $matchup_tables_data = array('colors' => array()); ?>

<?php
if ( have_rows( 'matchups' ) ) :
    while ( have_rows( 'matchups' ) ) : the_row();

        $table_id = get_sub_field('data_source');
        if ( $table_id && isset( $tablepress_tables['table_post'][$table_id] ) ) {
            $matchup_tables_data[$table_id] = get_post_field( 'post_content', $tablepress_tables['table_post'][$table_id], 'raw' );
            // var_dump($matchup_tables_data);
            $matchup_tables_data[$table_id] = apply_filters( 'mc_get_matchup_table_data', json_decode( $matchup_tables_data[$table_id], true ) );
            // $matchup_tables_data[$table_id] = apply_filters( 'mc_add_moe_table_data', $matchup_tables_data[$table_id] );
        }

        //remove vs. from map name, use as new key value
        $mapname = explode(' vs. ', get_sub_field('name'));
        $mapname = implode('_', $mapname);

        //insert new object with named table ids
        $matchup_tables_data['table_ids'][$mapname] = $table_id;
        $matchup_tables_data['colors'][$table_id] = get_sub_field('colors');
        $matchup_tables_data = ($matchup_tables_data) ? $matchup_tables_data : array();

        $matchups[] = array(
            'name' => get_sub_field('name'),
            'table_id' => $table_id,
            'map_description' => get_sub_field('map_description')
        );
    endwhile;
endif;

$electoral_votes = array("AL" => 9,"AK" => 3,"AZ" => 11,"AR" => 6,"CA" => 55,"CO" => 9,"CT" => 7,"DE" => 3,"DC" => 3,"FL" => 29,"GA" => 16,"HI" => 4,"ID" => 4,"IL" => 20,"IN" => 11,"IA" => 6,"KS" => 6,"KY" => 8,"LA" => 8,"ME" => 4,"MD" => 10,"MA" => 11,"MI" => 16,"MN" => 10,"MS" => 6,"MO" => 10,"MT" => 3,"NE" => 5,"NV" => 6,"NH" => 4,"NJ" => 14,"NM" => 5,"NY" => 29,"NC" => 15,"ND" => 3,"OH" => 18,"OK" => 7,"OR" => 7,"PA" => 20,"RI" => 4,"SC" => 9,"SD" => 3,"TN" => 11,"TX" => 38,"UT" => 6,"VT" => 3,"VA" => 13,"WA" => 12,"WV" => 5,"WI" => 10,"WY" => 3);

wp_localize_script( 'morning-consult-all-js', 'mc_fifty_states_matchup_data', $matchup_tables_data );
wp_localize_script( 'morning-consult-all-js', 'mc_fifty_states_electoral_votes', $electoral_votes );
?>

<ul class="nav nav-tabs map-tabs" role="tablist">
    <?php foreach ($matchups as $matchup_key => $matchup) : ?>
        <li role="presentation" class="<?php echo ($matchup_key === 0) ? 'active' : ''; ?>">
            <a
                href="#matchup-map-<?php echo $matchup_key; ?>"
                aria-controls="matchup-map-<?php echo $matchup_key; ?>"
                data-map-matchup="<?php echo $matchup['name'] ?>"
                data-matchup-table-id="<?php echo $matchup['table_id']; ?>"
                role="tab"
                data-toggle="tab">
                <?php echo $matchup['name']; ?>
            </a>
        </li>
    <?php endforeach; ?>
    <?php reset($matchups); ?>
</ul>

<div class="tab-content">
    <?php foreach ($matchups as $matchup_key => $matchup) : ?>
      <?php $matchup_name = explode(' vs. ' , $matchup['name']); ?>
      <?php $matchup_name = implode('_', $matchup_name); ?>
        <div
            role="tabpanel"
            class="tab-pane fade <?php echo ($matchup_key === 0) ? 'active in' : ''; ?>"
            id="matchup-map-<?php echo $matchup_key; ?>">
            <div id="electoral-votes-totals-<?php echo $matchup[table_id]; ?>" class="clearfix electoral-votes-totals">
              <span class="totals totals-label">Electoral College</span>
              <span class="totals totals-d"></span>
              <span class="totals totals-r"></span>
            </div>
            <div class="electoral-votes-bar clearfix" id="electoral-votes-bar-<?php echo $matchup['table_id']; ?>" >
                <div class="votes votes-d"></div>
                <div class="votes votes-r"></div>
                <div class="midpoint"></div>
                <div class="midpoint-label">270 to win</div>
            </div>
            <map class="map-wrap" data-map-matchup="<?php echo $matchup_name ?>" data-matchup-table-id="<?php echo $matchup['table_id']; ?>">
            </map>
            <div class="description map-description">
                <?php echo $matchup['map_description']; ?>
            </div>
        </div>
    <?php endforeach; ?>
    <?php reset($matchups); ?>
</div>

<div class="page-text col-xs-12"><?php the_field('page_text'); ?></div>


<ul class="nav nav-tabs table-tabs" role="tablist">
    <?php foreach ($matchups as $matchup_key => $matchup) : ?>
        <li role="presentation" class="<?php echo ($matchup_key === 0) ? 'active' : ''; ?>">
            <a
                href="#matchup-table-<?php echo $matchup_key; ?>"
                aria-controls="matchup-table-<?php echo $matchup_key; ?>"
                role="tab"
                data-toggle="tab">
                <?php echo $matchup['name']; ?>
            </a>
        </li>
    <?php endforeach; ?>
    <?php reset($matchups); ?>
</ul>

<div class="tab-content table-wrap">
<?php foreach ($matchups as $matchup_key => $matchup) : ?>
    <div
        role="tabpanel"
        class="tab-pane <?php echo ($matchup_key === 0) ? 'active in' : ''; ?>"
        id="matchup-table-<?php echo $matchup_key; ?>">

        <?php tablepress_print_table( array(
            'id' => $matchup['table_id'],
            'use_datatables' => true,
            'print_name' => false
        ) ); ?>

    </div>
<?php endforeach; ?>
</div>
