<?php
/**
 * Morning Consult 2015 functions and definitions
 *
 * @package Morning Consult 2016
 */



/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 940; /* pixels */
}

if ( ! function_exists( 'morning_consult_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function morning_consult_setup() {

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => 'Primary Menu',
		'secondary' => 'Secondary Menu',
		'footer' => 'Footer Menu',
		'footer-lower' => 'Footer Lower Menu',
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );
}
endif; // morning_consult_setup
add_action( 'after_setup_theme', 'morning_consult_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function morning_consult_widgets_init() {
	register_sidebar( array(
		'name'					=> 'Subscribe Bar',
		'id'						=> 'subscribe-bar',
		'description'	 => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</aside>',
		'before_title'	=> '',
		'after_title'	 => '',
	) );

	register_sidebar( array(
		'name'					=> 'Footer',
		'id'						=> 'footer-main',
		'description'	 => '',
		'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
		'after_widget'	=> '</div>',
		'before_title'	=> '',
		'after_title'	 => '',
	) );
}
add_action( 'widgets_init', 'morning_consult_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function morning_consult_scripts() {

	$url = get_site_url();
	$version = "20161108-03";

	if (strpos($url, '.dev') !== false || strpos($url, '.staging') !== false || strpos($url, 'localhost') !== false) {
		wp_enqueue_style( 'morning-consult-style', get_template_directory_uri() . "/style-dev.css", array() );
	} else {
		wp_enqueue_style( 'morning-consult-style', get_stylesheet_uri(), array(), $version );
	}

	wp_enqueue_style( 'font-awesome', "//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" );

	wp_enqueue_script( 'jquery' );

	if (strpos($url, '.dev') !== false || strpos($url, '.staging') !== false || strpos($url, 'localhost') !== false) {
		wp_enqueue_script( 'morning-consult-all-js', get_template_directory_uri() . '/js/build/scripts.js', array('jquery'), $version, true );
	} else {
		wp_enqueue_script( 'morning-consult-all-js', get_template_directory_uri() . '/js/build/scripts.min.js', array('jquery'), $version, true );
	}

	if (is_page_template('budget-calculator.php')) {
		if (strpos($url, '.dev') !== false || strpos($url, '.staging') !== false || strpos($url, 'localhost') !== false) {
			wp_enqueue_script( 'vue-js', get_template_directory_uri() . '/js/vue.js', array(), null, true );
		} else {
			wp_enqueue_script( 'vue-js', get_template_directory_uri() . '/js/vue.min.js', array(), null, true );
		}
		wp_enqueue_script( 'mc-budget-calculator', get_template_directory_uri() . '/js/budget-calculator.js', array('jquery','vue-js'), $version, true );
	}

	wp_enqueue_script( 'mc-ads', get_template_directory_uri() . '/js/ads/min/mc-ads.min.js', array('jquery'), $version, true );

}
add_action( 'wp_enqueue_scripts', 'morning_consult_scripts' );

function morning_consult_admin_scripts() {
	wp_enqueue_script( 'morning-brief-send', get_template_directory_uri() . '/js/morning-brief-send.js', array('jquery'), '20160928-01', true );
}
add_action( 'admin_enqueue_scripts', 'morning_consult_admin_scripts' );

/* Morning Brief Image Size */
function mc_image_sizes() {
	add_image_size( 'mc-morning-brief', 600, 2000 );
}
add_action( 'after_setup_theme', 'mc_image_sizes');

function mc_image_sizes_display($sizes) {
	return array_merge($sizes, array(
		'mc-morning-brief' => 'Morning Consult Email'
	));
}
add_filter( 'image_size_names_choose', 'mc_image_sizes_display' );

/*
* Gets the excerpt of a specific post ID or object
* https://pippinsplugins.com/a-better-wordpress-excerpt-by-id-function/
*
* @param - $post - object/int - the ID or object of the post to get the excerpt of
* @param - $length - int - the length of the excerpt in words
* @param - $tags - string - the allowed HTML tags. These will not be stripped out
* @param - $extra - string - text to append to the end of the excerpt
*/
function mc_excerpt_by_id($post, $length = 55, $tags = '', $extra = '...') {

	if(is_int($post)) {
		// get the post object of the passed ID
		$post = get_post($post);
	} elseif(!is_object($post)) {
		return false;
	}

	if(has_excerpt($post->ID)) {
		$the_excerpt = $post->post_excerpt;
		return apply_filters('the_content', $the_excerpt);
	} else {
		$the_excerpt = $post->post_content;
	}

	$the_excerpt = strip_shortcodes(strip_tags($the_excerpt, $tags));
	$the_excerpt = preg_split('/\b/', $the_excerpt, $length * 2+1);
	$excerpt_waste = array_pop($the_excerpt);
	$the_excerpt = implode($the_excerpt);
	$the_excerpt .= $extra;

	return apply_filters('the_content', $the_excerpt);
}

// if excerpt exists, return it unmodified.
// if no excerpt exists, trim to $length and append $extra 'read more'
// if body content is < $length, return unmodified.
function mc_custom_excerpt($ID, $length = 30) {
	$new_excerpt = '';
	if(has_excerpt($ID)) {
		$excerpt = get_the_excerpt($ID);
		return $excerpt;
	}
	else {
		$excerpt = get_the_excerpt($ID);
		$excerpt = explode(' ', $excerpt);
		$count = count($excerpt);
		$extra = '<span>... <a href="'.get_permalink($ID).'" class="read-more">read more</a></span>';

		if($count > $length) {
			for($i = 0; $i < $length; $i++) {
				$new_excerpt .= $excerpt[$i] . ' ';
			}
			$new_excerpt .= $extra;
		}
		else {
			$new_excerpt = implode(' ', $excerpt);
		}

		return $new_excerpt;

	}

	

}

function mc_remove_yoast_metabox() {
	if (get_post_type() === 'mc_daily_alerts') {
		remove_meta_box( 'wpseo_meta', 'mc_daily_alerts', 'normal' );
	}
}
add_action( 'add_meta_boxes', 'mc_remove_yoast_metabox', 11 );

function dequeue_yoast_js() {
	$screen = get_current_screen();
	if ($screen && $screen->base === 'post' && $screen->post_type === 'mc_daily_alerts') {
		wp_deregister_script('yoast-seo-admin-global-script');
		wp_deregister_script('yoast-seo-admin-media');
		wp_deregister_script('yoast-seo-metabox');
		wp_deregister_script('yoast-seo-primary-category');
		wp_deregister_script('yoast-seo-select2');
		wp_deregister_script('yoast-seo-select2-translations');
		wp_deregister_script('yoast-seo-post-scraper');
		wp_deregister_script('yoast-seo-replacevar-plugin');
		wp_deregister_script('yoast-seo-shortcode-plugin');
	}
}
add_action('admin_enqueue_scripts', 'dequeue_yoast_js', 500);


add_filter('gform_field_value_date_entered', 'date_entered_on_email_capture');
function date_entered_on_email_capture() {
	return date('m/d/Y');
}

/** 
* Webhook integrations with MCI
*/
require get_template_directory() . '/inc/mci-webhooks.php';
/**
 * Bootstrap nav walker
 */
require get_template_directory() . '/inc/wp-bootstrap-navwalker.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Custom post types
 */
require get_template_directory() . '/inc/post-types.php';

/**
 * ACF post save hook
 */
require get_template_directory() . '/inc/acf-single-content.php';

/**
 * ACF post save hook for home page featured posts
 */
require get_template_directory() . '/inc/acf-featured-post.php';

/**
 * Shortcodes
 */
require get_template_directory() . '/inc/shortcodes/rankings-table.php';
$mc_rankings_table_shortcode = new MCRankingsTable();

/**
 * ACF after save hook for candidates
 */
require get_template_directory() . '/inc/acf-candidate-after-save.php';

/**
 * Afternoon Email
 */
require get_template_directory() . '/inc/daily-alerts-email.php';
