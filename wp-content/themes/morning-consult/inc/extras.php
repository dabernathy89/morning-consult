<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package Morning Consult 2015
 */

/* Name mappings for states, to be used globally if needed */
$states_to_abbreviations = array(
		'Alabama'=>'AL',
		'Alaska'=>'AK',
		'Arizona'=>'AZ',
		'Arkansas'=>'AR',
		'California'=>'CA',
		'Colorado'=>'CO',
		'Connecticut'=>'CT',
		'Delaware'=>'DE',
		'Florida'=>'FL',
		'Georgia'=>'GA',
		'Hawaii'=>'HI',
		'Idaho'=>'ID',
		'Illinois'=>'IL',
		'Indiana'=>'IN',
		'Iowa'=>'IA',
		'Kansas'=>'KS',
		'Kentucky'=>'KY',
		'Louisiana'=>'LA',
		'Maine'=>'ME',
		'Maryland'=>'MD',
		'Massachusetts'=>'MA',
		'Michigan'=>'MI',
		'Minnesota'=>'MN',
		'Mississippi'=>'MS',
		'Missouri'=>'MO',
		'Montana'=>'MT',
		'Nebraska'=>'NE',
		'Nevada'=>'NV',
		'New Hampshire'=>'NH',
		'New Jersey'=>'NJ',
		'New Mexico'=>'NM',
		'New York'=>'NY',
		'North Carolina'=>'NC',
		'North Dakota'=>'ND',
		'Ohio'=>'OH',
		'Oklahoma'=>'OK',
		'Oregon'=>'OR',
		'Pennsylvania'=>'PA',
		'Rhode Island'=>'RI',
		'South Carolina'=>'SC',
		'South Dakota'=>'SD',
		'Tennessee'=>'TN',
		'Texas'=>'TX',
		'Utah'=>'UT',
		'Vermont'=>'VT',
		'Virginia'=>'VA',
		'Washington'=>'WA',
		'West Virginia'=>'WV',
		'Wisconsin'=>'WI',
		'Wyoming'=>'WY'
	);

	$governors_to_party = array(
		"Charlie Baker" => "R",
		"Larry Hogan" => "R",
		"Matt Mead" => "R",
		"Jack Markell" => "D",
		"Gary Herbert" => "R",
		"Bill Haslam" => "R",
		"Jack Dalrymple" => "R",
		"Bill Walker" => "I",
		"Brian Sandoval" => "R",
		"Asa Hutchinson" => "R",
		"Scott Walker" => "R",
		"Mary Fallin" => "R",
		"Gina Raimondo" => "D",
		"Paul LePage" => "R",
		"Chris Christie" => "R",
		"Bruce Rauner" => "R",
		"Matt Bevin" => "R",
		"Rick Snyder" => "R",
		"Dan Malloy" => "D",
		"Sam Brownback" => "R",
		"Dennis Daugaard" => "R",
		"Steve Bullock" => "D",
		"Greg Abbott" => "R",
		"Andrew Cuomo" => "D",
		"Robert Bentley" => "R",


	);
	$senators_to_party = array(
		"Thomas Carper" => "D",
		"Richard Durbin" => "D",
		"Patrick Leahy" => "D",
		"Joe Manchin" => "D",
		"Claire McCaskill" => "D",
		"John Reed" => "D",
		"Harry Reid" => "D",
		"Bernard Sanders" => "D",
		"Charles Schumer" => "D",
		"Brian Schatz" => "D",
		"Susan Collins" => "R",
		"Lindsey Graham" => "R",
		"John Hoeven" => "R",
		"Mark Kirk" => "R",
		"Mitch McConnell" => "R",
		"Lisa Murkowski" => "R",
		"Pat Roberts" => "R",
		"John McCain" => "R",
		"Marco Rubio" => "R",
		"John Thune" => "R",
		"David Vitter" => "R"
	);

	$mc_global_post_categories = array(
		'tech',
		'health',
		'energy',
		'finance',
		'congress',
		'campaigns',
		'general',
		'whitehouse',
		'polling',
		'washington',
		'brands'
	);

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function morning_consult_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	return $classes;
}
add_filter( 'body_class', 'morning_consult_body_classes' );

function morning_consult_get_featured_image_info($id = null) {
    if ( !is_null($id) || has_post_thumbnail() ) {
        $image_id = is_null($id) ? get_post_thumbnail_id() : $id;
        $image_url = wp_get_attachment_url( $image_id );
        $image_caption = get_post_field('post_excerpt', $image_id, 'display');
        return array(
            'url' => $image_url,
            'caption' => $image_caption
        );
    }

    return null;
}

function morning_consult_calc_image_ratio($width, $height, $width_limit = 600) {
	$sizes = array(
		'width' => $width,
		'height' => $height
	);
	// if width > width_limit then 
	// calculate height ratio and overwrite $sizes with new values
	if($width > $width_limit) {
		$new_width = $width_limit;
		$ratio = $width / $width_limit;
		$new_height = round($height / $ratio);
		$sizes['width'] = $new_width;
		$sizes['height'] = $new_height;
	}
	return $sizes;
}

function morning_consult_get_featured_image_by_size($size = 'thumbnail') {
    $thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id(), $size);
    return is_array($thumb_url) ? $thumb_url[0] : "";
}

// returns primary category of the post (if more than one)
// otherwise returns first category (alphabetical)
function morning_consult_get_primary_category($post_id) {
	$category = get_the_category($post_id);
	if ( $category ) {
		if ( class_exists('WPSEO_Primary_Term') ) {
			// Show the post's 'Primary' category, if this Yoast feature is available, & one is set
			$term_id = get_post_meta($post_id, '_yoast_wpseo_primary_category', true);
			$term = get_term( $term_id );
			if (is_wp_error($term)) {
				// Default to first category (not Yoast) if an error is returned
				return $category[0]->name;
			}
			else {
				// Yoast Primary category
				return $term->name;
			}
		}
		else {
			// Default, return the first category in WP's list of assigned categories
			return $category[0]->name;
		}
	}
	else return null;
}

// Grab the slug of the first category which is in the list of main categories
function morning_consult_which_category($post_id = null) {
	global $mc_global_post_categories;
	global $post;
    if (is_null($post_id)) {
        $post_id = $post->ID;
    }
	$main_categories = $mc_global_post_categories;
	$categories = get_the_category($post_id);

	foreach($categories as $category) {
		if (in_array($category->slug, $main_categories)) {
			return $category->slug;
		}
	}

	return null;
}

// return the links of all the categories which are in the list of main categories
function morning_consult_category_links($post_id = null, $separator = ", ") {
	global $mc_global_post_categories;
  global $post;
  $html = "";
  if (is_null($post_id)) {
      $post_id = $post->ID;
  }

  $main_categories = $mc_global_post_categories;
  $all_categories = get_the_category($post_id);

  foreach($all_categories as $category) {
    if (in_array($category->slug, $main_categories)) {
      $html .= '<a class="category-bold category ' . $category->slug . '" href="/' . $category->slug . '">' . $category->name . '</a>' . $separator;
    }
  }

  $html = trim($html,$separator);

  echo $html;
}

// return the links of all the categories which are in the list of main categories
function morning_consult_category_links_square($post_id = null) {
		global $mc_global_post_categories;
    global $post;
    $html = "";
    if (is_null($post_id)) {
        $post_id = $post->ID;
    }

    $main_categories = $mc_global_post_categories;
    $all_categories = get_the_category($post_id);

    foreach($all_categories as $category) {
        if (in_array($category->slug, $main_categories)) {
					$html .= '<span class="category ' . $category->slug. '" >';
          $html .= '<a href="/' . $category->slug . '" >';
          $html .= $category->name;
          $html .= '</a>';
					$html .= '</span>';
        }
    }

    echo $html;
}

// return the slugs, space separated, of all the categories which are in the list of main categories
function morning_consult_which_categories($post_id = null) {
	global $mc_global_post_categories;
  global $post;
  if (is_null($post_id)) {
      $post_id = $post->ID;
  }
  $main_categories = $mc_global_post_categories;
  $categories = get_the_category($post_id);
  $category_slugs = array();

  foreach($categories as $category) {
      if (in_array($category->slug, $main_categories)) {
          $category_slugs[] = $category->slug;
      }
  }

  return implode(' ', $category_slugs);
}

// return the IDs of all the categories which are in the list of main categories
function morning_consult_which_categories_ids($post_id = null) {
	global $mc_global_post_categories;
  global $post;
  if (is_null($post_id)) {
      $post_id = $post->ID;
  }
  $main_categories = $mc_global_post_categories;
  $categories = get_the_category($post_id);
  $category_ids = array();

  foreach($categories as $category) {
      if (in_array($category->slug, $main_categories)) {
          $category_ids[] = $category->cat_ID;
      }
  }

  return $category_ids;
}

// Custom 'more' tag for excerpts
// add_filter('excerpt_more', 'morning_consult_custom_excerpt');
// function morning_consult_custom_excerpt($more) {
//     return '... <a class="read-more" href="'. get_permalink($post->ID) . '">read more</a>';
// }

if ( version_compare( $GLOBALS['wp_version'], '4.1', '<' ) ) :
	/**
	 * Filters wp_title to print a neat <title> tag based on what is being viewed.
	 *
	 * @param string $title Default title text for current view.
	 * @param string $sep Optional separator.
	 * @return string The filtered title.
	 */
	function morning_consult_wp_title( $title, $sep ) {
		if ( is_feed() ) {
			return $title;
		}

		global $page, $paged;

		// Add the blog name
		$title .= get_bloginfo( 'name', 'display' );

		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) ) {
			$title .= " $sep $site_description";
		}

		// Add a page number if necessary:
		if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
			$title .= " $sep " . sprintf( __( 'Page %s', 'morning-consult' ), max( $paged, $page ) );
		}

		return $title;
	}
	add_filter( 'wp_title', 'morning_consult_wp_title', 10, 2 );

	/**
	 * Title shim for sites older than WordPress 4.1.
	 *
	 * @link https://make.wordpress.org/core/2014/10/29/title-tags-in-4-1/
	 * @todo Remove this function when WordPress 4.3 is released.
	 */
	function morning_consult_render_title() {
		?>
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		<?php
	}
	add_action( 'wp_head', 'morning_consult_render_title' );
endif;

add_filter('upload_mimes', 'morning_consult_allow_svg');
function morning_consult_allow_svg($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}

add_filter("gform_submit_button", "morning_consult_alter_form_submit_button", 10, 2);
function morning_consult_alter_form_submit_button($button, $form){
    if ($form["button"]["type"] == "text") {
        // (class=) followed by (single or double quote) followed by (anything that is not a single or double quote)
        $pattern = '/(class=)(\'|")([^\'"]+)/';
        $replacement = '${1}${2}${3} btn btn-default';
        $newbutton = preg_replace($pattern, $replacement, $button);
        if ( !is_null($newbutton) ) {
            $button = $newbutton;
        }
    }
    return $button;
}

add_filter("gform_init_scripts_footer", create_function("","return true;"));
add_filter( 'gform_confirmation_anchor', create_function("","return false;"));

add_filter("gform_ajax_spinner_url", "morning_consult_gf_spinner_url", 10, 2);
function morning_consult_gf_spinner_url($image_src, $form){
    return get_template_directory_uri() . '/img/ajax-loader.png';
}

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

// Lets us use this URL format: example.com/custom-post-type/category/category-name/
add_action('init', 'category_cpt_rewrites');
function category_cpt_rewrites() {
    $custom_post_types = array('opinions' => 'mc_opinion', 'briefs' => 'mc_brief', 'polls' => 'mc_poll');
    foreach ( $custom_post_types as $key => $post_type ) {
        $rule = '^' . $key . '\/category\/(.+?)\/?(page)?\/?(\d*)?\/?$';
        $rewrite = 'index.php?post_type=' . $post_type . '&category_name=$matches[1]&paged=$matches[3]';
        add_rewrite_rule($rule,$rewrite,'top');
    }
}

add_action('pre_get_posts','morning_consult_archive_filter');
function morning_consult_archive_filter($query) {
	if ( !is_admin() && $query->is_main_query() ) {
		if ($query->is_search() || $query->is_author()) {
			$query->set('post_type', array( 'post', 'mc_brief', 'mc_poll', 'mc_opinion' ) );
		}
	}
}

add_filter( 'excerpt_length', 'morning_consult_excerpt_length', 20 );
function morning_consult_excerpt_length( $length ) {
	return 55;
}

add_filter( 'user_contactmethods', 'morning_consult_user_contact_methods' );
function morning_consult_user_contact_methods( $user_contact ) {

    /* Add user contact methods */
    $user_contact['twitter'] = __( 'Twitter Username' );

    return $user_contact;
}

/* Don't display briefs on author archive pages */
add_action( 'pre_get_posts', 'mc_remove_briefs_from_author_archive' );
function mc_remove_briefs_from_author_archive( $query ) {
    if (!is_admin() && $query->is_main_query() && is_author() && $query->is_author) {
        $post_types = $query->get('post_type');

        foreach ($post_types as $key => $value) {
            if ($value === 'mc_brief') {
                unset($post_types[$key]);
            }
        }

        $query->set('post_type',$post_types);
    }
}

add_filter( 'request', 'mc_rewrite_filter_request' );
function mc_rewrite_filter_request( $vars ) {
    if( isset($vars['email_print']) ) {
        $vars['email_print'] = true;
    }
    return $vars;
}

add_action( 'init', 'mc_rewrite_add_rewrites' );
function mc_rewrite_add_rewrites() {
    add_rewrite_endpoint( 'email_print', EP_PERMALINK );
}

add_filter( 'template_include', 'mc_brief_email_template', 99 );
function mc_brief_email_template( $template ) {
    if( is_singular() && get_query_var( 'email_print' ) ) {
        $new_template = locate_template( array( 'page-brief-email.php' ) );
        if ( '' != $new_template ) {
            return $new_template ;
        }
    }
    return $template;
}

add_filter( 'mc_get_matchup_table_data', 'mc_format_fifty_states_table_data');
function mc_format_fifty_states_table_data( $table_data = array() ) {
    foreach ($table_data as $key => &$val) {
        if ($key === 0) {
            $val[] = "Winner Value";
            $val[] = "Winner Name";
            $val[] = "Winner Index";
        }
        if ($key > 0) {
            $val = mc_fifty_states_matchup_add_winner( $val, $table_data[0] );
        }
    }
    return $table_data;
}

function mc_fifty_states_matchup_add_winner( $row, $column_names ) {
    $dem = (float)str_replace('?', '', $row[1]);
    $rep = (float)str_replace('?', '', $row[2]);
    $winner = ( $dem > $rep ) ? $dem : $rep;

    if ( $dem === $rep ) {
        $winner = 'tie';
        $row[] = 0; // value of winner
        $row[] = 'tie'; // name of winner
        $row[] = null; // index of winner
    }

    foreach ($row as $key => $value) {
        $float_val = (float)str_replace('?', '', $value);
        if ($winner === $float_val && $winner !== 'tie') {
            $row[] = $value; // value of winner
            $row[] = $column_names[$key]; // name of winner
            $row[] = $key; // index of winner
        }
    }
    return $row;
}

function mc_build_polling_page_iframe($num = '6') {
	$urls_to_call = array();
	$returned_html = array();


	// start index at 2 because of bug in returned data
	// so, we're skipping over that result.
	for($url = 2; $url <= $num +1; $url++) {
		$obj = array(
			'html' => 'https://morningconsultintelligence.com/www/latest/question_'.$url.'.html',
			'button' => 'https://morningconsultintelligence.com/latest/question_'.$url,
			'img' => 'https://morningconsultintelligence.com/www/latest/question_'.$url.'.png',
			'num' => $num
		);
		$urls_to_call[] = $obj;
	}
	foreach($urls_to_call as $iframe) {
		$chart = array(
			'button' => $iframe['button'],
			'img' => $iframe['img'],
			'num' => $iframe['num']
		);
		$html = get_transient( md5( 'mc_poll_' . $iframe['html']) );
		if($html !== false) {
			$chart['html'] = $html;
			$returned_html[] = $chart;
		}

		if($html === false) {
			$response = wp_remote_get( $iframe['html'] );
			if( !is_wp_error($response) ) {
				$html = wp_remote_retrieve_body($response);
				if($html) {
					set_transient( md5( 'mc_poll_' . $iframe['html'] ), $html, HOUR_IN_SECONDS);
					$chart['html'] = $html;
					$returned_html[] = $chart;
				}
			}
		}
	}
	return $returned_html;
}

function mc_senate_rankings_top10($table_data) {


	function sort10($a, $b) {
		if($a['sortRating'] === $b['sortRating']) {
			return 0;
		}
		return ($a['sortRating'] > $b['sortRating']) ? -1 : 1;
	}

	function convert_state_to_abbr($state) {
		global $states_to_abbreviations;
		$newstate = $states_to_abbreviations[$state];
		return $newstate;
	}

function get_photo_url($name, $color = true) {
	if($color === true) {
		$color = 'color';
	}
	else {
		$color = 'plain';
	}
		$base = 'http://mcistorage.s3-website-us-east-1.amazonaws.com/q1-2016-senate-rankings/';
		$name = explode(' ', $name);
		$name = implode('_',$name);
		$name = strtolower($name);
		$full = $base . $name . '_' . $color . '.png';
		return $full;
	}

	foreach($table_data as $key => $val) {
		if($key > 0) {
			$approval[$key]['name'] = $table_data[$key][5];
			$approval[$key]['state'] = convert_state_to_abbr($table_data[$key][0]);
			$approval[$key]['party'] = $table_data[$key][6];
			$approval[$key]['moe'] = $table_data[$key][4];
			$approval[$key]['approval'] = trim($table_data[$key][1], '%');
			$approval[$key]['sortRating'] = trim($table_data[$key][1], '%');
			$approval[$key]['disapproval'] = trim($table_data[$key][2], '%');
			$approval[$key]['unknown'] = trim($table_data[$key][3], '%');
			$approval[$key]['photo'] = get_photo_url($table_data[$key][5], false);
			$approval[$key]['link'] = 'https://morningconsultintelligence.com/examine?v=djE2ZDE%3D&d=MTYwNDA3&s=bW9ybmluZyBjb25zdWx0';
			$approval[$key]['objName'] = 'Approval';

			$disapproval[$key]['name'] = $table_data[$key][5];
			$disapproval[$key]['state'] = convert_state_to_abbr($table_data[$key][0]);
			$disapproval[$key]['party'] = $table_data[$key][6];
			$disapproval[$key]['moe'] = $table_data[$key][4];
			$disapproval[$key]['sortRating'] = trim($table_data[$key][2], '%');
			$disapproval[$key]['approval'] = trim($table_data[$key][1], '%');
			$disapproval[$key]['disapproval'] = trim($table_data[$key][2], '%');
			$disapproval[$key]['unknown'] = trim($table_data[$key][3], '%');
			$disapproval[$key]['photo'] = get_photo_url($table_data[$key][5], false);
			$disapproval[$key]['link'] = 'https://morningconsultintelligence.com/examine?v=djE2ZDE%3D&d=MTYwNDA3&s=bW9ybmluZyBjb25zdWx0';
			$disapproval[$key]['objName'] = 'Disapproval';

			$unknown[$key]['name'] = $table_data[$key][5];
			$unknown[$key]['state'] = convert_state_to_abbr($table_data[$key][0]);
			$unknown[$key]['party'] = $table_data[$key][6];
			$unknown[$key]['moe'] = $table_data[$key][4];
			$unknown[$key]['sortRating'] = trim($table_data[$key][3], '%');
			$unknown[$key]['approval'] = trim($table_data[$key][1], '%');
			$unknown[$key]['disapproval'] = trim($table_data[$key][2], '%');
			$unknown[$key]['unknown'] = trim($table_data[$key][3], '%');
			$unknown[$key]['photo'] = get_photo_url($table_data[$key][5], false);
			$unknown[$key]['link'] = 'https://morningconsultintelligence.com/examine?v=djE2ZDE%3D&d=MTYwNDA3&s=bW9ybmluZyBjb25zdWx0';
			$unknown[$key]['objName'] = 'Undecided';

		}
	}

	usort($approval, 'sort10');
	usort($disapproval, 'sort10');
	usort($unknown, 'sort10');

	$top10 = array_slice($approval, 0, 10);
	$bottom10 = array_slice($disapproval, 0, 10);
	$unknown10 = array_slice($unknown, 0, 10);

	$alltop['top10'] = $top10;
	$alltop['bottom10'] = $bottom10;
	$alltop['unknown10'] = $unknown10;

	return $alltop;
}

function mc_governor_table_data($data) {

	function get_photo_url($name) {
		$name = strtolower($name);
		$name = explode(' ', $name);
		$name = implode('-', $name);
		$base = 'http://mcistorage.s3-website-us-east-1.amazonaws.com/governor-photos/';
		$full = $base . $name . '.png';
		return $full;

	}

	function convert_state_to_abbr($state, $short = false) {
		global $states_to_abbreviations;
		$newstate = $states_to_abbreviations[$state];
		if($short === true) {
			return $newstate;
		}
		else {
			return strtolower('us-'.$newstate);
		}
	}

	function get_party_name($name) {
		global $governors_to_party;
		$party = '';
		if(isset($governors_to_party[$name])) {
			$party = $governors_to_party[$name];
		}
		return $party;
	}

	function sort10ApprovalByNet($a, $b) {
		if($a['approve'] === $b['approve']) {
			return ( ($a['approve'] - $a['disapprove']) > ($b['approve'] - $b['disapprove']) ) ? -1 : 1;
		}
		return ( $a['sortRating'] > $b['sortRating'] ) ? -1 : 1;
	}

	function sort10DisapprovalByNet($a, $b) {
		if($a['disapprove'] === $b['disapprove']) {
			return ( ($a['disapprove'] - $a['approve']) > ($b['disapprove'] - $b['approve']) ) ? -1 : 1;
		}
		return ($a['disapprove'] > $b['disapprove']) ? -1 : 1;
	}

	$data = json_decode($data);
	$gov_data = array();

	foreach($data as $key => $val) {
		if($key > 0) {
			$gov_data['raw_data'][] = array(
				'state' => $data[$key][0],
				'governor' => $data[$key][1],
				'approve' => trim($data[$key][2], '%'),
				'disapprove' => trim($data[$key][3], '%'),
				'unknown' => trim($data[$key][4], '%'),
				'value' => trim($data[$key][2], '%'),
				'moe' => trim($data[$key][5], '%'),
				'name' => $data[$key][0],
				'hc-key' => convert_state_to_abbr($data[$key][0]),
				'party' => get_party_name($data[$key][1]),
				'stateAbbr' => convert_state_to_abbr($data[$key][0], true)
			);
			$approve[] = array(
				'state' => $data[$key][0],
				'governor' => $data[$key][1],
				'approve' => trim($data[$key][2], '%'),
				'disapprove' => trim($data[$key][3], '%'),
				'sortRating' => trim($data[$key][2], '%'),
				'unknown' => trim($data[$key][4], '%'),
				'moe' => trim($data[$key][5], '%'),
				'photo' => get_photo_url($data[$key][1]),
				'hc-key' => convert_state_to_abbr($data[$key][0]),
				'party' => get_party_name($data[$key][1]),
				'stateAbbr' => convert_state_to_abbr($data[$key][0], true)
			);
			$disapprove[] = array(
				'state' => $data[$key][0],
				'governor' => $data[$key][1],
				'approve' => trim($data[$key][2], '%'),
				'disapprove' => trim($data[$key][3], '%'),
				'sortRating' => trim($data[$key][3], '%'),
				'unknown' => trim($data[$key][4], '%'),
				'moe' => trim($data[$key][5], '%'),
				'photo' => get_photo_url($data[$key][1]),
				'hc-key' => convert_state_to_abbr($data[$key][0]),
				'party' => get_party_name($data[$key][1]),
				'stateAbbr' => convert_state_to_abbr($data[$key][0], true)
			);
		}
	}

	usort($approve, 'sort10ApprovalByNet');
	usort($disapprove, 'sort10DisapprovalByNet');

	$approve = array_slice($approve, 0, 10);
	$disapprove = array_slice($disapprove, 0, 10);

	$gov_data['top10'] = $approve;
	$gov_data['bottom10'] = $disapprove;


	return $gov_data;
}

function mc_senator_ratings_table_data_v2($data) {
	
	function get_photo_url($name) {
		$name = strtolower($name);
		$name = explode(' ', $name);
		$name = implode('-', $name);
		$base = 'http://mcistorage.s3-website-us-east-1.amazonaws.com/q3-2016-senate-rankings/';
		$full = $base . $name . '.png';
		return $full;
	}

	function convert_state_to_abbr($state, $short = false) {
		global $states_to_abbreviations;
		$newstate = $states_to_abbreviations[$state];
		if($short === true) {
			return $newstate;
		}
		else {
			return strtolower('us-'.$newstate);
		}
	}

	function get_party_name($name) {
		global $senators_to_party;
		$party = '';
		if(isset($senators_to_party[$name])) {
			$party = $senators_to_party[$name];
		}
		return $party;
	}

	function sort10ApprovalByNet($a, $b) {
		if($a['approve'] === $b['approve']) {
			return ( ($a['approve'] - $a['disapprove']) > ($b['approve'] - $b['disapprove']) ) ? -1 : 1;
		}
		return ( $a['sortRating'] > $b['sortRating'] ) ? -1 : 1;
	}

	function sort10DisapprovalByNet($a, $b) {
		if($a['disapprove'] === $b['disapprove']) {
			return ( ($a['disapprove'] - $a['approve']) > ($b['disapprove'] - $b['approve']) ) ? -1 : 1;
		}
		return ($a['disapprove'] > $b['disapprove']) ? -1 : 1;
	}

	$data = json_decode($data);
	$sen_data = array();

	foreach($data as $key => $val) {
		if($key > 0) {
			$sen_data['raw_data'][] = array(
				'state' => $data[$key][0],
				'governor' => $data[$key][1],
				'approve' => trim($data[$key][2], '%'),
				'disapprove' => trim($data[$key][3], '%'),
				'unknown' => trim($data[$key][4], '%'),
				'value' => trim($data[$key][2], '%'),
				'moe' => trim($data[$key][5], '%'),
				'name' => $data[$key][0],
				'party' => get_party_name($data[$key][1]),
				'stateAbbr' => convert_state_to_abbr($data[$key][0], true)
			);
			$approve[] = array(
				'state' => $data[$key][0],
				'governor' => $data[$key][1],
				'approve' => trim($data[$key][2], '%'),
				'disapprove' => trim($data[$key][3], '%'),
				'sortRating' => trim($data[$key][2], '%'),
				'unknown' => trim($data[$key][4], '%'),
				'moe' => trim($data[$key][5], '%'),
				'photo' => get_photo_url($data[$key][1]),
				'party' => get_party_name($data[$key][1]),
				'stateAbbr' => convert_state_to_abbr($data[$key][0], true)
			);
			$disapprove[] = array(
				'state' => $data[$key][0],
				'governor' => $data[$key][1],
				'approve' => trim($data[$key][2], '%'),
				'disapprove' => trim($data[$key][3], '%'),
				'sortRating' => trim($data[$key][3], '%'),
				'unknown' => trim($data[$key][4], '%'),
				'moe' => trim($data[$key][5], '%'),
				'photo' => get_photo_url($data[$key][1]),
				'party' => get_party_name($data[$key][1]),
				'stateAbbr' => convert_state_to_abbr($data[$key][0], true)
			);
		}
	}

	usort($approve, 'sort10ApprovalByNet');
	usort($disapprove, 'sort10DisapprovalByNet');

	$approve = array_slice($approve, 0, 10);
	$disapprove = array_slice($disapprove, 0, 10);

	$sen_data['top10'] = $approve;
	$sen_data['bottom10'] = $disapprove;

	return $sen_data;
}

function create_html_legend($color_array) {
	//color_array should be an array with values like:
	// '#hexcolor' => str or int value
	// '#FFFFFF' => 60
	// '#FFFFFF' => '> 55%'
	$html = '';
	foreach($color_array as $color => $value) {
		$html .= '<span class="html-legend-color" style="background-color: '.$color.'"></span><span class="html-legend-value">'.$value.'</span>';
	}
	return '<div class="html-legend">' . $html . '</div>';

}

function mc_presidential_candidate_table_data($table_data) {

	function compute_color($value, $redorgreen = 'red') {
		$value = floatval($value);

		$categories = array( 70,60,50,40,30 );
		$colors = array(
			'red' => array('#a63603', '#e6550d', '#fd8d3c', '#fdae6b', '#fdd0a2', '#feedde'),
			'green' => array('#006837', '#31a354', '#78c679', '#addd8e', '#d9f0a3', '#ffffcc')
		);

		foreach($categories as $idx => $cat_val) {
			if($value > $cat_val) {
				return $colors[$redorgreen][$idx];
			}
			else if($value < 30) {
				return $colors[$redorgreen][5];
			}
		}
	}

	function get_winner_value($clinton, $trump) {
		$clinton = floatval($clinton);
		$trump = floatval($trump);

		if($clinton > $trump) {
			return $clinton;
		}
		else return $trump;

	}

	function format_table_data($data) {
		$data = json_decode($data);
		foreach($data as $key => $value) {
			if($key > 0) {
				$clinton_fav[] = array(
					'hc-key' => 'us-' . strtolower($data[$key][0]),
					'value' => floatval(trim($data[$key][1], '%')),
					'clinton_favorable' => trim($data[$key][1], '%'),
					'clinton_unfavorable' => trim($data[$key][3], '%'),
					'least_fav' => $data[$key][5],
					'clinton_dk' => trim($data[$key][8], '%'),
					'clinton_moe' => trim($data[$key][9], '%'),
					'clinton_within_moe' => trim($data[$key][11], '%'),
					'statename' => $data[$key][0],
					'color' => compute_color(trim($data[$key][1], '%'), 'green')
				);
				$clinton_unfav[] = array(
					'hc-key' => 'us-' . strtolower($data[$key][0]),
					'value' => trim($data[$key][3], '%'),
					'clinton_favorable' => trim($data[$key][1], '%'),
					'clinton_unfavorable' => trim($data[$key][3], '%'),
					'least_fav' => $data[$key][5],
					'clinton_dk' => trim($data[$key][8], '%'),
					'clinton_moe' => trim($data[$key][9], '%'),
					'clinton_within_moe' => trim($data[$key][11], '%'),
					'statename' => $data[$key][0],
					'color' => compute_color(trim($data[$key][3], '%'), 'red')
				);
				$trump_fav[] = array(
					'hc-key' => 'us-' . strtolower($data[$key][0]),
					'value' => floatval(trim($data[$key][2], '%')),
					'trump_favorable' => trim($data[$key][2], '%'),
					'trump_unfavorable' => trim($data[$key][4], '%'),
					'least_fav' => $data[$key][5],
					'trump_dk' => trim($data[$key][7], '%'),
					'trump_moe' => trim($data[$key][10], '%'),
					'trump_within_moe' => trim($data[$key][12], '%'),
					'statename' => $data[$key][0],
					'color' => compute_color(trim($data[$key][2], '%'), 'green')
				);
				$trump_unfav[] = array(
					'hc-key' => 'us-' . strtolower($data[$key][0]),
					'value' => trim($data[$key][4], '%'),
					'trump_favorable' => trim($data[$key][2], '%'),
					'trump_unfavorable' => trim($data[$key][4], '%'),
					'least_fav' => $data[$key][5],
					'trump_dk' => trim($data[$key][7], '%'),
					'trump_moe' => trim($data[$key][10], '%'),
					'trump_within_moe' => trim($data[$key][12], '%'),
					'statename' => $data[$key][0],
					'color' => compute_color(trim($data[$key][4], '%'), 'red')
				);
			}
		}
		$all_formatted_data = array(
			'clinton_fav' => $clinton_fav,
			'clinton_unfav' => $clinton_unfav,
			'trump_fav' => $trump_fav,
			'trump_unfav' => $trump_unfav
		);
		return $all_formatted_data;

	}

	return format_table_data($table_data);

}

function mc_mrp_presidential_candidate_data($table_data) {

	if(!function_exists('find_winner')) {
		function find_winner($clinton, $trump) {
			return floatval($clinton > $trump ? $clinton : $trump);
		}
	}

	if(!function_exists('find_winner_name')) {
		function find_winner_name($clinton, $trump) {
			return $clinton > $trump ? 'Clinton' : 'Trump';
		}
	}

	if(!function_exists('find_color')) {
		function find_color($clinton, $trump, $tossup) {
			$colors = array(
				'lightblue' => '#8fbadb',
				'blue' => '#0c4a6b',
				'red' => '#d33f2f',
				'lightred' => '#d99c86',
				'yellow' => '#e9d971'
			);
			if(strpos($tossup, 'Toss-up-') !== false) {
				if(preg_match('/Clinton/', $tossup) === 1) {
					return $colors['lightblue'];
				}
				else if(preg_match('/Trump/', $tossup) === 1) {
					return $colors['lightred'];
				}
			}
			if(strpos($tossup, 'Toss-up') !== false ) {
				return $colors['yellow'];
			}
			else return $clinton > $trump ? $colors['blue'] : $colors['red'];
		}
	}

	if(!function_exists('capitalize_state')) {
		function capitalize_state($state) {
			$upchar = strtoupper(substr($state, 0, 1));
			$exp = str_split($state);
			$exp[0] = $upchar;
			$imp = implode($exp);
			return $imp;
		}
	}

	if(!function_exists('format_table_data_2way')) {
		function format_table_data_2way($data) {
			$data = json_decode($data);
			$formatted_table_data = array();
			foreach($data as $key => $val) {
				if($key > 0) {
					$formatted_table_data[] = array(
						'hc-key' => 'us-' . strtolower($data[$key][0]),
						'value' => find_winner($data[$key][1], $data[$key][2]),
						'winnerName' => $data[$key][5],
						'name' => $data[$key][5],
						'color' => find_color($data[$key][1], $data[$key][2], $data[$key][5]),
						'stateAbbr' => $data[$key][0],
						'clintonData' => $data[$key][1],
						'trumpData' => $data[$key][2],
						'moe' => $data[$key][3]
					);
				}
			}
			return $formatted_table_data;
		}
	}


	return format_table_data_2way($table_data);

}

function mc_mrp_presidential_candidate_data_4way($table_data) {

	if(!function_exists('find_winner')) {
		function find_winner($clinton, $trump) {
			return floatval($clinton > $trump ? $clinton : $trump);
		}
	}

	if(!function_exists('find_winner_name')) {
		function find_winner_name($clinton, $trump) {
			return $clinton > $trump ? 'Clinton' : 'Trump';
		}
	}

	if(!function_exists('find_color')) {
		function find_color($clinton, $trump, $tossup) {
			$colors = array(
				'lightblue' => '#8fbadb',
				'blue' => '#0c4a6b',
				'red' => '#d33f2f',
				'lightred' => '#d99c86',
				'yellow' => '#e9d971'
			);
			if(strpos($tossup, 'Toss-up-') !== false) {
				if(preg_match('/Clinton/', $tossup) === 1) {
					return $colors['lightblue'];
				}
				else if(preg_match('/Trump/', $tossup) === 1) {
					return $colors['lightred'];
				}
			}
			if(strpos($tossup, 'Toss-up') !== false ) {
				return $colors['yellow'];
			}
			else return $clinton > $trump ? $colors['blue'] : $colors['red'];
		}
	}

	if(!function_exists('capitalize_state')) {
		function capitalize_state($state) {
			$upchar = strtoupper(substr($state, 0, 1));
			$exp = str_split($state);
			$exp[0] = $upchar;
			$imp = implode($exp);
			return $imp;
		}
	}

	if(!function_exists('format_table_data_4way')) {
		function format_table_data_4way($data) {
			$data = json_decode($data);
			$formatted_table_data = array();
			foreach($data as $key => $val) {
				if($key > 0) {
					$formatted_table_data[] = array(
						'hc-key' => 'us-' . strtolower($data[$key][0]),
						'value' => find_winner($data[$key][1], $data[$key][2]),
						'winnerName' => $data[$key][7],
						'name' => $data[$key][7],
						'color' => find_color($data[$key][1], $data[$key][2], $data[$key][7]),
						'stateAbbr' => $data[$key][0],
						'clintonData' => $data[$key][1],
						'trumpData' => $data[$key][2],
						'johnsonData' => $data[$key][3],
						'steinData' => $data[$key][4],
						'dontKnow' => $data[$key][5], 
						'moe' => $data[$key][6]
					);
				}
			}
			return $formatted_table_data;
		}
	}
	return format_table_data_4way($table_data);
}

function mc_election_interactive_format_data($data) {
	$data = json_decode($data, true);
	$jsdata = array();

	foreach($data as $key => $value) {
		if($key > 0) {
			$jsdata[] = array(
				'Clinton' => $data[$key][0],
				'Trump' => $data[$key][1],
				'Value' => $data[$key][2],
				'Demographic' => $data[$key][3],
				'Sample' => $data[$key][4]
			);
		}
	}
	return json_encode($jsdata);
}