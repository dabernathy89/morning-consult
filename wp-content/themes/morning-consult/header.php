<?php
/**
 * The default header for the theme.
 *
 * @package Morning Consult 2015
 */
get_template_part('inc/partials/header','opening'); ?>

<?php $section_cat = get_field('section_home_category'); ?>

<header id="header-main" class="site-header <?php echo $section_cat ? $section_cat->slug : ""; ?>" role="banner">
  <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle mc-nav-hamburger collapsed" data-toggle="collapse" data-target="#mc-navbar-mobile" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <i class="fa fa-bars fa-lg"></i>
      </button>
      <a class="navbar-brand" href="https://morningconsult.com">
        <img src="<?php echo get_template_directory_uri() . '/img/mc-logo-sq-white.png'; ?>">
        <span>Morning Consult</span>
      </a>

      <?php

      if($section_cat) {
      ?>
        <p class="navbar-text nav-cat-desc hidden-xs <?php echo $section_cat ? $section_cat->slug : ""; ?>"><?php echo $section_cat->name ?></p>
      <?php } ?>
      <div class="vertical-spacer hidden-xs " style="float: left;"><div></div></div>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="mc-navbar-desktop">
      <ul class="nav navbar-nav navbar-left">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle mc-nav-hamburger-desktop" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i><span>Menu</span></a>
          <ul class="dropdown-menu desktop-dropdown">
            <li><a href="https://morningconsult.com/campaigns">Campaigns</a></li>
            <li><a href="https://morningconsult.com/congress">Congress</a></li>
            <!-- <li><a href="https://morningconsult.com/whitehouse">White House</a></li> -->
            <li><a href="https://morningconsult.com/health">Health</a></li>
            <li><a href="https://morningconsult.com/energy">Energy</a></li>
            <li><a href="https://morningconsult.com/finance">Finance</a></li>
            <li><a href="https://morningconsult.com/tech">Tech</a></li>
            <li><a href="https://morningconsult.com/polling">Polling</a></li>
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="mc-intelligence"><a target="_blank" href="https://morningconsultintelligence.com/">Intelligence</a></li>
        <li class="vertical-spacer"><div></div></li>
        <li class="mc-search">
          <a href="#" class="dropdown-toggle mc-search-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <img class="mc-search-icon" src="<?php echo get_template_directory_uri() . '/img/magnifyingglass.png'; ?>" alt="">
          </a>
          <ul class="dropdown-menu mc-search-menu">
            <?php get_search_form(); ?>
          </ul>
        </li>
        <li>
          <!-- <button class="btn btn-default mc-subscribe-button">Subscribe</button> -->
          <?php if (is_page('subscribe') === false) : ?>
            <div class="collapse navbar-collapse nav-main navbar-right subscribe-link">
              <!-- <a class="search" href="/search/"><i class="fa fa-search"></i></a> -->
              <a href="#" class="btn btn-default navbar-btn" data-toggle="modal" data-target=".subscribe-modal">Subscribe</a>
            </div>
          <?php endif; ?>
        </li>
      </ul>
    </div>

    <div class="collapse hidden-sm hidden-md hidden-lg" id="mc-navbar-mobile">
      <ul class="nav navbar-nav">
        <li><a href="https://morningconsult.com/campaigns">Campaigns</a></li>
        <li><a href="https://morningconsult.com/congress">Congress</a></li>
        <!-- <li><a href="https://morningconsult.com/whitehouse">White House</a></li> -->
        <li><a href="https://morningconsult.com/health">Health</a></li>
        <li><a href="https://morningconsult.com/energy">Energy</a></li>
        <li><a href="https://morningconsult.com/finance">Finance</a></li>
        <li><a href="https://morningconsult.com/tech">Tech</a></li>
        <li><a href="https://morningconsult.com/polling">Polling</a></li>
				<li><a href="https://morningconsult.com/subscribe">Subscribe</a></li>
        <li class="mc-intelligence"><a target="_blank" href="https://morningconsultintelligence.com/">Intelligence</a></li>
        <li class="mc-search">

          <a href="#" class="dropdown-toggle mc-search-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <img class="mc-search-icon" src="<?php echo get_template_directory_uri() . '/img/magnifyingglass.png'; ?>" alt="">
          </a>
          <ul class="dropdown-menu mc-search-menu">
            <?php get_search_form(); ?>
          </ul>
        </li>
        <li>
          <!-- <button class="btn btn-default mc-subscribe-button">Subscribe</button> -->
          <?php if (is_page('subscribe') === false) : ?>
            <div class="collapse navbar-collapse nav-main navbar-right subscribe-link">
              <!-- <a class="search" href="/search/"><i class="fa fa-search"></i></a> -->
              <a href="#" class="btn btn-default navbar-btn" data-toggle="modal" data-target=".subscribe-modal">Subscribe</a>
            </div>
          <?php endif; ?>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

</header>
