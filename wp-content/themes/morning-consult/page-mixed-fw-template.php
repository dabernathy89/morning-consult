<?php /*
* Template Name: Mixed Full-Width Template
*/ ?>

<?php get_header(); ?>

<?php while(have_posts()) : the_post(); ?>



<container class="container">
	<row class="row">
		<header class="title-byline col-xs-12">
			<h1><?php the_title(); ?></h1>
			<div class="byline-date">
				<span class="author vcard">
					<?php the_author(); ?>
				</span>&nbsp;&nbsp;<span class="separator">|</span>&nbsp;&nbsp;
				<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
					<time class="entry-date published updated" datetime="<?php echo get_the_date( 'c' ); ?>">
						<?php the_date(); ?>
					</time>
				</a>
			</div><!-- .entry-meta -->
		</header>
		<main class="col-xs-12 col-md-8 col-md-offset-2 text">
			<?php the_field('first_text_section'); ?>
		</main>
	</row>
</container>

<container class="container">
	<row class="row">
		<main class="col-xs-12 image-1">
			<?php $image1 = get_field('first_image'); ?>
			<img class="img" src="<?php echo $image1['url']; ?>" alt="" />
		</main>
	</row>
</container>

<container class="container">
	<row class="row">
		<main class="col-xs-12 col-md-8 col-md-offset-2 text">
			<?php the_field('second_text_section'); ?>
		</main>
	</row>
</container>

<container class="container">
	<row class="row">
		<main class="col-xs-12 image-1">
			<?php $image2 = get_field('second_image'); ?>
			<img class="img" src="<?php echo $image2['url']; ?>" alt="" />
		</main>
	</row>
</container>

<container class="container">
	<row class="row">
		<main class="col-xs-12 col-md-8 col-md-offset-2 text">
			<?php the_field('third_text_section'); ?>
		</main>
	</row>
</container>

<container class="container last-container" >
	<row class="row">
		<div class="col-xs-12 col-md-7">
			<?php $image3 = get_field('bottom_image'); ?>
			<img src="<?php echo $image3['url']; ?>" alt="" />
		</div>
		<div class="col-xs-12 col-md-5">
			<?php the_field('bottom_text'); ?>
		</div>
	</row>
</container>
<?php endwhile; ?>

<?php get_footer(); ?>
