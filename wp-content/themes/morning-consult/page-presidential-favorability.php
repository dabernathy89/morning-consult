<?php /*
* Template Name: Presidential Candidate Favorability
*/

get_header();

?>

<script src="<?php echo get_template_directory_uri() . '/js/handlebars/handlebars-v4.0.5.js'; ?>"></script>
<script src="<?php echo get_template_directory_uri() . '/js/highmaps/js/highmaps.js'; ?>"></script>
<script src="<?php echo get_template_directory_uri() . '/js/highmaps/js/mapfiles/us-us-all.js'; ?>"></script>
<script src="<?php echo get_template_directory_uri() . '/js/presidential-favorability.js'; ?>"></script>

</script>

<?php
$tables = json_decode( get_option('tablepress_tables'), true);
$data_table_id = get_field('data_table_id');

if( isset($data_table_id) ) {
  $table_data = get_post_field('post_content', $tables['table_post'][$data_table_id], 'raw');
  $all_formatted_data = mc_presidential_candidate_table_data($table_data);
  wp_localize_script('morning-consult-all-js', 'mc_presidential_candidate_table_data', $all_formatted_data);
}

while(have_posts()) : the_post(); ?>

<container class="container">
  <row class="row">
    <main id="content-area" class="col-xs-12">
      <h1><?php the_title(); ?></h1>
      <h2><?php the_field('subheading'); ?></h2>

      <row class="row">
        <section class="intro-paragraph col-xs-12 col-md-10 col-md-offset-1">
          <?php the_field('intro_paragraph'); ?>
        </section>
      </row>

      <row class="row">
        <section id="tabs" class="col-xs-10 col-xs-offset-1">
          <row class="row">
            <ul class="nav nav-pills tab-pills" role="tablist">
              <li role="presentation" class="active col-xs-6 col-sm-4">
                <a href="#clinton" data-toggle="tab">Why Voters Don't Like Clinton</a>
              </li>
              <li role="presentation" class="col-xs-6 col-sm-4">
                <a href="#trump" data-toggle="tab">Why Voters Don't Like Trump</a>
              </li>
              <li id="map-content-tab" role="presentation" class="col-xs-12 col-sm-4">
                <a href="#map-content" data-toggle="tab">How They Fare in Each State</a>
              </li>
            </ul>
          </row>

          <div class="tab-content row">

              <div id="clinton" role="tabpanel" class="tab-pane fade in active col-xs-12">
                <?php the_field('clinton_tab_content'); ?>
                <?php the_field('methodology'); ?>

              </div>

              <div id="trump" role="tabpanel" class="tab-pane fade col-xs-12">
                <?php the_field('trump_tab_content'); ?>
                <?php the_field('methodology'); ?>

              </div>

              <div id="map-content" role="tabpanel" class="tab-pane fade col-xs-12">
                <row class="row">
                  <div class="map-tab-content col-xs-12">
                    <?php the_field('map_tab_content'); ?>
                  </div>

                  <ul class="nav nav-tabs map-tabs" role="tablist">
                    <li role="presentation" class="active col-xs-6 col-sm-4 col-sm-offset-2">
                      <a href="#favorability" data-toggle="tab">Favorability by State</a>
                    </li>
                    <li role="presentation" class="col-xs-6 col-sm-4">
                      <a href="#unfavorability" data-toggle="tab">Unfavorability by State</a>
                    </li>
                  </ul>

                  <div class="tab-content">


                    <div id="favorability" role="tabpanel" class="tab-pane active fade in col-xs-12">
                      <row class="row">
                        <div id="favorability-legend" class="col-xs-12">
                          <?php
                            $fav_colors = array(
                              '#006837' => '> 70%',
                              '#31a354' => '60%',
                              '#78c679' => '50%',
                              '#addd8e' => '40%',
                              '#d9f0a3' => '30%',
                              '#ffffcc' => '< 30%'
                            );
                            $fav_legend = create_html_legend($fav_colors);
                            echo $fav_legend;
                          ?>
                        </div>
                        <div id="map-content-1" class="col-xs-12 col-md-6"></div>
                        <div id="map-content-2" class="col-xs-12 col-md-6"></div>
                      </row>
                    </div>

                    <div id="unfavorability" role="tabpanel" class="tab-pane fade col-xs-12">
                      <row class="row">
                        <div id="unfavorability-legend" class="col-xs-12">
                          <?php
                            $unfav_colors = array(
                              '#a63603' => '> 70%',
                              '#e6550d' => '60%',
                              '#fd8d3c' => '50%',
                              '#fdae6b' => '40%',
                              '#fdd0a2' => '30%',
                              '#feedde' => '< 30%'
                            );
                            $unfav_legend = create_html_legend($unfav_colors);
                            echo $unfav_legend;
                          ?>
                        </div>
                        <div id="map-content-3" class="col-xs-12 col-md-6"></div>
                        <div id="map-content-4" class="col-xs-12 col-md-6"></div>
                      </row>
                    </div>



                    <div class="table-data col-xs-12">
                      <span class="visible-sm visible-xs" style="text-align:center;">&laquo; Scroll to see more data &raquo;</span>
                      <?php tablepress_print_table(array('id'=> $data_table_id)); ?>
                    </div>

                    <div class="methodology col-xs-12"><?php the_field('methodology'); ?></div>

                  </div>

                </row>
              </div>

          </div>
        </section>
      </row>

    </main>
  </row>
</container>

<?php endwhile; ?>

<?php get_footer(); ?>
