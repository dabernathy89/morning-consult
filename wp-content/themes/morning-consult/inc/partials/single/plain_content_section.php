<div class="plain-content">
    <div class="row">
        <div class="col-xs-12">
            <?php the_sub_field('content'); ?>
        </div>
    </div>
</div>