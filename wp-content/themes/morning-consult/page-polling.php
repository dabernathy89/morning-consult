<?php
/**
 * Template name: Polling
 *
 * @package Morning Consult 2015
 */

get_header(); ?>

<?php while(have_posts()) : the_post(); ?>

<container class="container page-template-polling">
  <row class="row">
    <main class="polling-content col-sm-8 col-xs-12">
      <section class="mc-polling">
        <row class="row">
          <div class="mc-polling-headline col-xs-12">
            <h1><?php the_title(); ?></h1>
          </div>
          <div class="button-container col-xs-12">
            <row class="row">
              <div class="col-xs-12 col-md-8"><?php the_field('intro_blurb'); ?></div>
              <div class="col-xs-12 col-md-4">
                <button type="button" class="btn btn-default polling-button" name="poll-with-us">Poll With Us</button>
                <button type="button" class="btn btn-default polling-button" name="methodology">Methodology</button>
              </div>
            </row>
          </div>
        </row>
      </section>
      <div class="divider" style="background-image: url(<?php echo get_template_directory_uri() . '/img/mc_alerts_bg_default.png';?>)"></div>
      <section class="mc-intelligence">
        <h1>Morning Consult Intelligence</h1>
        <div class="youtube-embed">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/-s40_e2lBQ8" frameborder="0" allowfullscreen></iframe>
        </div>
        <p>
          <?php the_field('video_description'); ?>
        </p>
      </section>

      <?php include(locate_template('inc/partials/mci-polling-charts.php')); ?>
    </main>

    <main class="col-sm-4 alerts-wrap hidden-xs">
        <?php get_template_part( 'inc/partials/alerts' ); ?>
    </main>

  </row>

</container>


<?php endwhile; ?>

<?php get_footer(); ?>
