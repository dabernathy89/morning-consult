jQuery(document).ready(function($){
	$('#post-body').on('click', '.morning-brief-create-campaign', function(event) {
		event.preventDefault();
		var self = this;
		var response_container = $(this).parents('.acf-input').find('.response');

		$.post(
			ajaxurl,
			{
				"action": 'mc_create_campaign',
				"url": window.location.href,
				"emailsystem": $(self).data('emailsystem')
			},
			function(response) {
				console.log(response);
				if (response.success === true) {
					response_container.html(response.data.message + ' ' + response.data.data);
				} else {
					if(!response.data.message) {
						response_container.html("There was a problem. Here is the error message:<br> " + response.data.message + " " + response.data.data);
					}
					else {
						response_container.html("There was a problem: <br>" + response.data.message + "<br>" + "Details: <br/>" + response.data.data);
					}
				}
			}
		);
	});
});
