(function(w,d,$) {

  function initHandlebars() {
    Handlebars.registerHelper('plusOne', function(index) {
      return (parseInt(index) +1);
    });
    Handlebars.registerHelper('minusOne', function(unknown) {
      return (parseInt(unknown) -1);
    });

    Handlebars.registerHelper('getMCILink', function(name) {
      var nameToLink = {
        "Charlie Baker" : "https://morningconsultintelligence.com/examine?v=TEVHMV9DaGFybGllX0Jha2Vy&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Larry Hogan" : "https://morningconsultintelligence.com/examine?v=TEVHMV9MYXJyeV9Ib2dhbg%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Matt Mead" : "https://morningconsultintelligence.com/examine?v=TEVHMV9NYXR0X01lYWQ%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Jack Markell" : "https://morningconsultintelligence.com/examine?v=TEVHMV9KYWNrX01hcmtlbGw%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Gary Herbert" : "https://morningconsultintelligence.com/examine?v=TEVHMV9HYXJ5X0hlcmJlcnQ%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Jack Dalrymple" : "https://morningconsultintelligence.com/examine?v=TEVHMV9KYWNrX0RhbHJ5bXBsZQ%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Bill Haslam" : "https://morningconsultintelligence.com/examine?v=TEVHMV9CaWxsX0hhc2xhbQ%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Asa Hutchinson" : "https://morningconsultintelligence.com/examine?v=TEVHMV9Bc2FfSHV0Y2hpbnNvbg%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Brian Sandoval" : "https://morningconsultintelligence.com/examine?v=TEVHMV9Ccmlhbl9TYW5kb3ZhbA%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Bill Walker" : "https://morningconsultintelligence.com/examine?v=TEVHMV9CaWxsX1dhbGtlcg%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Sam Brownback" : "https://morningconsultintelligence.com/examine?v=TEVHMV9TYW1fQnJvd25iYWNr&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Dan Malloy" : "https://morningconsultintelligence.com/examine?v=TEVHMV9EYW5fTWFsbG95&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Rick Snyder" : "https://morningconsultintelligence.com/examine?v=TEVHMV9SaWNrX1NueWRlcg%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Chris Christie" : "https://morningconsultintelligence.com/examine?v=TEVHMV9DaHJpc19DaHJpc3RpZQ%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Paul LePage" : "https://morningconsultintelligence.com/examine?v=TEVHMV9QYXVsX0xlUGFnZQ%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Bruce Rauner" : "https://morningconsultintelligence.com/examine?v=TEVHMV9CcnVjZV9SYXVuZXI%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Scott Walker" : "https://morningconsultintelligence.com/examine?v=TEVHMV9TY290dF9XYWxrZXI%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Gina Raimondo" : "https://morningconsultintelligence.com/examine?v=TEVHMV9HaW5hX1JhaW1vbmRv&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Matt Bevin" : "https://morningconsultintelligence.com/examine?v=TEVHMV9NYXR0X0Jldmlu&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
        "Mary Fallin" : "https://morningconsultintelligence.com/examine?v=TEVHMV9NYXJ5X0ZhbGxpbg%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0"
      }
      return nameToLink[name];
    });

    Handlebars.registerHelper('ifGreaterThan100', function(approve, unknown, disapprove, options) {
      var approve = parseInt(approve);
      var disapprove = parseInt(disapprove);
      var unknown = parseInt(unknown);
      var total = (approve + disapprove + unknown);

      if((total) > 100) {
        return options.fn(this);
      } else {
        return options.inverse(this);
      }
    });

    Handlebars.registerHelper('ifLessThan100', function(approve, unknown, disapprove, options) {
      var approve = parseInt(approve);
      var disapprove = parseInt(disapprove);
      var unknown = parseInt(unknown);
      var total = (approve + disapprove + unknown);

      if((total) < 100) {
        return options.fn(this);
      }
      else {
        return options.inverse(this);
      }
    });

    var source = $('#hb-governor-ranking').html();
    var template = Handlebars.compile(source);
    var sourceBottom = $("#hb-governor-ranking-bottom").html();
    var templateBottom = Handlebars.compile(sourceBottom);
    var top10Data = mc_governor_table_data.top10;
    var bottom10Data = mc_governor_table_data.bottom10;
    var top10html = template(top10Data);
    var bottom10html = templateBottom(bottom10Data);

    $('#top10').html(top10html);
    $('#bottom10').html(bottom10html);
  }

  function init() {
    initHandlebars();
  }

  $(d).ready(function() {
    init();
  })
})(window, document, jQuery);
