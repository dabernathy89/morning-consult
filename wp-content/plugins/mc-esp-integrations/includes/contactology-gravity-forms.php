<?php

if (class_exists("GFForms")) {
    GFForms::include_feed_addon_framework();

    class MorningConsultContactologyGravityForms extends GFFeedAddOn {

        protected $_version = "1.0";
        protected $_min_gravityforms_version = "1.7.9999";
        protected $_slug = "mc_contactology";
        protected $_path = "wp-content/themes/morning-consult/inc/contactology-gravity-forms.php";
        protected $_full_path = __FILE__;
        protected $_title = "Contactology Add-On";
        protected $_short_title = "Contactology Add-On";
        protected $api_key;
        protected $contactology;

        public function get_api_key() {
            return $this->api_key;
        }

        public function init(){
            parent::init();
            $this->api_key = $this->get_plugin_setting("mc_contactology_api_key");
            if ($this->api_key) {
                $this->contactology = new MCESPI_Campaigner_Api($this->api_key, true);
            }
        }

        public function get_lists() {
            $lists_for_feed = array(
                array(
                    'label' => 'No List',
                    'value' => '0'
                )
            );
            $lists = $this->contactology->List_Get_Active_Lists();

            foreach ($lists as $listkey => $listval) {
                $lists_for_feed[] = array(
                    'label' => $listval['name'],
                    'value' => $listval['listId']
                );
            }

            return $lists_for_feed;
        }

        public function get_fields() {
            $fields_for_feed = array();
            $fields = $this->contactology->CustomField_Get_All();
            foreach ($fields as $fieldkey => $fieldval) {
                $fields_for_feed[] = array(
                    'label' => $fieldval['fieldName'],
                    'name' => $fieldval['token']
                );
            }
            return $fields_for_feed;
        }

        public function feed_settings_fields() {
            if (!$this->api_key) {
                return array();
            }

            $lists = $this->get_lists();
            $fields = $this->get_fields();

            return array(
                array(
                    "title"  => "Contactology Settings",
                    "fields" => array(
                        array(
                            "label"   => "Feed name",
                            "type"    => "text",
                            "name"    => "feedName",
                            "class"   => "small"
                        ),
                        array(
                            "label"   => "List",
                            "type"    => "select",
                            "name"    => "mc_contactology_subscribe_list",
                            "choices" => $lists
                        ),
                        array(
                            "name" => "mappedFields",
                            "label" => "Map Fields",
                            "type" => "field_map",
                            "field_map" => $fields
                        ),
                        array(
                            "name" => "condition",
                            "label" => "Condition",
                            "type" => "feed_condition",
                            "checkbox_label" => "Enable Condition",
                            "instructions" => "Process this feed if"
                        )
                    )
                )
            );
        }

        public function plugin_settings_fields() {
            return array(
                array(
                    "title"  => "Contactology Add-On Settings",
                    "fields" => array(
                        array(
                            "name"    => "mc_contactology_api_key",
                            "label"   => "Contactology API Key",
                            "type"    => "text",
                            "class"   => "small"
                        )
                    )
                )
            );
        }

        public function process_feed($feed, $entry, $form){
            if (!$feed['meta']['mc_contactology_subscribe_list'] || !$feed['is_active']) {
                return;
            }

            $listid = $feed['meta']['mc_contactology_subscribe_list'];

            $contact = array();

            foreach ($feed['meta'] as $metakey => $metaval) {
                if (strpos($metakey, 'mappedFields_') === 0 && $metaval) {
                    $label = str_replace('mappedFields_', '', $metakey);
                    $contact[$label] = $entry[$metaval];
                }
            }

            foreach ($form['fields'] as $field) {
                if (is_array($field)) {
                    if ($field['type'] === "email" && !$field['adminOnly']) {
                        $contact['email'] = $entry[$field['id']];
                    }
                } elseif (is_object($field)) {
                    if ($field->type === "email" && !$field->adminOnly) {
                        $contact['email'] = $entry[$field->id];
                    }
                }
            }

            if (empty($contact) || !array_key_exists('email', $contact)) {
                return;
            }

            $importcontact = $this->contactology->Contact_Import(
                array($contact),
                'Website Signup',
                array(
                    'listIds'=>array(
                        (int)$listid
                    ),
                    'updateCustomFields' => TRUE
                )
            );
        }

        public function feed_list_columns() {
            return array(
                'feedName' => 'Name'
            );
        }
    }
}