<?php /*
* Template Name: Video Dial Test
*/

get_header();



?>
<!-- <script src="https://www.youtube.com/iframe_api"></script> -->
<script src="<?php echo get_template_directory_uri() . '/js/video-dial-test.js'; ?>"></script>

<!-- var id = qV_46FWdliM -->
<?php while(have_posts()) : the_post(); ?>

<container class="container main-container">
	<row class="row">
		<div class="col-xs-12 col-md-10 col-md-offset-1 main-column">
			<h1 class="page-title"><?php the_title(); ?></h1>

			<div class="entry-meta">
				<span class="author vcard">
					<?php the_author(); ?>
				</span>&nbsp;&nbsp;<span class="separator">|</span>&nbsp;&nbsp;
				<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
					<time class="entry-date published updated" datetime="<?php echo get_the_date( 'c' ); ?>">
						<?php the_date(); ?>
					</time>
				</a>
			</div><!-- .entry-meta -->

			<div class="intro-text"><?php the_content(); ?></div>
			<row class="row">
				<div class="video-container col-xs-12 col-md-7">
					<h2>"Who We Are"</h2>
					<div id="mainVideo"></div>
					<div id="main-animation" class="dial-animation">
						<img style="width: 100%;" src="<?php echo get_template_directory_uri() . '/img/dialtest-page/video3.png'; ?>" alt="">
						<div id="main-progress-bar" class="progress-bar" data-width="0"></div>
					</div>
				</div>
				<div class="main-video-text col-xs-12 col-md-5">
					<?php the_field('main_video_text'); ?>
				</div>
			</row>
			<row class="row">
				<div class="video-container-wrapper col-xs-12 col-md-6">
					<div class="video-container">
						<h2>"Quiet Moments"</h2>
						<div id="video1"></div>
						<div id="animation-1" class="dial-animation">
							<img style="width: 100%;" src="<?php echo get_template_directory_uri() . '/img/dialtest-page/video1.png'; ?>" alt="">
							<div id="progress-bar-1" class="progress-bar" data-width="0"></div>
						</div>
					</div>
					<div class="bottom-video-text">
						<?php the_field('video_1_description'); ?>
					</div>
				</div>
				<div class="video-container-wrapper col-xs-12 col-md-6">
					<div class="video-container">
						<h2>"Always"</h2>
						<div id="video2"></div>
						<div id="animation-2" class="dial-animation">
							<img style="width: 100%;" src="<?php echo get_template_directory_uri() . '/img/dialtest-page/video2.png'; ?>" alt="">
							<div id="progress-bar-2" class="progress-bar" data-width="0"></div>
						</div>
					</div>
					<div class="bottom-video-text">
						<?php the_field('video_2_description'); ?>
					</div>
				</div>

			</row>
		</div>

	</row>
</container>








<?php endwhile; ?>



<?php get_footer(); ?>
