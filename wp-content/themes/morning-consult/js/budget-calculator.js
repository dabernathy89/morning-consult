var mc_bc = new Vue({
    el: '#budget-calculator',
    data: {
        totals: mc_budget_calc_totals,
        items: mc_budget_calc_items
    },
    computed: {
        largestTotal: function() {
            var max = 0;
            for (var i in this.totals) {
                if (this.totals[i].total > max) {
                    max = this.totals[i].total;
                }
            }
            return max;
        },
    },
    methods: {
        percentOfLargestTotal: function(total) {
            var percentage = (total / this.largestTotal) * 100;
            return (percentage < 100) ? percentage : 100;
        },
        percentOfTotal: function(budgeted,total) {
            var percentage = (budgeted / total) * 100;
            return (percentage < 100) ? percentage : 100;
        },
        updateBudgets: function() {
            for (var x = 0; x < this.totals.length; x++) {
                this.totals[x].budgeted = 0;
            }

            if (this.totals.length > 1) {
                for (var i = 0; i < this.items.length; i++) {
                    if ( this.items[i].acf_fc_layout === "item" && this.items[i].chosenTotal && this.items[i].chosenTotal !== "none" && this.items[i].chosenTotal in this.totals) {
                        this.totals[parseInt(this.items[i].chosenTotal)].budgeted += this.items[i].amount;
                    }
                }
            } else if (this.totals.length === 1) {
                for (var i = 0; i < this.items.length; i++) {
                    if ( this.items[i].acf_fc_layout === "item" && this.items[i].isChecked) {
                        this.totals[0].budgeted += this.items[i].amount;
                    }
                }
            }
        },
        hasMidpoints: function(total) {
            return !!total.midpoints;
        }
    }
});

mc_bc.$watch('items', mc_bc.updateBudgets, {deep: true});

jQuery(document).ready(function($){
    var totals_offset = $('section.totals').offset().top;
    var initial_body_padding = parseInt($('body').css('padding-top').replace('px',''));

    $(document).scroll(function(){
        var new_padding;
        var menu_height;

        menu_height = ($('#header-main').height());
        if ($(window).width() > 767) {
            menu_height = 0;
        }

        if (($(document).scrollTop() + menu_height) > totals_offset) {
            if ($(window).width() > 767) {
                $('#header-main').slideUp(300, function() {
                    stickCalculatorToTop(menu_height, initial_body_padding);
                });
            } else {
                stickCalculatorToTop(menu_height, initial_body_padding);
            }
        } else {
            $('section.totals').removeClass('stuck');
            $('body').css('padding-top',initial_body_padding.toString() + 'px')
            $('section.totals').css('top','auto');
            $('#header-main').slideDown(300);
        }
    });

    function stickCalculatorToTop(menu_height, initial_body_padding) {
        $('section.totals').addClass('stuck');
        $('section.totals').css('top',menu_height.toString() + 'px');
        new_padding = initial_body_padding + $('section.totals').height();
        new_padding = new_padding.toString() + 'px';
        $('body').css('padding-top',new_padding);
    }
});