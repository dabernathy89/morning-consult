(function(w, d, $) {

	var barColors = {
		"lightblue" : '#8fbadb',
		"blue" : '#0c4a6b',
		"red" : '#d33f2f',
		"lightred" : '#d99c86',
		"yellow" : '#e9d971'
	}

	var electoralVotes = {
		"AL" : 9,
		"AK" : 3,
		"AZ" : 11,
		"AR" : 6,
		"CA" : 55,
		"CO" : 9,
		"CT" : 7,
		"DE" : 3,
		"DC" : 3,
		"FL" : 29,
		"GA" : 16,
		"HI" : 4,
		"ID" : 4,
		"IL" : 20,
		"IN" : 11,
		"IA" : 6,
		"KS" : 6,
		"KY" : 8,
		"LA" : 8,
		"ME" : 4,
		"MD" : 10,
		"MA" : 11,
		"MI" : 16,
		"MN" : 10,
		"MS" : 6,
		"MO" : 10,
		"MT" : 3,
		"NE" : 5,
		"NV" : 6,
		"NH" : 4,
		"NJ" : 14,
		"NM" : 5,
		"NY" : 29,
		"NC" : 15,
		"ND" : 3,
		"OH" : 18,
		"OK" : 7,
		"OR" : 7,
		"PA" : 20,
		"RI" : 4,
		"SC" : 9,
		"SD" : 3,
		"TN" : 11,
		"TX" : 38,
		"UT" : 6,
		"VT" : 3,
		"VA" : 13,
		"WA" : 12,
		"WV" : 5,
		"WI" : 10,
		"WY" : 3
	}

	function updateElectoralCount() {
		var mrp = w.mc_mrp_presidential_candidate_data;
		var bar = $('#electoral-votes-bar');
		// var totalsDiv = $('#electoral-votes-totals');
		var totals = {"D": 0, "R": 0};

		$.each(mrp, function(index, row) {
			if(mrp[index]['winnerName'].includes('Trump')) {
				totals['R'] += parseInt(electoralVotes[mrp[index].stateAbbr])
			}
			else if(mrp[index]['winnerName'].includes('Clinton')) {
				totals['D'] += parseInt(electoralVotes[mrp[index].stateAbbr])
			}
		});
		$.each(totals, function(index, val) {
			var width = Math.round((val / 538) * 100) ;
			bar.find(('.votes-' + index).toLowerCase()).css('width', width.toString() + '%');
			$(('.totals-' + index).toLowerCase()).html(totals[index]);
		});
	}

	function mapSize() {
		if(w.innerWidth < 400) {
			return 200;
		}
		else if(w.innerWidth < 480) {
			return 300;
		}
		else if(w.innerWidth > 480 && w.innerWidth < 768) {
			return 500;
		}
		else return 600;
	}

	function initMap() {
		var mapData = w.mc_mrp_presidential_candidate_data;
		$('#map-container').highcharts('Map', {
			chart: {
				height: mapSize()
			},
			title : {
				text: ''
			},
			mapNavigation: {
				enabled: false,
			},
			credits: {
				enabled: false
			},
			legend: {
				enabled: false
			},
			plotOptions: {
				map: {
					borderColor: '#FFFFFF',
					borderWidth: 0.5,
					states: {
						hover: {
							color: 'red'
						},
						normal: {
							animation: false,
						}
					}
				}
			},
			series: [{
				data: mapData,
				mapData: Highcharts.maps['countries/us/us-all'],
				joinBy: 'hc-key',
				tooltip: {
					headerFormat: '',
					pointFormat: '<span><strong>Clinton: </strong>{point.clintonData}</span><br/><span><strong>Trump: </strong>{point.trumpData}</span><br/><span>Winner: {point.winnerName}</span>'
				},
			}],

		});
	}

  function init() {
		initMap();
		updateElectoralCount();
  }

  $(d).ready(function() {
    init();
  });

})(window, document, jQuery);
