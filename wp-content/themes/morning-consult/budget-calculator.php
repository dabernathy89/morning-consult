<?php
/**
 * Template Name: Budget Calculator
 *
 * @package Morning Consult 2015
 */

get_header(); ?>

    <style>
        #budget-calculator {
            margin-bottom: 20px;
        }

        .total-container {
            margin-bottom: 20px;
        }

        .total-container > .name {
            font-weight: bold;
        }

        .total-bar {
            height: 40px;
            <?php if (get_field('budget_calc_totals_bg')) : ?>
                background-color: <?php the_field('budget_calc_totals_bg'); ?>;
            <?php else : ?>
                background-color: rgb(220,220,220);
            <?php endif; ?>
            position: relative;
            margin-bottom: 20px;
        }

        @media (min-width: 768px) {
            .total-bar.has-midpoints {
                margin-bottom: 60px;
            }
        }

        .budgeted-bar {
            position: absolute;
            top: 0;
            left: 0;
            <?php if (get_field('budget_calc_budgeted_amount_bg')) : ?>
                background-color: <?php the_field('budget_calc_budgeted_amount_bg'); ?>;
            <?php else : ?>
                background-color: red;
            <?php endif; ?>
            height: 40px;
            transition: width .25s;
            transition-timing-function: ease-in-out;
        }

        .midpoint {
            position: absolute;
            left: 0;
            top: 0;
            height: 40px;
            border-left: 1px dashed rgb(50,50,50);
        }

        .midpoint:last-child {
            width: 400px;
        }

        .midpoint span {
            position: relative;
            top: 40px;
            left: 0;
            display: block;
        }

        @media (max-width: 767px) {
            .midpoint {
                display: none;
            }
        }

        .item label {
            font-weight: normal;
        }

        section.totals {
            padding: 20px 0 10px;
        }

        section.totals.stuck {
            background-color: rgb(240,240,240);
            position: fixed;
            width: 100%;
            z-index: 100;
        }

        .item .description {
            padding-left: 20px;
        }
    </style>

    <div class="page default">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">

                    <?php while ( have_posts() ) : the_post(); ?>

                        <?php get_template_part( 'content', 'page' ); ?>

                    <?php endwhile; // end of the loop. ?>

                </div>
            </div>
        </div>

        <?php
            $totals = get_field('budget_calc_totals');
            $items = get_field('budget_calc_items');

            foreach ($totals as &$total) {
                $total['total'] = (float)$total['total'];
                $total['budgeted'] = 0;
                if (is_array($total['midpoints'])) {
                    foreach ($total['midpoints'] as &$midpoint) {
                        $midpoint['amount'] = (float)$midpoint['amount'];
                    }
                } else {
                    unset($total['midpoints']);
                }
            }

            foreach ($items as &$item) {
                $item['amount'] = (float)$item['amount'];
                $item['chosenTotal'] = "none";
                $item['isChecked'] = false;
            }

            wp_localize_script( 'mc-budget-calculator', 'mc_budget_calc_totals', $totals );
            wp_localize_script( 'mc-budget-calculator', 'mc_budget_calc_items', $items );
        ?>

        <div id="budget-calculator">
            <section class="totals">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                            <template v-repeat="total in totals">
                                <div class="total-container total-{{$index}}">
                                    <p class="name">{{ total.name }}: ${{ total.total }}bn (budgeted: ${{ total.budgeted }}bn)</p>
                                    <div
                                        v-class="total-bar: true, has-midpoints: hasMidpoints(total)"
                                        v-style="width: percentOfLargestTotal(total.total) + '%'">
                                        <div class="budgeted-bar" v-style="width: percentOfTotal(total.budgeted, total.total) + '%'"></div>
                                        <template v-repeat="midpoint in total.midpoints">
                                            <div class="midpoint" v-style="margin-left: percentOfTotal(midpoint.amount, total.total) + '%'">
                                                <span class="amount">
                                                    ${{ midpoint.amount }}bn
                                                </span>
                                                <span class="name">
                                                    {{ midpoint.name }}
                                                </span>
                                            </div>
                                        </template>
                                    </div>
                                </div>
                            </template>
                        </div>
                    </div>
                </div>
            </section>

            <section class="items">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">

                            <!-- item if there is only one budget total -->
                            <div class="single-total-items" v-if="totals.length === 1">
                                <template v-repeat="item in items">
                                    <div class="item-wrap single-total" v-if="item.acf_fc_layout === 'item'">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="{{ 'chosen_total_' + $parent.$index }}" v-model="item.isChecked" value="{{ $index }}">
                                                <strong>{{{ item.name }}} - ${{ item.amount }}bn</strong>
                                            </label>
                                        </div>
                                        <div class="description" v-html="item.description"></div>
                                    </div>
                                    <!-- item section -->
                                    <div class="item-section item-{{$index}}" v-if="item.acf_fc_layout === 'item_section'">
                                        <hr>
                                        <h3 class="name" v-html="item.name"></h3>
                                        <p class="description" v-html="item.description"></p>
                                    </div>
                                </template>
                            </div>

                            <!-- item if there are multiple budget totals -->
                            <div class="multiple-totals-items" v-if="totals.length > 1">
                                <template v-repeat="item in items">
                                    <div class="item item-{{$index}}" v-if="item.acf_fc_layout === 'item'">
                                        <h4 class="name">{{{ item.name }}} - ${{ item.amount }}bn</h4>
                                        <div class="description" v-html="item.description"></div>
                                        <div class="radio-inline">
                                            <label>
                                                <input type="radio" v-attr="name: 'chosen_total_' + $index" value="none" v-model="item.chosenTotal">
                                                None
                                            </label>
                                        </div>
                                        <template v-repeat="total_radio in totals">
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="{{ 'chosen_total_' + $parent.$index }}" v-model="item.chosenTotal" value="{{ $index }}">
                                                    {{ total_radio.name }}
                                                </label>
                                            </div>
                                        </template>
                                    </div>

                                    <!-- item section -->
                                    <div class="item-section item-{{$index}}" v-if="item.acf_fc_layout === 'item_section'">
                                        <hr>
                                        <h3 class="name" v-html="item.name"></h3>
                                        <p class="description" v-html="item.description"></p>
                                    </div>
                                </template>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div>

        <div class="container secondary-content">
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <?php the_field('budget_calc_content_below_calculator'); ?>
                </div>
            </div>
        </div>

    </div><!-- #primary -->

<?php get_footer(); ?>
