<?php
/**
 * The opening section of the html, up to the opening <body> tag
 *
 * @package Morning Consult 2015
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<!-- <script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script> -->
	<link rel="apple-touch-icon" sizes="180x180" href="wp-content/themes/morning-consult/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="wp-content/themes/morning-consult/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="wp-content/themes/morning-consult/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="wp-content/themes/morning-consult/img/favicon/manifest.json">
	<link rel="mask-icon" href="wp-content/themes/morning-consult/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="wp-content/themes/morning-consult/img/favicon/favicon.ico">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="wp-content/themes/morning-consult/img/favicon/mstile-144x144.png">
	<meta name="msapplication-config" content="wp-content/themes/morning-consult/img/favicon/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">

	<!-- site verification for google news -->
	<meta name="google-site-verification" content="Cf9zflqpiA7uoMq5I4bCyyiqmbYUeOUBWW6fxxL1Krw" />

	<?php wp_head(); ?>

	<script type='text/javascript'>
		// var googletag = googletag || {};
		// googletag.cmd = googletag.cmd || [];
		(function() {
			var gads = document.createElement('script');
			gads.async = true;
			gads.type = 'text/javascript';
			var useSSL = 'https:' == document.location.protocol;
			gads.src = (useSSL ? 'https:' : 'http:') +
				'//www.googletagservices.com/tag/js/gpt.js';
			var node = document.getElementsByTagName('script')[0];
			node.parentNode.insertBefore(gads, node);
		})();
	</script>

</head>

<body <?php body_class(); ?>>
