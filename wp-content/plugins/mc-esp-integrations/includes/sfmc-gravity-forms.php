<?php

if (class_exists("GFForms")) {
		GFForms::include_feed_addon_framework();

		class MorningConsultSFMCGravityForms extends GFFeedAddOn {

				protected $_version = "1.0";
				protected $_min_gravityforms_version = "1.9.16";
				protected $_slug = "mc_sfmc";
				protected $_path = "wp-content/plugins/mc-esp-integrations/includes/sfmc-gravity-forms.php";
				protected $_full_path = __FILE__;
				protected $_title = "SFMC Add-On";
				protected $_short_title = "SFMC Add-On";
				protected $client;

				private static $_instance = null;

				public static function get_instance() {
					if(self::$_instance === null) {
						self::$_instance = new self();
					}
					return self::$_instance;
				}

				public function init() {
						parent::init();
						$this->client = new ET_Client();
				}

				public function get_lists() {
						$lists_for_feed = get_transient( 'mc_sfmc_gf_lists' );

						if ( $lists_for_feed === false ) {
								$lists_for_feed = array();

								try {
										$sfmc_lists = new ET_List();
										$sfmc_lists->authStub = $this->client;
										$response = $sfmc_lists->get();

										foreach ($response->results as $list) {
												$lists_for_feed[] = array(
														'label' => $list->ListName . ' (ID: ' . $list->ID . ')',
														'name' => 'mc_sfmc_subscribe_lists[' . $list->ID . ']'
												);
										}

										set_transient(
												'mc_sfmc_gf_lists',
												$lists_for_feed,
												HOUR_IN_SECONDS
										);

										return $lists_for_feed;
								} catch (Exception $e) {
										echo 'Error: ' . $e->getMessage();
								}
						}

						return $lists_for_feed;
				}

				public function get_fields() {
						$fields = array(
								array(
										'label' => 'First Name',
										'name' => 'first_name'
								),
								array(
										'label' => 'Last Name',
										'name' => 'last_name'
								),
								array(
										'label' => 'Email',
										'name' => 'email'
								),
								array(
										'label' => 'Organization',
										'name' => 'organization'
								),
								array(
										'label' => 'Title',
										'name' => 'title'
								)
						);

						return $fields;
				}

				public function feed_settings_fields() {
						$lists = $this->get_lists();
						$fields = $this->get_fields();

						return array(
								array(
										"title"	=> "SFMC Settings",
										"fields" => array(
												array(
														"label"	 => "Feed name",
														"type"		=> "text",
														"name"		=> "feedName",
														"class"	 => "small"
												),
												array(
														"label"	 => "Lists",
														"type"		=> "checkbox",
														"name"		=> "mc_sfmc_subscribe_lists",
														"choices" => $lists
												),
												array(
														"name" => "mappedFields",
														"label" => "Map Fields",
														"type" => "field_map",
														"field_map" => $fields
												),
												array(
														"name" => "condition",
														"label" => "Condition",
														"type" => "feed_condition",
														"checkbox_label" => "Enable Condition",
														"instructions" => "Process this feed if"
												)
										)
								)
						);
				}

				public function get_selected_lists( $el ) {
						return $el === "1";
				}

				public function format_selected_list_ids( $el ) {
						return array('ID' => $el);
				}

				public function process_feed($feed, $entry, $form){

						$lists = array_keys(array_filter($feed['meta']['mc_sfmc_subscribe_lists'], array($this,'get_selected_lists')));
						$lists = array_map(array($this,'format_selected_list_ids'), $lists);

						$field_map = $this->get_field_map_fields( $feed, 'mappedFields' );

						try {
								// Create Subscriber
								$subscriber = new ET_Subscriber();
								$subscriber->authStub = $this->client;

								$subscriber_attrs = array(
										array(
												'Name' => 'First Name',
												'Value' => $entry[$field_map['first_name']],
										),
										array(
												'Name' => 'Last Name',
												'Value' => $entry[$field_map['last_name']],
										),
										array(
												'Name' => 'Organization',
												'Value' => $entry[$field_map['organization']],
										),
										array(
												'Name' => 'Title',
												'Value' => $entry[$field_map['title']]
										)
								);

								$subscriber->props = array(
										'EmailAddress' => $entry[$field_map['email']],
										'SubscriberKey' => $entry[$field_map['email']],
										'Attributes' => $subscriber_attrs,
										'Lists' => $lists
								);

								$response = $subscriber->post();

								if ($response->status){
										$this->add_note(
												$entry['id'],
												'Successfully added new subscriber to SFMC: ' . $response->results[0]->StatusMessage,
												'success'
										);
								} else {
										$this->add_note(
												$entry['id'],
												'Error adding new subscriber to SFMC: ' . $response->results[0]->StatusMessage,
												'error'
										);
								}
						} catch (Exception $create_error) {
								$message = 'Error creating SFMC subscriber: ' . $create_error->getMessage();
								$this->add_feed_error( $message, $feed, $entry, $form );
						}

						if ($response->results[0]->ErrorCode === 12014) {
								try {
										$patchResponse = $subscriber->patch();

										if ($response->status){
												$this->add_note(
														$entry['id'],
														'Successfully updated existing subscriber in SFMC: ' . $response->results[0]->StatusMessage,
														'success'
												);
										} else {
												$this->add_note(
														$entry['id'],
														'Error updating existing subscriber in SFMC: ' . $response->results[0]->StatusMessage,
														'error'
												);
										}
								} catch (Exception $update_error) {
										$message = 'Error updating SFMC subscriber: ' . $update_error->getMessage();
										$this->add_feed_error( $message, $feed, $entry, $form );
								}
						}
				}

				public function feed_list_columns() {
						return array(
								'feedName' => 'Name'
						);
				}

				public function can_duplicate_feed( $id ) {
						return true;
				}
		}
}
