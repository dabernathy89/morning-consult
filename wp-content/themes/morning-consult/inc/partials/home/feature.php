<?php

$home_featured_args = array(
    'posts_per_page' => '1',
    'post_type' => array('post', 'mc_alert'),
    'meta_key' => 'post_make_home_page_feature',
    'meta_value' => '1'
);

$home_featured = new WP_Query($home_featured_args);

if( $home_featured->have_posts() ): while( $home_featured->have_posts() ): $home_featured->the_post();

$home_featured_post_id = get_the_ID();

?>

  <article id="post-<?php the_ID(); ?>" <?php post_class('excerpt featured clearfix'); ?>>
    <header class="entry-header">
      <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
    </header><!-- .entry-header -->

    <div class="thumbnail-wrap">
      <a href="<?php echo get_permalink(); ?>">
        <?php the_post_thumbnail('large'); ?>
      </a>
    </div>

    <div class="meta-primary clearfix">
      <span class="author vcard">
        <em>
          <?php $all_authors = get_coauthors(); ?>
          By <?php foreach ($all_authors as $authorkey => $author) : ?>
            <?php include get_stylesheet_directory() . "/inc/partials/single/meta_author.php"; ?>
          <?php endforeach; ?>
        </em>
      </span>
			<br/>
      <div class="category-tags">
        <?php morning_consult_category_links_square(); ?>
      </div>
    </div>

    <div class="entry-content">
			<?php echo mc_custom_excerpt( get_the_ID(), 30 ); ?>
    </div>
  </article>

<?php endwhile; endif;
wp_reset_postdata(); ?>
