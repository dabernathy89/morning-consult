jQuery(function($) {
    // Morning Consult Variables
    var lastScrollTop = 0;
    var buffer = 20;
    var navbarHeight = $('#header-main').outerHeight();
    var backstretch_options = {};

    $('.embed-container, .video').fitVids();

    $('[data-toggle="tooltip"]').tooltip();

    if (window.featured_story_image) {
        // TODO: load based on screen size
        if ($('body').hasClass('author')) {
            backstretch_options.centeredY = false;
        }
        $('.featured').backstretch(window.featured_story_image.url, backstretch_options);
    }

    $('.gform_wrapper').on('submit', '[target^="gform_ajax"]', function(event) {
        var spinner = $(this).find('.gform_ajax_spinner');
        spinner.velocity(
            {rotateZ: "360deg"},
            {
                duration: 2000,
                easing: "linear",
                loop: true
            }
        );
    });

    $(window).on('resize', function(event) {
        resize_charts();
    });

    function resize_charts() {
        $('[id^=visualizer-]').each(function(index, el) {
            resize_chart(el);
        });
    }

    function resize_chart(chart, new_height) {
        var id = $(chart).attr('id');
        var curwidth = $(chart).width();
        var newheight = curwidth / (4/3);
        if (visualizer && visualizer.charts && visualizer.charts[id] && google && google.visualization) {
            visualizer.charts[id].settings['height'] = newheight;
        }
    }

    resize_charts();

    $('[data-bg]').each(function(index, el) {
        $(this).backstretch($(this).attr('data-bg'));
    });

    $('a[href*="facebook.com/sharer.php"], a[href*="twitter.com/share"], a[href*="linkedin.com/shareArticle"]').on('click', function(event) {
        event.preventDefault();
        window.open(
            $(this).attr('href'),
            'share-dialog',
            'width=520,height=350'
        );
    });

    $('.news-links').slick({
        infinite: false,
        variableWidth: true,
        speed: 300,
        slidesToShow: 1
    });

    $('.load-more').on('click', function(event) {
        event.preventDefault();
        var container = $(this).parent();
        var temp_container = $('<div></div>');
        var url = $(this).attr('href');
        var load_more_link = $(this);
        var next_page;
        $(this).data('read-more-text', $(this).html());
        $(this).html('Loading...').blur();

        if ($(this).hasClass('load-more-alerts')) {
            next_page = parseInt($(this).attr('data-current-page')) + 1;
            url = window.location.pathname + '?alerts_page=' + next_page.toString();
        }

        temp_container.load( url + ' #' + container.attr('id'), {},
            function(){
                var new_url = temp_container.find('.load-more').attr('href');
                load_more_link.attr('href',new_url);
                load_more_link.html(load_more_link.data('read-more-text'));
                temp_container.children('.load-more-wrapper').children().not('a').insertBefore(load_more_link);
                container.find('[data-bg]').each(function(index, el) {
                    $(el).backstretch($(el).attr('data-bg'));
                });
                if (load_more_link.hasClass('load-more-alerts')) {
                    load_more_link.attr('data-current-page',next_page);
                }
            }
        );
    });
});

// https://css-tricks.com/snippets/javascript/lighten-darken-color/
function adjustHexColor(color, amount) {
    var usePound = false;

    if (color[0] == "#") {
        color = color.slice(1);
        usePound = true;
    }

    var num = parseInt(color,16);

    var r = (num >> 16) + amount;

    if (r > 255) {
        r = 255;
    } else if (r < 0) {
        r = 0;
    }

    var b = ((num >> 8) & 0x00FF) + amount;

    if (b > 255) {
        b = 255;
    } else if (b < 0) {
        b = 0;
    }

    var g = (num & 0x0000FF) + amount;

    if (g > 255) {
        g = 255;
    } else if (g < 0) {
        g = 0;
    }

    return (usePound?"#":"") + (g | (b << 8) | (r << 16)).toString(16);
    
}
