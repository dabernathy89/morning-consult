<?php

function mc_remove_old_featured_posts( $post_id ) {
    $is_featured = get_field('post_make_home_page_feature',$post_id);

    if ($is_featured !== true) {
        return;
    }

    $other_featured_posts = get_posts(array(
        'posts_per_page' => -1,
        'post__not_in' => array($post_id),
        'meta_key' => 'post_make_home_page_feature',
        'meta_value' => '1'
    ));

    foreach ($other_featured_posts as $featured_post) {
        update_post_meta( $featured_post->ID, 'post_make_home_page_feature', '' );
    }
}

add_action('acf/save_post', 'mc_remove_old_featured_posts', 100);