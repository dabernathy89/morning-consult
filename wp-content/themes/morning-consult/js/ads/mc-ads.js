var w = window;
var d = document;

w.MCAds = {};
w.MCAds.adsOnPage = [];

import desktopAdSizes from './desktop/ad-sizes.js';
import mobileAdSizes from './mobile/ad-sizes.js';
import Utils from './utils.js';

class MCAds {

	// Pushes ad id to global adsOnPage object
  static adOnPage(id) {
    w.MCAds.adsOnPage.push(id);
  }

	//checks to see if ad ID is already in the given array
  static containsAd(arr, value) {
    let contains;
    for(var i = 0; i < arr.length; i++) {
      if(arr[i].id === value) {
        contains = true;
      }
    }
    if(contains === true) {
      return true
    }
    else return false;
  }

	static determineRandomFallback(cv, i, arr) {
    const screenWidth = this.getViewportWidth();
		let randomPct = Math.random(),
		adSizes = (screenWidth < 768) ? mobileAdSizes : desktopAdSizes;
		
		if(cv.hasOwnProperty('pct') && randomPct >= cv.pct) {
				googletag.cmd.push(function() {
					googletag.defineSlot(
						adSizes[cv.adType].name,
						adSizes[cv.adType].sizes,
						adSizes[cv.adType].adId
					).addService(googletag.pubads());
				});
			}
			else if(cv.hasOwnProperty('pct') && randomPct <= cv.pct) {
				googletag.cmd.push(function() {
					googletag.defineSlot(
						adSizes[cv.fallback].name,
						adSizes[cv.fallback].sizes,
						adSizes[cv.fallback].adId
					).addService(googletag.pubads());
					//if fallback evaluates to true, then
					//overwrite current ad object with fallback values
					arr[i].adType = adSizes[cv.fallback].name;
					arr[i].script = adSizes[cv.fallback].script;
				});
			}
			else {
				googletag.cmd.push(function() {
					googletag.defineSlot(
						adSizes[cv.adType].name,
						adSizes[cv.adType].sizes,
						adSizes[cv.adType].adId
					).addService(googletag.pubads());
				});
			}
	}

	// handles GPT API
	// defines ad "slots" so they can be fired either immediately (for regular ads)
	// or fired on scroll (lazy loading ads)
	static defineAdSlots(adsToLoad) {
    let regularAdsToLoad = adsToLoad.regularAds,
    lazyAdsToLoad = adsToLoad.lazyLoadingAds,
		fullurl = (window.location != window.parent.location) ? document.referrer : document.location,
		_self = this;
		
    regularAdsToLoad.map((cv, i, arr) => { _self.determineRandomFallback(cv, i, arr) }, adsToLoad)
    lazyAdsToLoad.map((cv, i, arr) => { _self.determineRandomFallback(cv, i, arr) }, adsToLoad)

		googletag.pubads().set('page_url', fullurl)
		googletag.pubads().setTargeting('urlvariable', fullurl)
		googletag.enableServices();

		return adsToLoad
	}

	// debounced function that watches for ads scrolling into viewport
	// fires placeAd() when ad is visible
	static detectInViewport(adsToWatch) {
    let _self = this;
    let getCoords = Utils.debounce(() => {
      adsToWatch.forEach((cv, i, arr) => {
        let rect = cv.elm.getBoundingClientRect();

        if( (rect.top >= 0) && (rect.bottom <= w.innerHeight)
        && (rect.left >= 0) && (rect.right <= w.innerWidth) ) {
          //if visible, fire ad placement
          _self.placeAd(arr[i].id, arr[i].script);
          //then remove from current array to not fire again
          arr.splice(i, 1);
        }
      });
		}, 250);
    w.addEventListener('scroll', getCoords);
  }	

	// Get all desktop or mobile divs (ad spots) by data-ad-display attr
  static findAdsOnPage() {
		let screenWidth = this.getViewportWidth();
    let _self = this;
    const desktopOrMobile = screenWidth < 768 ? 'mobile' : 'desktop';
    let adsToFill = $('div[data-ad-display="' + desktopOrMobile + '"]'),
    adSizesToLoad = screenWidth < 768 ? mobileAdSizes : desktopAdSizes,
    lazyLoadingAds = [],
    regularAds = [];

    // Split up results into instantly served (regular) ads
    // and lazy loading ads, which only load when scrolled
    // into the viewport.
		// have to use $.each because its a jQuery object 
    $.each(adsToFill, function() {
      for(var i = 0; i < this.attributes.length; i++) {
        if((this.attributes[i].name === 'data-ad-lazy')
          && ( !_self.containsAd(lazyLoadingAds, this.id) ) ) {
          lazyLoadingAds.push({
            "id": this.id,
            "adType": this.attributes[i].nodeValue,
            "display": _self.findDisplayValue(this.attributes),
            "script": adSizesToLoad[this.attributes[i].nodeValue].script,
						"fallback": adSizesToLoad[this.attributes[i].nodeValue].fallback,
						"pct": adSizesToLoad[this.attributes[i].nodeValue].pct
          });
        }
      }

      for(var j = 0; j < this.attributes.length; j++) {
        if( (this.attributes[j].name !== 'data-ad-lazy')
        && ( !_self.containsAd(lazyLoadingAds, this.id) )
        && ( !_self.containsAd(regularAds, this.id)) ) {
          regularAds.push({
            "id": this.id,
						"pct" : adSizesToLoad[this.dataset.adType]['pct'],
						"fallback" : adSizesToLoad[this.dataset.adType]['fallback'],
            "adType": _self.findNodeValue(this.attributes),
            "display": _self.findDisplayValue(this.attributes),
            "script" : adSizesToLoad[_self.findNodeValue(this.attributes)].script
          });
        }
      }
    });

    var allAdsToLoad = {
      regularAds: regularAds,
      lazyLoadingAds : lazyLoadingAds
    };
    return allAdsToLoad;
  }

	//gets display (desktop or mobile) value from html element's array of attributes
  static findDisplayValue (arr, value) {
    for(var i = 0; i < arr.length; i++) {
      if(arr[i].name === 'data-ad-display') {
        return arr[i].nodeValue;
      }
    }
  }

	// Gets data-ad-type attr from html element's array of attributes
  static findNodeValue (arr, value) {
    for(var i = 0; i < arr.length; i++) {
      if(arr[i].name === 'data-ad-type') {
        return arr[i].nodeValue;
      }
    }
  }

	// self explanatory
  static getViewportWidth() {
		return w.innerWidth;
	}

	// universal place ad function
 	static placeAd(id, script) {
    if(id.indexOf('#') !== 0) {
      id = '#' + id;
    }
    $(id).append(script);
    $(id).css({'display':'block'});
    this.adOnPage(id);
  }

  // fire ad placements for each non-lazy ad
  static placeRegularAds (adList) {
    let _self = this;
    for(let i = 0; i < adList.length; i++) {
      _self.placeAd(adList[i].id, adList[i].script);
    }
  }

	// creates object of lazy loading ads
	// then passes them to detectInViewport()
  static setupLazyAds(adList) {
    let adsToWatch = [];
		adList.forEach((cv) => {
			const elm = d.getElementById(cv.id);
      adsToWatch.push({
        "elm": elm,
        "id": cv.id,
        "adType": cv.adType,
        "script" : cv.script
      });
		});
    this.detectInViewport(adsToWatch);
  }
}

export default MCAds;
