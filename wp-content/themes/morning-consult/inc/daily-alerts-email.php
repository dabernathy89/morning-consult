<?php

class McDailyAlertsEmail {
    public function __construct() {
        add_action('acf/save_post', array($this,'maybe_populate_news_ticker_stories'), 10);
    }

    protected function get_previous_day_alerts($today, $yesterday, $cat_id) {
        $args = array(
            'post_type' => array('mc_alert','post'),
            'posts_per_page' => -1,
            'cat' => $cat_id,
            'date_query' => array(
                'after' => array(
                    'year' => (string)$yesterday['year'],
                    'month' => (string)$yesterday['mon'],
                    'day' => (string)$yesterday['mday'],
                    'hour' => '14',
                    'minute' => '00',
                    'second' => '00'
                ),
                'before' => array(
                    'year' => (string)$today['year'],
                    'month' => (string)$today['mon'],
                    'day' => (string)$today['mday'],
                    'hour' => (string)$today['hours'],
                    'minute' => (string)$today['minutes'],
                    'second' => (string)$today['seconds']
                )
            )
        );

        $alerts = get_posts($args);

        return $alerts;
    }

    public function maybe_populate_news_ticker_stories($post_id) {
        $process = get_field('daily_alerts_generate_articles', $post_id);

        if (!$process) {
            return;
        }

        update_field('daily_alerts_generate_articles', false, $post_id);

        $cat = morning_consult_which_category($post_id);
        $cat_id = get_category_by_slug($cat);
        $cat_id = $cat_id->term_id;

        $today = getdate(current_time('timestamp'));
        $yesterday = getdate(get_the_date('U', $post_id) - 86400);

        $alerts = $this->get_previous_day_alerts($today, $yesterday, $cat_id);

        foreach ($alerts as $alert) {
            $row_data = array(
                'title' => get_the_title($alert->ID),
                'url' => get_permalink($alert->ID),
                'excerpt' => mc_excerpt_by_id($alert->ID, 30),
                'date' => get_the_date('n/j/y g:i A', $alert->ID)
            );
            add_row(
                'field_56b0b96b69520',
                $row_data,
                $post_id
            );
        }
    }
}

$mc_daily_alerts_email = new McDailyAlertsEmail();