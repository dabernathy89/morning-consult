<?php
/**
 * Template name: Fifty States
 *
 * @package Morning Consult 2015
 */

get_header(); ?>

<script type="text/javascript" src="<?php echo get_template_directory_uri() . '/js/highmaps/js/highmaps.src.js'; ?>"></script>
<script src="https://code.highcharts.com/mapdata/countries/us/us-all.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() . '/js/fifty-states.js?v=20160412'; ?>"></script>

    <div class="page fifty-state">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <?php while ( have_posts() ) : the_post(); ?>

                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <header class="entry-header">
                                <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                                <div class="entry-meta">
                                  <span class="author vcard">
                                    <?php the_author(); ?>
                                  </span>&nbsp;&nbsp;<span class="separator">|</span>&nbsp;&nbsp;
                                  <a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
                                    <time class="entry-date published updated" datetime="<?php echo get_the_date( 'c' ); ?>">
                                      <?php the_date(); ?>
                                    </time>
                                  </a>
                                </div><!-- .entry-meta -->
                            </header><!-- .entry-header -->
                            <div class="lead-paragraph">
                              <?php the_field('page_introduction'); ?>
                            </div>

                            <div class="entry-content">

                                <?php get_template_part( 'content', 'fifty-states' ); ?>

                            </div><!-- .entry-content -->
                        </article><!-- #post-## -->

                    <?php endwhile; // end of the loop. ?>

                </div>
            </div>
        </div>
    </div><!-- #primary -->

<?php get_footer(); ?>
