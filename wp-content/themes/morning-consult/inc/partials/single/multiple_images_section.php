<?php $mc_post_section_images = get_sub_field('images'); ?>
<div class="container multiple-images images-<?php echo count($mc_post_section_images); ?>">
    <div class="row">
        <?php
            if( have_rows('images') ):
                $mc_post_section_images_counter = 0;

                if (count($mc_post_section_images) == 2 || count($mc_post_section_images) == 4):
                    echo "<div class='col-xs-10 col-sm-offset-1'><div class='row'>";
                endif;

                while( have_rows('images') ): the_row();

                    $image = get_sub_field('image');

                    if (count($mc_post_section_images) == 2):

                        echo '<div class="col-sm-6">';
                        echo '<img src="' . $image['url'] . '" alt="' . $image['caption'] . '">';
                        echo "</div>";

                    elseif(count($mc_post_section_images) == 3):

                        echo '<div class="col-sm-4">';
                        echo '<img src="' . $image['url'] . '" alt="' . $image['caption'] . '">';
                        echo "</div>";

                    elseif(count($mc_post_section_images) == 4):

                        if ($mc_post_section_images_counter == 2) {
                            echo '</div><div class="row">';
                        }

                        echo '<div class="col-sm-6">';
                        echo '<img src="' . $image['url'] . '" alt="' . $image['caption'] . '">';
                        echo "</div>";

                    endif;

                    $mc_post_section_images_counter++;
                    unset($image);

                endwhile;

                if (get_sub_field('images_caption')) : ?>

                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <span class="caption"><?php the_sub_field('images_caption'); ?></span>
                        </div>

                <?php endif;

                if (count($mc_post_section_images) == 2 || count($mc_post_section_images) == 4):
                    echo "</div></div>";
                endif;
            endif;

            unset($mc_post_section_images, $mc_post_section_images_counter);
        ?>
    </div>
</div>