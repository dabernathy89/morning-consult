<?php
/**
 * Morning Consult ESP Integrations Create Campaign Sfmc
 * @version 0.1.0
 * @package Morning Consult ESP Integrations
 */

class MCESPI_Create_Campaign_Sfmc {
    /**
     * Push an HTML email up to Salesforce Marketing Cloud
     *
     * @since  0.1.0
     * @return void
     */


    /**
     * Checks to see if campaign already exists, if not
     * then calls create_campaign and creates it
     *
     * @param int $post_id WordPress post ID
     * @param string $email_content string of HTML content
     * @return json_error or json_success message
     */
    public function try_create_campaign($post_id, $email_content) {
      $sfmc_client = new ET_Client();
      $campaign_exists = $this->check_if_campaign_exists($sfmc_client, $post_id);

      //if campaign exists, reply with confirmation and ID
      if($campaign_exists !== false) {
        $exists_message = array(
          'message' => 'Campaign already exists in Salesforce. ID: ',
          'data' => $campaign_exists[0]->ID
        );
        wp_send_json_success($exists_message);
      }
      //if campaign does not exist, create it
      else if($campaign_exists === false) {
        $this->create_campaign($sfmc_client, $post_id, $email_content);
      }
      else {
        wp_send_json_error(array(
          'message' => 'Error creating campaign',
          'data' => ''
        ));
      }
    }

    /**
     *
     * Checks to see if campaign exists, if it does then
     * sends using send_with_classification based on category param
     *
     * @param int $post_id WordPress post ID
     * @param string $category WordPress post category
     * @return json_success or json_error message
     */
    public function try_send_campaign($post_id, $category) {

      try {
        $sfmc_client = new ET_Client();
        $campaign_exists = $this->check_if_campaign_exists($sfmc_client, $post_id);
				$post_type = get_post_type($post_id);

        if($campaign_exists !== false) {
          $email_id = $campaign_exists[0]->ID;
          $list_ids = $this->get_list_ids($category, $post_type);
          $this->send_with_classification($email_id, $list_ids['listID'], $list_ids['sendClassification']);
        }
        // if campaign doesnt exist, send message saying create it.
        else if($campaign_exists === false) {
          $exists_message = array(
            'message' => 'Campaign does not exist in Salesforce. Please create it first.',
            'data' => ''
          );
          wp_send_json_success($exists_message);
        }
      }

      catch (Exception $e) {
        wp_send_json_error(array(
          'message' => 'Error Sending Campaign: ',
          'data' => $e->getMessage()
        ));
      }
    }

    private function create_campaign($sfmc_client, $post_id, $email_content) {
      try {
        //create client and POST content
        $postEmail = new ET_Email();
        $postEmail->authStub = $sfmc_client;
        $postEmail->props = array(
            "CustomerKey" => 'mc_sfmc_ck_' . $post_id,
            "Name" => get_the_title($post_id),
            "Subject" => get_the_title($post_id),
            "HTMLBody" => $email_content,
            "EmailType" => "HTML",
            "IsHTMLPaste" => "true"
        );
        // POST the email to SFMC
        $postResult = $postEmail->post();

        // handle successful POST, but not successful transaction
        if ( $postResult->status === false || $postResult->results[0]->StatusCode === "Error" ) {
          if ( isset( $postResult->results[0]->StatusMessage ) ) {
            wp_send_json_error( $postResult->results[0]->StatusMessage );
          }
        }
        //if successful, send success message
        else if($postResult->status === true) {
          $success_message = array(
            'message' => 'Campaign Created successfully: ',
            'data' => $postResult->results[0]->NewID
          );
          wp_send_json_success($success_message);
        }
      }

      catch(Exception $e) {
        $error_message = array();
        $error_message['exception'] = $e;
        $error_message['message'] = $e->getMessage();
        wp_send_json_error($error_message);
      }
    }

    public function get_list_ids($category, $post_type) {
			$testarray = array(
				"Test_for_JB" => array(
				"listID" => 769,
				"senderProfile" => 999,
				"sendClassification" => 999
			));
			switch($post_type) {
				case 'mc_brief':
					$brief_list_ids = require_once('brief-list-ids.php');
					return $brief_list_ids[$category];
				case 'mc_daily_alerts':
					$ticker_list_ids = require_once('ticker-list-ids.php');
					return $ticker_list_ids[$category];
				case 'template_email':
					$template_test_ids = require_once('brief-list-ids.php');
					return $template_test_ids['Test_for_JB'];
			}
			
    }

    private function check_if_campaign_exists($sfmc_client, $post_id) {
      try {
        $temp_email = new ET_Email();
        $temp_email->authStub = $sfmc_client;
        $temp_email->props = array(
          'ID',
          'CustomerKey',
          'Name',
          'Subject',
          'CategoryID'
        );

        $temp_email->filter = array(
          'Property' => 'CustomerKey',
          'SimpleOperator' => 'equals',
          'Value' => 'mc_sfmc_ck_' . $post_id
        );

        $res = $temp_email->get();

        if(count($res->results) > 0) {
          return $res->results;
        }
        else return false;
      }

      catch(Exception $e) {
        wp_send_json_error(array(
          'message' => $e->getMessage(),
          'data' => ''
        ));
      }

    }

    private function send_with_classification($email_id, $list_id, $send_classification_id) {
      try {
        $sfmc_client = new ET_Client();
        $res = $sfmc_client->SendEmailToList($email_id, $list_id, $send_classification_id);

        if($res->status !== false) {
          wp_send_json_success(array(
            'message' => 'Sent Successfully to List ID: ',
            'data' => $res->results[0]->Object->SendDefinitionList->List->ID
          ));
        }
        else if ($res->status === false) {
          wp_send_json_error(array(
            'message' => 'Failed to send to list: ',
            'data' => $res->message
          ));
        }
      }

      catch(Exception $e) {
        wp_send_json_error(array(
          'message' => $e->getMessage(),
          'data' => ''
        ));
      }
    }
}
