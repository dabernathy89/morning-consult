<?php

  $display_news_ticker_ad = apply_filters( 'mc-display-news-ticker-ad', true, get_the_ID() );

  $alerts_page = (int)( isset($_GET['alerts_page']) ) ? $_GET['alerts_page'] : 1;

  $alerts_args = array (
    'post_type'              => array('mc_alert', 'mc_brief'),
    'posts_per_page'         => '10',
    'paged'                  => $alerts_page
  );

  if (is_singular()) {
    $alerts_args['post__not_in'] = array(get_the_ID());
    $category_ids = morning_consult_which_categories_ids();
    if ($category_ids) {
      $alerts_args['category__in'] = $category_ids;
    }
  }

  if (is_page_template('page-section-home.php' )) {
    $section_cat = get_field('section_home_category');
    $alerts_args['cat'] = $section_cat->term_id;
  }

  $alerts = new WP_Query($alerts_args);

  if( $alerts->have_posts() ): ?>

    <!-- <div id="top-ticker-alert" data-ad-type="morningconsult_1" data-ad-display="desktop" class="ad-container ticker-alert-top"></div> -->
		<div id="jumpyield-300-600" data-ad-type="jumpyield_300_600_Control" data-ad-display="desktop" class="ad-container ticker-alert-top"></div>

    <div class="alerts">
      <div class="section-title-wrapper">
        <img src="<?php echo get_template_directory_uri(). '/img/mc-logo-ticker.svg'; ?>" alt="" class="section-title-img">
        <h3 class="section-title">News Ticker</h3>
      </div>

      <div class="load-more-wrapper" id="alerts-load-more-wrapper">
        <?php $ad_count = 2; ?>

      <?php while( $alerts->have_posts() ): $alerts->the_post(); ?>
        <?php //every 2 alerts after first 2 alerts, display an ad ?>
        <?php if ((($alerts->current_post % 2) === 0) && ($alerts->current_post >= 2) && $display_news_ticker_ad): ?>

          <!-- <div id="ticker_alert_<?php echo $ad_count; ?>" data-ad-type="morningconsult_<?php echo $ad_count; ?>" data-ad-display="desktop" data-ad-lazy="morningconsult_<?php echo $ad_count; ?>" class="ad-container ticker-alert"></div> -->
					<div id="ticker_alert_<?php echo $ad_count; ?>" 
					data-ad-type="morningconsult_newsticker_s<?php echo $ad_count; ?>_Control" 
					data-ad-display="desktop" 
					data-ad-lazy="morningconsult_newsticker_s<?php echo $ad_count; ?>_Control" 
					class="ad-container ticker-alert">
					</div>				
					
          <?php $ad_count = ($ad_count + 1); ?>
          <?php endif; ?>

          <article id="post-<?php the_ID(); ?>" <?php post_class('excerpt'); ?>>

            <header class="alert-entry-header">
              <?php the_title( '<h3 class="alert-entry-title"><a href="' . get_permalink() . '">', '</a></h3>' ); ?>
            </header>

            <div class="alert-entry-content">
              <?php echo mc_excerpt_by_id(get_the_ID(), 30); ?>
            </div>

            <div class="feature">
              <?php if (get_field('alert_feature')) : ?>
                <div class="embed-container">
                    <?php the_field('alert_feature'); ?>
                </div>
              <?php elseif (has_post_thumbnail()) : ?>
                <?php the_post_thumbnail('medium'); ?>
              <?php endif; ?>
            </div>

            <footer class="entry-footer meta">
              <?php morning_consult_category_links(get_the_ID(), " &bull; "); ?>
              <br>
              <a class="timeago" href="<?php echo get_permalink(); ?>">
                <?php the_time( 'F j, g:i a' ); ?>
              </a>
              <div class="share-icons">
                <a class="facebook" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(get_the_permalink()); ?>">
                  <i class="fa fa-facebook"></i>
                </a>
                <a class="twitter" href="http://twitter.com/share?url=<?php echo urlencode(get_the_permalink()); ?>&text=<?php the_title(); ?>">
                  <i class="fa fa-twitter"></i>
                </a>
                <a class="linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_the_permalink()); ?>">
                  <i class="fa fa-linkedin"></i>
                </a>
              </div>
            </footer>
          </article>
        <?php endwhile; ?>

        <a class="load-more load-more-alerts" href="" data-current-page="<?php echo $alerts_page; ?>">Load More <i class="fa fa-chevron-down"></i></a>
      </div>
    </div><!-- alerts -->
  <?php endif; wp_reset_postdata(); ?>
