<?php
    $author_id = get_the_author_meta('ID');
    $author_featured_image = get_field('author_archive_background_image', 'user_' . $author_id);
    $author_featured_image = morning_consult_get_featured_image_info($author_featured_image['id']);
		$author_email = get_field('staff_author_email', 'user_' . $author_id);
?>

<div class="author-info">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4 pull-right">
                <div class="content">
                    <img src="<?php echo $author_featured_image['url']; ?>" class="bio-image">
                </div>
            </div>
            <div class="col-sm-6 col-md-8">
                <div class="content">
                    <h1 class="name"><?php echo get_the_author_meta( 'first_name' ); ?> <?php echo get_the_author_meta( 'last_name' ); ?></h1>
                    <p class="job-title"><strong><?php the_field('author_job_title', 'user_' . $author_id); ?></strong></p>
										<a class="email" href="mailto:<?php echo $author_email; ?>"><?php echo $author_email; ?></a>
                    <?php
                        $author_twitter = strtolower(get_the_author_meta( 'twitter' ));
                        if ($author_twitter && strpos($author_twitter, 'twitter.com') !== false) :
                            echo '<span class="slashes"> // </span><a class="twitter" target="_blank" href="' . $author_twitter . '">@' . substr($author_twitter, strlen('twitter.com/') + strpos($author_twitter, 'twitter.com/')) . '</a>';
												elseif ($author_twitter && strpos($author_twitter, '@') !== false) :
													echo '<span class="slashes"> // </span><a class="twitter" target="_blank" href="https://www.twitter.com/' . $author_twitter . '">@' . trim($author_twitter, '@') . '</a>';
                        elseif ($author_twitter) :
                            echo '<span class="slashes"> // </span><a class="twitter" target="_blank" href="http://www.twitter.com/' . $author_twitter . '">@' . $author_twitter . '</a>';
                        endif;
                    ?>
                    <div class="bio">
                        <?php the_field('author_long_bio', 'user_' . $author_id); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>