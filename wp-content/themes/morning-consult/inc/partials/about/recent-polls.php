<?php
    $recent_polls_args = array (
        'post_type'              => 'mc_poll',
        'posts_per_page'         => '4',
    );

    $recent_polls = new WP_Query($recent_polls_args);

    if( $recent_polls->have_posts() ): while( $recent_polls->have_posts() ): $recent_polls->the_post();
    ?>

    <div class="col-xs-6 col-md-3">
        <a href="<?php the_permalink(); ?>">
            <h4 class="title"><?php the_title(); ?></h4>
            <?php if ( has_post_thumbnail() ) {
                the_post_thumbnail('medium');
            } ?>
        </a>
    </div>

    <?php
    endwhile;
    endif;
    wp_reset_postdata();
?>