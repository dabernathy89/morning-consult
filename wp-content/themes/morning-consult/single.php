<?php
/**
 * The template for displaying all single posts.
 *
 * @package Morning Consult 2015
 */

get_header(); ?>

<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-7 col-lg-8 article-wrap">

			<?php while ( have_posts() ) : the_post(); ?>
				<?php if ( has_post_thumbnail() ) : ?>
					<?php $post_featured_image = morning_consult_get_featured_image_info(); ?>

						<?php the_post_thumbnail('large'); ?>
						<?php if ($post_featured_image['caption']) : ?>
							<p class="featured-image-caption"><?php echo $post_featured_image['caption']; ?></p>
						<?php endif; ?>

				<?php endif; ?>

				<div class="single">

					<?php get_template_part( 'content', 'single' ); ?>

				</div>

			<?php endwhile; ?>

		</div>

		<div id="ad-story-single" data-ad-type="morningconsult_1" data-ad-lazy="morningconsult_1" data-ad-display="mobile" class="ad-container story-single"></div>
	    <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4 alerts-wrap">
	        <?php get_template_part( 'inc/partials/alerts' ); ?>
	    </div>
	</div>
</div>

<?php get_footer(); ?>
