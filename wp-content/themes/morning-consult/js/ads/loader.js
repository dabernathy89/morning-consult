// wait for gpt to be ready, then call init functions
let gptAdLoader = window.setInterval(function() {

	if(window.googletag && window.googletag.apiReady) {
		window.clearInterval(gptAdLoader);
		let MCAdsReady = new Event('MCAdsReady');

		// kicks off placement of ad scripts
		$(document).ready(() => {
			window.dispatchEvent(MCAdsReady);
		});
	}
}, 100)



export default gptAdLoader;
