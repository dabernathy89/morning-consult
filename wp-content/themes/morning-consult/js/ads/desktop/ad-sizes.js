let adSizes = {
	/* uncomment these if we need to drop jumpyield support
	"morningconsult_1" : {
		sizes: [[160, 600], [300, 250], [300, 600]],
		script: "<!-- /135698611/morningconsult_1 --><div id='div-gpt-ad-1467116217077-0'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1467116217077-0'); });</script></div>",
		name: '/135698611/morningconsult_1',
		adId: 'div-gpt-ad-1467116217077-0'
	},
	"morningconsult_2" : {
		sizes: [[300, 600], [300, 250]],
		script: "<!-- /135698611/morningconsult_2 --><div id='div-gpt-ad-1467116217077-1'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1467116217077-1'); });</script></div>",
		name: '/135698611/morningconsult_2',
		adId: 'div-gpt-ad-1467116217077-1'
	},
	"morningconsult_3" : {
		sizes: [300, 250],
		script: "<!-- /135698611/morningconsult_3 --><div id='div-gpt-ad-1467116217077-2' style='height:250px; width:300px;'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1467116217077-2'); });</script></div>",
		name: '/135698611/morningconsult_3',
		adId: 'div-gpt-ad-1467116217077-2'
	},
	"morningconsult_4" : {
		sizes: [300, 250],
		script: "<!-- /135698611/morningconsult_4 --><div id='div-gpt-ad-1467116217077-3' style='height:250px; width:300px;'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1467116217077-3'); });</script></div>",
		name: '/135698611/morningconsult_4',
		adId: 'div-gpt-ad-1467116217077-3'
	},
	"morningconsult_5" : {
		sizes: [300, 250],
		script: "<!-- /135698611/morningconsult_5 --><div id='div-gpt-ad-1467116217077-4' style='height:250px; width:300px;'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1467116217077-4'); });</script></div>",
		name: '/135698611/morningconsult_5',
		adId: 'div-gpt-ad-1467116217077-4'
	},
	*/

	"jumpyield_300_600_Floor1" : {
		sizes: [300, 600],
		script: "<!-- /101659253/morningconsult_newsticker_300x600_Floor1 --><div id='div-gpt-ad-8398195469801-0' style='height:600px; width:300px;'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-8398195469801-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_300x600_Floor1',
		adId: 'div-gpt-ad-8398195469801-0',
	},
	"jumpyield_300_600_Control" : {
		sizes: [300, 600],
		script: "<!-- /101659253/morningconsult_newsticker_300x600_Control --><div id='div-gpt-ad-6639247135062-0' style='height:600px; width:300px;'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-6639247135062-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_300x600_Control',
		adId: 'div-gpt-ad-6639247135062-0',
		pct: 0.95,
		fallback: "jumpyield_300_600_Floor1"
	},
	"morningconsult_newsticker_s2_Control" : {
		sizes: [[300, 600], [300,250]],
		script: "<!-- /101659253/morningconsult_newsticker_s2_300x600_Control --><div id='div-gpt-ad-9911945492688-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-9911945492688-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_s2_300x600_Control',
		adId: 'div-gpt-ad-9911945492688-0',
		pct: 0.95,
		fallback: 'morningconsult_newsticker_s2_Floor1'
	},
	"morningconsult_newsticker_s2_Floor1" : {
		sizes: [300, 600],
		script: "<!-- /101659253/morningconsult_newsticker_s2_300x600_Floor1 --><div id='div-gpt-ad-5579869292223-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-5579869292223-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_s2_300x600_Floor1',
		adId: 'div-gpt-ad-5579869292223-0'
	},
	"morningconsult_newsticker_s3_Control" : {
		sizes: [300,250],
		script: "<!-- /101659253/morningconsult_newsticker_s3_300x250_Control --><div id='div-gpt-ad-5849151333972-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-5849151333972-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_s3_300x250_Control',
		adId: 'div-gpt-ad-5849151333972-0',
		pct: 0.95,
		fallback: 'morningconsult_newsticker_s3_Floor1'
	},
	"morningconsult_newsticker_s3_Floor1" : {
		sizes: [300,250],
		script: "<!-- /101659253/morningconsult_newsticker_s3_300x250_Floor1 --><div id='div-gpt-ad-5291563273390-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-5291563273390-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_s3_300x250_Floor1',
		adId: 'div-gpt-ad-5291563273390-0',
	},
	"morningconsult_newsticker_s4_Control": {
		sizes: [300,250],
		script: "<!-- /101659253/morningconsult_newsticker_s4_300x250_Control --><div id='div-gpt-ad-4583788211015-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-4583788211015-0'); });</script></div>",
		name: "/101659253/morningconsult_newsticker_s4_300x250_Control",
		adId: "div-gpt-ad-4583788211015-0",
		pct: 0.95,
		fallback: 'morningconsult_newsticker_s4_Floor1'
	},
	"morningconsult_newsticker_s4_Floor1" : {
		sizes: [300,250],
		script: "<!-- /101659253/morningconsult_newsticker_s4_300x250_Floor1 --><div id='div-gpt-ad-1546157942493-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1546157942493-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_s4_300x250_Floor1',
		adId: 'div-gpt-ad-1546157942493-0',
	},
	"morningconsult_newsticker_s5_Control" : {
		sizes: [300,250],
		script: "<!-- /101659253/morningconsult_newsticker_s5_300x250_Control --><div id='div-gpt-ad-5945178775929-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-5945178775929-0'); });</script></div>",
		name: "/101659253/morningconsult_newsticker_s5_300x250_Control",
		adId: "div-gpt-ad-5945178775929-0",
		pct: 0.95,
		fallback: 'morningconsult_newsticker_s5_Floor1'
	},
	"morningconsult_newsticker_s5_Floor1" : {
		sizes: [300,250],
		script: "<!-- /101659253/morningconsult_newsticker_s5_300x250_Floor1 --><div id='div-gpt-ad-6919211697350-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-6919211697350-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_s5_300x250_Floor1',
		adId: 'div-gpt-ad-6919211697350-0'
	}
}

export default adSizes;
