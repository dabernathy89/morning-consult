(function(w, d, $) {

  function setupClickHandlers() {
    $('.icon-container button').click(function() {
      $('html, body').animate({
        scrollTop: $('#get-started').offset().top
      }, 600)
    });

    $('.icon-icon').click(function() {
      $('html, body').animate({
        scrollTop: $($(this).data('target')).offset().top - 100
      })
    })
  }

  function init() {
    setupClickHandlers();
  }

  $(d).ready(function() {
    init();
  });

})(window, document, jQuery);
