(function(w,d,$) {

  function initHandlebars() {
    Handlebars.registerHelper('plusOne', function(index) {
      return (parseInt(index) +1);
    });
    Handlebars.registerHelper('minusOne', function(unknown) {
      return (parseInt(unknown) -1);
    });

    Handlebars.registerHelper('getMCILink', function(name) {
      var nameToLink = {
				"Bernard Sanders" : "https://morningconsultintelligence.com/examine?v=TEVHMl9CZXJuYXJkX1NhbmRlcnM%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
				"Susan Collins" : "https://morningconsultintelligence.com/examine?v=TEVHMl9TdXNhbl9Db2xsaW5z&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
				"John Thune" : "https://morningconsultintelligence.com/examine?v=TEVHMl9Kb2huX1RodW5l&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
				"Thomas Carper" : "https://morningconsultintelligence.com/examine?v=TEVHMl9UaG9tYXNfQ2FycGVy&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
				"Patrick Leahy" : "https://morningconsultintelligence.com/examine?v=TEVHMl9QYXRyaWNrX0xlYWh5&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
				"Lisa Murkowski" : "https://morningconsultintelligence.com/examine?v=TEVHMl9MaXNhX011cmtvd3NraQ%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
				"Brian Schatz" : "https://morningconsultintelligence.com/examine?v=TEVHMl9Ccmlhbl9TY2hhdHo%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
				"Charles Schumer" : "https://morningconsultintelligence.com/examine?v=TEVHMl9DaGFybGVzX1NjaHVtZXI%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
				"John Hoeven" : "https://morningconsultintelligence.com/examine?v=TEVHMl9Kb2huX0hvZXZlbg%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
				"John Reed" :  "https://morningconsultintelligence.com/examine?v=TEVHMl9Kb2huX1JlZWQ%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
				"Mitch McConnell" : "https://morningconsultintelligence.com/examine?v=TEVHMl9NaXRjaF9NY0Nvbm5lbGw%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
				"Pat Roberts" : "https://morningconsultintelligence.com/examine?v=TEVHMl9QYXRfUm9iZXJ0cw%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
				"Harry Reid" : "https://morningconsultintelligence.com/examine?v=TEVHMl9IYXJyeV9SZWlk&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
				"John McCain" : "https://morningconsultintelligence.com/examine?v=TEVHMl9Kb2huX01jQ2Fpbg%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
				"Marco Rubio" : "https://morningconsultintelligence.com/examine?v=TEVHMl9NYXJjb19SdWJpbw%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
				"Lindsey Graham" : "https://morningconsultintelligence.com/examine?v=TEVHMl9MaW5kc2V5X0dyYWhhbQ%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
				"David Vitter" : "https://morningconsultintelligence.com/examine?v=TEVHMl9EYXZpZF9WaXR0ZXI%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
				"Joe Manchin" : "https://morningconsultintelligence.com/examine?v=TEVHMl9Kb2VfTWFuY2hpbg%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
				"Claire McCaskill" : "https://morningconsultintelligence.com/examine?v=TEVHMl9DbGFpcmVfTWNDYXNraWxs&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
				"Mark Kirk" : "https://morningconsultintelligence.com/examine?v=TEVHMl9NYXJrX0tpcms%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0",
				"Richard Durbin" : "https://morningconsultintelligence.com/examine?v=TEVHMl9SaWNoYXJkX0R1cmJpbg%3D%3D&d=bGVnZGF0&s=bW9ybmluZyBjb25zdWx0"
      }
      return nameToLink[name];
    });

    Handlebars.registerHelper('ifGreaterThan100', function(approve, unknown, disapprove, options) {
      var approve = parseInt(approve);
      var disapprove = parseInt(disapprove);
      var unknown = parseInt(unknown);
      var total = (approve + disapprove + unknown);

      if((total) > 100) {
        return options.fn(this);
      } else {
        return options.inverse(this);
      }
    });

    Handlebars.registerHelper('ifLessThan100', function(approve, unknown, disapprove, options) {
      var approve = parseInt(approve);
      var disapprove = parseInt(disapprove);
      var unknown = parseInt(unknown);
      var total = (approve + disapprove + unknown);

      if((total) < 100) {
        return options.fn(this);
      }
      else {
        return options.inverse(this);
      }
    });

    var source = $('#hb-governor-ranking').html();
    var template = Handlebars.compile(source);
    var sourceBottom = $("#hb-governor-ranking-bottom").html();
    var templateBottom = Handlebars.compile(sourceBottom);
    var top10Data = mc_senator_ratings_table_data_v2.top10;
    var bottom10Data = mc_senator_ratings_table_data_v2.bottom10;
    var top10html = template(top10Data);
    var bottom10html = templateBottom(bottom10Data);

    $('#top10').html(top10html);
    $('#bottom10').html(bottom10html);
  }

  // function initMap() {
  //   var govMapData = mc_senator_ratings_table_data_v2.raw_data;
  //   $('#map-content').highcharts('Map', {
  //     title: {
  //       text: 'Governor Approval Ratings by State',
  //       y: 30
  //     },
  //     series: [{
  //       data:  govMapData,
  //       mapData: Highcharts.maps['countries/us/us-all'],
  //       joinBy: "hc-key",
  //       borderColor: '#FFFFFF',
  //       borderWidth: 0.5,
  //       tooltip: {
  //         useHTML : true,
  //         headerFormat: '',
  //         pointFormat: '<span><strong>{point.governor}</strong>: {point.approve}%<br/>{point.name}</span>'
  //       },
  //       states: {
  //         hover: {
  //           color: "#EFEFEF",
  //         },
  //         normal: {
  //           animation: false
  //         }
  //       }
  //     }],
  //     credits: {
  //       enabled: false
  //     },
  //     colors: [
  //       'rgba(200, 41, 36, 1)',
  //       'rgba(200, 84, 57, 1)',
  //       'rgba(194, 113, 61, 1)',
  //       'rgba(188, 140, 64, 1)',
  //       'rgba(185, 162, 66, 1)',
  //       'rgba(181, 179, 72, 1)',
  //       'rgba(156, 178, 70, 1)',
  //       'rgba(134, 171, 71, 1)',
  //       'rgba(111, 166, 69, 1)',
  //       'rgba(65, 147, 54, 1)'
  //     ],

  //     legend: {
  //       itemDistance: 10,
  //       itemMarginTop: 55,
  //       itemHoverStyle: {
  //         "cursor" : "pointer"
  //       },
  //       verticalAlign: "top",
  //       valueDecimals: 0,
  //       symbolPadding: 2,
  //       margin: 10,
  //       layout: "horizontal",
  //       align: "center"
  //     },
  //     labels : {
  //       items: [{
  //         html: '<div></div><span>30%</span>',
  //         style: {
  //           bottom: "10px"
  //         }
  //       }]
  //     },
  //     colorAxis: {
  //       min: 20,
  //       max: 72,
  //       type: 'linear',
  //       minColor: '#e9dfed',
  //       maxColor: '#451b56',
  //       lineColor: '#000000'
  //     }
  //   });
  // }

  function init() {
    // initMap();
    initHandlebars();
  }

  $(d).ready(function() {
    init();
  })
})(window, document, jQuery);
