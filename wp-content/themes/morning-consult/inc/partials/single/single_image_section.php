<?php $single_large_image = get_sub_field('large_image'); ?>

<?php if (get_sub_field('image_size') === "full_screen_width") : ?>
    <div class="single-image full-width">
        <img src="<?php echo $single_large_image['url']; ?>" alt="<?php the_sub_field('image_caption'); ?>">
        <div class="container">
            <span class="caption"><?php the_sub_field('image_caption'); ?></span>
        </div>
    </div>
<?php elseif (get_sub_field('image_size') === "full_content_width") : ?>
    <div class="single-image container-width container">
        <img src="<?php echo $single_large_image['url']; ?>" alt="<?php the_sub_field('image_caption'); ?>">
        <span class="caption"><?php the_sub_field('image_caption'); ?></span>
    </div>
<?php endif; ?>

<?php unset($single_large_image); ?>