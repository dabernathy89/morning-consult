<?php
/**
 * Template name: Policy Landing Page
 *
 * @package Morning Consult 2016
 */

get_header(); ?>

  <div class="page policy-landing">

    <?php
      while ( have_posts() ) : the_post();
        $image = get_field('policy_landing_header_image');
    ?>
      <!-- <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>> -->

        <div class="container">
          <div class="row">
            <div class="col-xs-12">

              <header class="entry-header policy-landing__header">
                  <?php the_title( '<h1 class="policy-landing__header-headline entry-title">', '</h1>' ); ?>
              </header><!-- .entry-header -->

            </div><!-- .col-xs-12 -->
          </div><!-- .row -->
        </div><!-- .container -->

        <div class="policy-landing__header-image-container container-fluid">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">

                <div class="policy-landing__header-image-wrap">
                  <img class="policy-landing__header-image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                </div>

              </div><!-- .col-xs-12 -->
            </div><!-- .row -->
          </div><!-- .container -->
        </div><!-- .container-fluid -->

        <div class="container">
          <div class="row">
            <div class="col-xs-12">

              <div class="policy-landing__introduction-wrap">
                <div class="policy-landing__introduction lead-paragraph">
                  <?php the_field('policy_landing_introduction'); ?>
                </div>
              </div>

              <div class="policy-landing__topics-wrap entry-content">

                <?php get_template_part( 'content', 'policy-landing-topics' ); ?>

              </div><!-- .entry-content -->

            </div><!-- .col-xs-12 -->
          </div><!-- .row -->
        </div><!-- .container -->

      <!-- </article> --><!-- #post-## -->

    <?php endwhile; // end of the loop. ?>

  </div><!-- #primary -->

<?php get_footer(); ?>
