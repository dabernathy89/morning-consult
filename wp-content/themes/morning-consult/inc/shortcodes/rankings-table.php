<?php

class MCRankingsTable {

    public function __construct() {
        add_shortcode( 'mc_rankings_table', array($this,'rankings_table_shortcode') );
        add_filter( 'acf/load_field/type=message', array($this,'acf_shortcode_message') );
    }

    public function acf_shortcode_message( $field ) {

        global $post;

        if ($field['message'] === '[mc_rankings_table id="POST_ID"]') {
            $field['message'] = str_replace('POST_ID', $post->ID, $field['message']);
        }

        return $field;

    }


    public function rankings_table_shortcode( $atts ) {

        $html = "";

        extract( shortcode_atts(
            array(
                'id' => false,
            ), $atts )
        );

        if (!$id) {
            return $html;
        }

        $table_data = get_field('rankings',$id);
        $prev_table = get_field('previous_rankings_table',$id);
        if ($prev_table) {
            $prev_table_data = get_field('rankings',$prev_table);
        }

        $html .= '<div class="table-responsive mc-rankings-table-wrap"><table class="table table-striped mc-rankings-table">';
        $html .= '<thead>';
            $html .= '<tr>';
                $html .= '<th>Rank</th>';
                $html .= '<th>Race</th>';
                if (isset($prev_table_data)) :
                    $html .= '<th>Trends</th>';
                endif;
                $html .= '<th>Comments</th>';
            $html .= '</tr>';
        $html .= '</thead>';
        $html .= '<tbody>';

        foreach ($table_data as $index => $row) {
            $html .= '<tr>';
                $html .= '<td class="rank">' . (string)($index + 1) . '</td>';

                // Set up variables
                $bioguide_id = get_field('mc_race_bioguide_id', $row['race']->ID);
                if (has_post_thumbnail( $row['race']->ID )) {
                    $thumb_url_array = wp_get_attachment_image_src(get_post_thumbnail_id($row['race']->ID), 'medium', true);
                    $row_img = $thumb_url_array[0];
                } elseif ($bioguide_id) {
                    $row_img = 'https://theunitedstates.io/images/congress/225x275/' . $bioguide_id . '.jpg';
                } else {
                    $row_img = '';
                }

                $description = get_field('mc_race_description', $row['race']->ID);

                // Race
                $html .= '<td class="race">';
                    $html .= '<img class="race-img" src="' . $row_img . '" alt="Photo of ' . $row['race']->post_title . '">';
                    // Race description
                    $html .= '<div class="race-description">';
                        // Name
                        $html .= '<p class="name"><strong>';
                            $html .= $row['race']->post_title;
                        $html .= '</strong></p>';

                        // District
                        $html .= '<p class="description">' . $description . '</p>';
                    $html .= '</div>';
                $html .= '</td>';

                // Trending cell
                if (isset($prev_table_data)) :
                    $html .= '<td class="trending">';
                    $trend = $this->get_ranking_trend($row['race']->ID, $table_data, $prev_table_data);
                    if ($trend['current'] - $trend['old'] > 0) :
                        $html .= '<p class="movement down"><i class="fa fa-chevron-circle-down"></i> '
                              . (string)absint($trend['current'] - $trend['old']) . '</p>';
                        $html .= '<p class="previous">Prev: ' . $trend['old'] . '</p>';
                    elseif ($trend['current'] - $trend['old'] < 0) :
                        $html .= '<p class="movement up"><i class="fa fa-chevron-circle-up"></i> '
                              . (string)absint($trend['current'] - $trend['old']) . '</p>';
                        $html .= '<p class="previous">Prev: ' . $trend['old'] . '</p>';
                    else :
                        $html .= '<p class="movement none"><i class="fa fa-chevron-circle-right"></i> --</p>';
                        $html .= '<p class="previous">Same Position' . '</p>';
                    endif;
                    $html .= '</td>';
                endif;

                $html .= '<td class="comments"><div>' . $row['comments'] . '</div></td>';

            $html .= '</tr>';
        }

        $html .= '</tbody>';
        $html .= '</table></div>';

        return $html;

    }

    protected function get_ranking_trend($id, $table, $prev_table) {

        $trend = array();

        foreach ($table as $key => $value) {
            if ($value['race']->ID === $id) {
                $trend['current'] = $key + 1;
                break;
            }
        }

        unset($key,$value);

        foreach ($prev_table as $key => $value) {
            if ($value['race']->ID === $id) {
                $trend['old'] = $key + 1;
                break;
            }
        }

        return $trend;

    }
}