<?php
/**
 * Morning Consult ESP Integrations Create Campaign Ajax
 * @version 0.1.0
 * @package Morning Consult ESP Integrations
 */

class MCESPI_Create_Campaign_Ajax {
    /**
     * Parent plugin class
     *
     * @var   class
     * @since 0.1.0
     */
    protected $plugin = null;

    /**
     * Constructor
     *
     * @since  0.1.0
     * @param  object $plugin Main plugin object.
     * @return void
     */
    public function __construct( $plugin ) {
        $this->plugin = $plugin;
        $this->hooks();
    }

    /**
     * Initiate our hooks
     *
     * @since  0.1.0
     * @return void
     */
    public function hooks() {
        add_action( 'wp_ajax_mc_create_campaign', array( $this, 'create_campaign_callback' ) );
    }

    /**
     * Handle the AJAX request.
     *
     * @since  0.1.0
     * @return void
     */
    public function create_campaign_callback() {
        $url_query_parts = array();
        $url_query = parse_url($_POST['url'], PHP_URL_QUERY);
        parse_str($url_query,$url_query_parts);
        $post_id = $url_query_parts['post'];
        $post_type = get_post_type($post_id);

        if ($post_type === "mc_brief") {
            $meta_prefix = "brief_";
        } 
				elseif ($post_type === "mc_daily_alerts") {
            $meta_prefix = "daily_alerts_";
        }
				elseif ($post_type === "template_email") {
					$meta_prefix = 'template_email_';
				}

        $system = (array_key_exists('emailsystem', $_POST) && $_POST['emailsystem']) ? $_POST['emailsystem'] : "contactology";

        $sender_email = get_field($meta_prefix . 'sender_email_address',$post_id);
        $sender_name = get_field($meta_prefix . 'sender_name',$post_id);
        $email_content = get_field($meta_prefix . 'email_content',$post_id);

        // Inline CSS
        $css_inliner_response = wp_remote_post(
            'http://premailer.dialect.ca/api/0.1/documents',
            array(
                'timeout' => 15,
                'body' => array(
                    'html' => $email_content,
                    'base_url' => 'morningconsult.com',
										'preserve_styles' => 'false',
										'remove_comments' => 'true'
                )
            )
        );

        if (is_wp_error($css_inliner_response)) {
            wp_send_json_error( array(
							'message' => "While creating the campaign for the Morning Brief (Post ID {$post_id}), there was a problem with the CSS inliner tool. The error message was:\n ",
							'data' => $css_inliner_response->get_error_message() 
						));
        } else {
            $css_inliner_response_body = json_decode($css_inliner_response['body'],true);
            $html_link = $css_inliner_response_body['documents']['html'];
            $inlined_html_response = wp_remote_get($html_link);
            if (is_wp_error($inlined_html_response)) {
                wp_send_json_error( array(
									'message' => "While creating the campaign for the Morning Brief (Post ID {$post_id}), there was a problem with the CSS inliner tool. The error message was:\n",  
									'data' => $inlined_html_response->get_error_message()
								));
            } else {
                $email_content = $inlined_html_response['body'];
            }
        }

        remove_filter('the_title', 'wptexturize');
        remove_filter('the_title', 'convert_chars');

        if ($system === "inboxfirst") {
            $email_content = $this->remove_sfmc_tags( $email_content );
            $this->plugin->create_campaign_inboxfirst->create_campaign($post_id, $sender_email, $sender_name, $email_content, $meta_prefix);
        } elseif ($system === "sfmc") {
            $this->plugin->create_campaign_sfmc->try_create_campaign($post_id, $email_content);
        } elseif ($system === "sfmc-send") {
          $category = morning_consult_get_primary_category($post_id);
          $this->plugin->create_campaign_sfmc->try_send_campaign($post_id, $category);
        } elseif ($system === "sfmc-test") {
					//send only to test category pls
          $category = 'Test_for_JB';
					$this->plugin->create_campaign_sfmc->try_send_campaign($post_id, $category);
        }

        else {
            $email_content = $this->remove_sfmc_tags( $email_content );
            $this->plugin->create_campaign_campaigner->create_campaign($post_id, $sender_email, $sender_name, $email_content, $meta_prefix);
        }
    }

    public function remove_sfmc_tags( $email_content ) {
        $left_pos = strpos( $email_content, '<!-- sfmc -->' );
        $right_pos = strpos( $email_content, '<!-- //sfmc -->' );
        if ( $left_pos !== false && $right_pos !== false ) {
            $remove = substr( $email_content, $left_pos, ( $right_pos + strlen( '<!-- //sfmc -->' ) ) - $left_pos );
            $email_content = str_replace( $remove, '', $email_content );
        }
        return $email_content;
    }

}
