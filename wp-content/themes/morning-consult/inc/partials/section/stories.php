<?php
/**
 * The loop for posts in the 'stories' portion of the section home page.
 *
 * @package Morning Consult 2015
 */
?>

<?php

global $paged;

$temp_paged = $paged;
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

$section_cat = get_field('section_home_category');

$section_home_stories_args = array (
    'post_type'              => array('post','mc_opinion'),
    'posts_per_page'         => 9,
    'paged'                  => $paged,
    'post__not_in'           => array($section_home_featured_post_id)
);

if ($section_cat) {
    $section_home_stories_args['cat'] = $section_cat->term_id;
}

$section_home_stories = new WP_Query($section_home_stories_args);

if( $section_home_stories->have_posts() ): ?>

  <div class="articles load-more-wrapper" id="section-home-articles-wrapper">

  <?php $featured_img_left = false; ?>

  <?php $ad_counter = 1; ?>

  <?php while( $section_home_stories->have_posts() ): $section_home_stories->the_post(); ?>

    <?php if (get_post_type() === "post") {
        $featured_img_left = !$featured_img_left;
    }?>

    <?php
      if (!$featured_img_left && get_post_type() !== 'mc_opinion') :
        $post_layout = "content-title";
      elseif ($featured_img_left || get_post_type() === 'mc_opinion') :
        $post_layout = "title-content";
      endif;
    ?>

    <?php
      if($section_home_stories->current_post % 2 === 0 && $ad_counter < 6) :
    ?>

    <div id="ad-story-<?php echo $ad_counter; ?>" data-ad-type="morningconsult_<?php echo $ad_counter; ?>" data-ad-display="mobile" class="ad-container ad-story" style="display: none;"></div>

    <?php
      $ad_counter = ($ad_counter + 1);
      endif;
    ?>


    <article id="post-<?php the_ID(); ?>" <?php post_class('excerpt default clearfix layout-' . $post_layout); ?>>

      <div class="content-wrap left-of-title">
          <div class="entry-content">
					<?php echo mc_custom_excerpt( get_the_ID(), 30 ); ?>
          </div>
          <div class="category-tags clearfix">
              <?php morning_consult_category_links_square(); ?>
          </div>
      </div>

      <?php if (get_post_type() === "post" && has_post_thumbnail() ) : ?>
          <?php $thumb = morning_consult_get_featured_image_by_size('mc-morning-brief'); ?>
          <div class="thumbnail-title-wrap" data-bg="<?php echo $thumb; ?>">
      <?php else : ?>
          <div class="thumbnail-title-wrap">
      <?php endif; ?>
          <header class="entry-header">
            <?php if (get_post_type() === 'mc_opinion') : ?>
              <span class="opinion">Opinion</span>
            <?php endif; ?>
            <?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '">', '</a></h3>' ); ?>
            <span class="author vcard">
              <em>
                <?php $all_authors = get_coauthors(); ?>
                By <?php foreach ($all_authors as $authorkey => $author) : ?>
                  <?php include get_stylesheet_directory() . "/inc/partials/single/meta_author.php"; ?>
                <?php endforeach; ?>
              </em>
            </span>
          </header>
      </div>

      <div class="content-wrap right-of-title">
        <div class="entry-content">
					<?php echo mc_custom_excerpt( get_the_ID(), 30 ); ?>
        </div>
        <div class="category-tags">
          <?php morning_consult_category_links_square(); ?>
        </div>
      </div>


    </article>

  <?php endwhile; ?>

  <a class="load-more" href="<?php echo next_posts( $section_home_stories->max_num_pages, false ); ?>">Load More <i class="fa fa-chevron-down"></i></a>

  </div>

<?php endif; ?>
<?php $paged = $temp_paged; ?>
<?php wp_reset_postdata(); ?>
