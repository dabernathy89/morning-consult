<?php /*
* Template Name: Video Dial Test v2
*/

get_header();

?>
<?php 
$video1_id = get_field('video_1_id');
$video2_id = get_field('video_2_id');

$video_ids = array(
	'video1' => $video1_id,
	'video2' => $video2_id
);

wp_enqueue_script('video-dialtest-v2', get_template_directory_uri() . '/js/video-dial-test-v2.js');
wp_localize_script('video-dialtest-v2', 'mc_youtube_video_ids', $video_ids);

?>
<!-- <script src="<?php echo get_template_directory_uri() . '/js/video-dial-test-v2.js'; ?>"></script> -->

<?php while(have_posts()) : the_post(); ?>

<container class="container main-container">
	<row class="row">
		<main class="col-xs-12 col-sm-6 col-md-7 col-lg-8 main-column">
			<?php if ( has_post_thumbnail() ) : ?>
				<?php $post_featured_image = morning_consult_get_featured_image_info(); ?>

					<?php the_post_thumbnail('large'); ?>
					<?php if ($post_featured_image['caption']) : ?>
						<p class="featured-image-caption"><?php echo $post_featured_image['caption']; ?></p>
					<?php endif; ?>

			<?php endif; ?>
			<h1 class="page-title"><?php the_title(); ?></h1>

			<div class="entry-meta">
				<span class="author vcard">
					<?php the_author(); ?>
				</span>&nbsp;&nbsp;<span class="separator">|</span>&nbsp;&nbsp;
				<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
					<time class="entry-date published updated" datetime="<?php echo get_the_date( 'c' ); ?>">
						<?php the_date(); ?>
					</time>
				</a>
				<div class="share-icons">
					<a class="facebook" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(get_the_permalink()); ?>">
						<i class="fa fa-facebook"></i>
					</a>
					<a class="twitter" href="http://twitter.com/share?url=<?php echo urlencode(get_the_permalink()); ?>&text=<?php the_title(); ?>">
						<i class="fa fa-twitter"></i>
					</a>
					<a class="linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_the_permalink()); ?>">
						<i class="fa fa-linkedin"></i>
					</a>
				</div>
			</div><!-- .entry-meta -->

			<div class="intro-text text-content"><?php the_field('intro_paragraph'); ?></div>

			<div class="video-container">
				<h2 class="video-container--title"><?php the_field('video_1_title'); ?></h2>
				<div id="video1"></div>
				<div id="animation-1" class="dial-animation">
					<?php $video1 = get_field('video_1_image'); ?>
					<img style="width: 100%;" src="<?php echo $video1['url']; ?>" alt="" />
					<div id="progress-bar-1" class="progress-bar" data-width="0"></div>
				</div>
			</div>

			<div class="middle-text text-content"><?php the_field('middle_paragraph'); ?></div>

			<div class="video-container">
				<h2 class="video-container--title"><?php the_field('video_2_title'); ?></h2>
				<div id="video2"></div>
				<div id="animation-2" class="dial-animation">
					<?php $video2 = get_field('video_2_image'); ?>
					<img style="width: 100%;" src="<?php echo $video2['url']; ?>" alt="" />
					<div id="progress-bar-2" class="progress-bar" data-width="0"></div>
				</div>
			</div>

			<div class="end-text text-content"><?php the_field('end_paragraph'); ?></div>


		<footer class="single-footer">
			<?php
				$all_authors = get_coauthors();
				if ($all_authors) : ?>
					<div class="authors clearfix">
					<?php 
						foreach ($all_authors as $author) {
							$author_type = get_class($author);
							if ($author_type === "WP_User") {
								include "inc/partials/single/author_user.php";
							} elseif ($author_type === "stdClass") {
								include "inc/partials/single/author_guest.php";
							}
						}
					?>
					</div>
				<?php endif; ?>
			</footer><!-- .entry-footer -->

		</main>

		<div class="ticker-container col-xs-12 col-sm-6 col-md-5 col-lg-4">
		<?php include 'inc/partials/alerts.php'; ?>
		</div>

	</row>
</container>








<?php endwhile; ?>



<?php get_footer(); ?>
