<?php /**
* Template Name: State Governor Ratings - No Map
*
* @package Morning Consult 2016
*
*/

?>

<?php get_header(); ?>
<script src="<?php echo get_template_directory_uri() . '/js/handlebars/handlebars-v4.0.5.js'; ?>"></script>
<script src="<?php echo get_template_directory_uri() . '/js/state-governor-ratings-nomap.js'; ?>"></script>

<?php
$tables = json_decode( get_option('tablepress_tables'), true);
$table_id = get_field('table_id');
if( isset($table_id) && isset( $tables['table_post'][$table_id] ) ) {
  $table_data = get_post_field('post_content', $tables['table_post'][$table_id], 'raw');
  $formatted_data = mc_governor_table_data($table_data);
  wp_localize_script('morning-consult-all-js', 'mc_governor_table_data', $formatted_data);
}
?>

<?php while( have_posts() ) : the_post(); ?>

  <div class="container">
    <div class="row">
      <main id="content-area" class="col-xs-12 col-md-8">
        <h1><?php the_title(); ?></h1>
        <section class="intro-paragraph">
          <?php the_field('intro_paragraph'); ?>
        </section>

        <section id="tabs">
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#bottom10" data-toggle="tab">Highest Disapproval</a></li>
            <li role="presentation"><a href="#top10" data-toggle="tab">Highest Approval</a></li>
          </ul>

          <div class="tab-content row">
            <div id="top10" role="tabpanel" class="tab-pane fade  col-xs-12">
            </div>
            <div id="bottom10" role="tabpanel" class="tab-pane fade in active col-xs-12">
            </div>
          </div>
        </section>

        <section class="middle-paragraph">
          <?php the_field('middle_paragraph'); ?>
          <p class="table-hint visible-xs-block">&laquo; Scroll to View All Columns &raquo;</p>
        </section>

        <section class="table-data">
          <?php tablepress_print_table('id='.$table_id.''); ?>
        </section>
      </main>

      <div class="ticker-area col-xs-12 col-md-4">
        <?php get_template_part('inc/partials/alerts'); ?>
      </div>
    </div>
  </div>

  <script id="hb-governor-ranking" type="text/x-handlebars-template">
    <div class="hb-governor-legend">
      <ul>
        <li><div class="legend-color legend-green dark"></div>Approve</li>
        <li><div class="legend-color legend-grey"></div>Don't Know/Undecided</li>
        <li><div class="legend-color legend-red light"></div>Disapprove</li>
      </ul>
    </div>

    <div class="hb-governor-rows">
      <ul class="hb-gov-rows">
        {{#each this}}
        <li class="row">
          <div class="hb-gov-rank col-sm-1 col-xs-2">{{plusOne @index}}</div>
          <div class="hb-gov-photo col-sm-2 hidden-xs"><img src="{{this.photo}}" alt="" target="_blank"></div>
          <div class="hb-gov-name col-sm-9 col-xs-10">{{this.governor}} <span>({{this.party}}-{{this.stateAbbr}})</span></div>
          <div class="hb-gov-rating col-sm-9 col-xs-10">
            <i class="fa fa-thumbs-up dark" aria-hidden="true"></i>
            <div class="bar-container">
              {{#ifGreaterThan100 this.approve this.unknown this.disapprove}}
              <bar class="bar-approve dark" style="width: {{this.approve}}%"><span>{{this.approve}}%</span></bar>
              <bar class="bar-unknown" style="width: {{minusOne this.unknown}}%"></bar>
              <bar class="bar-disapprove light" style="width: {{this.disapprove}}%"><span style="float: right;">{{this.disapprove}}%</span></bar>
              {{else ifLessThan100 this.approve this.unknown this.disapprove}}
              <bar class="bar-approve dark" style="width: {{this.approve}}%"><span>{{this.approve}}%</span></bar>
              <bar class="bar-unknown" style="width: {{plusOne this.unknown}}%"></bar>
              <bar class="bar-disapprove light" style="width: {{this.disapprove}}%"><span style="float: right;">{{this.disapprove}}%</span></bar>
              {{else}}
              <bar class="bar-approve dark" style="width: {{this.approve}}%"><span>{{this.approve}}%</span></bar>
              <bar class="bar-unknown" style="width: {{this.unknown}}%"></bar>
              <bar class="bar-disapprove light" style="width: {{this.disapprove}}%"><span style="float: right;">{{this.disapprove}}%</span></bar>
              {{/ifGreaterThan100}}
            </div>
            <i class="fa fa-thumbs-down light" aria-hidden="true"></i>
          </div>
        </li>
        {{/each}}
        </div>
      </ul>
    </div></div>
  </script>

  <script id="hb-governor-ranking-bottom" type="text/x-handlebars-template">
    <div class="hb-governor-legend">
      <ul>
        <li><div class="legend-color legend-red dark"></div>Disapprove</li>
        <li><div class="legend-color legend-grey"></div>Don't Know/Undecided</li>
        <li><div class="legend-color legend-green light"></div>Approve</li>
      </ul>
    </div>

    <div class="hb-governor-rows">
      <ul class="hb-gov-rows">
        {{#each this}}
        <li class="row">
          <div class="hb-gov-rank col-sm-1 col-xs-2">{{plusOne @index}}</div>
          <div class="hb-gov-photo col-sm-2 hidden-xs"><img src="{{this.photo}}" alt="" target="_blank"></div>
          <div class="hb-gov-name col-sm-9 col-xs-10">{{this.governor}} <span>({{this.party}}-{{this.stateAbbr}})</span></div>
          <div class="hb-gov-rating col-sm-9 col-xs-10">
            <i class="fa fa-thumbs-down dark" aria-hidden="true"></i>
            <div class="bar-container">
              {{#ifGreaterThan100 this.approve this.unknown this.disapprove}}
              <bar class="bar-disapprove dark" style="width: {{this.disapprove}}%"><span>{{this.disapprove}}%</span></bar>
              <bar class="bar-unknown" style="width: {{minusOne this.unknown}}%"></bar>
              <bar class="bar-approve light" style="width: {{this.approve}}%"><span style="float: right;">{{this.approve}}%</span></bar>
              {{else ifLessThan100 this.approve this.unknown this.disapprove}}
              <bar class="bar-disapprove dark" style="width: {{this.disapprove}}%"><span>{{this.disapprove}}%</span></bar>
              <bar class="bar-unknown" style="width: {{plusOne this.unknown}}%"></bar>
              <bar class="bar-approve light" style="width: {{this.approve}}%"><span  style="float: right;">{{this.approve}}%</span></bar>
              {{else}}
              <bar class="bar-disapprove dark" style="width: {{this.disapprove}}%"><span>{{this.disapprove}}%</span></bar>
              <bar class="bar-unknown" style="width: {{this.unknown}}%"></bar>
              <bar class="bar-approve light" style="width: {{this.approve}}%"><span style="float: right;">{{this.approve}}%</span></bar>
              {{/ifGreaterThan100}}
            </div>
            <i class="fa fa-thumbs-up light" aria-hidden="true"></i>
          </div>
        </li>
        {{/each}}
        </div>
      </ul>
    </div></div>
  </script>

<?php endwhile; ?>

<?php get_footer(); ?>
