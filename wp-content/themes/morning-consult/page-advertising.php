<?php
/**
 * Template name: Advertising
 *
 * @package Morning Consult 2015
 */

get_header(); ?>

<?php while(have_posts()) : the_post(); ?>
  <script src="<?php echo get_template_directory_uri() .'/js/page-advertising.js' ?>"></script>

<container class="hero-container">
  <div class="hero-image" style="background-image: url(<?php echo get_template_directory_uri() . '/img/advertising-page/hero-image.jpg' ?>);">
    <div class="hero-transparent">
      <div class="hero-products">Our Products</div>
      <div class="spacer"></div>
    </div>
  </div>
</container>

<container class="container icon-container">
  <row class="row">
    <div data-target="#advertise-container" class="icon-icon col-xs-12 col-sm-6 col-md-3">
      <img src="<?php echo get_template_directory_uri() . '/img/advertising-page/icons/1.png' ?>" alt="">
      <h1>Advertise</h1>
    </div>
    <div data-target="#events-container" class="icon-icon col-xs-12 col-sm-6 col-md-3">
      <img src="<?php echo get_template_directory_uri() . '/img/advertising-page/icons/2.png' ?>" alt="">
      <h1>Events</h1>
    </div>
    <div data-target="#polling-container" class="icon-icon col-xs-12 col-sm-6 col-md-3">
      <img src="<?php echo get_template_directory_uri() . '/img/advertising-page/icons/3.png' ?>" alt="">
      <h1>Polling</h1>
    </div>
    <div data-target="#mci-container" class="icon-icon col-xs-12 col-sm-6 col-md-3">
      <img src="<?php echo get_template_directory_uri() . '/img/advertising-page/icons/4.png' ?>" alt="">
      <h1>Intelligence</h1>
    </div>

    <button class="btn">Get Started</button>

    <h1 class="icon-advertise">Advertise</h1>
  </row>
</container>

<div class="container-wrapper">
<container id="advertise-container" class="container advertise-container">
  <row class="row">
    <div class="col-xs-12 advertise-row">
      <row class="row">
        <img class="col-xs-4 col-md-2" src="<?php echo get_template_directory_uri() . '/img/advertising-page/icons/10.png' ?>" alt="">
        <div class="advertise-text-wrapper col-xs-8 col-md-10">
          <p class="advertise-header"><span>Email Newsletter</span> Ad Buyout</p>
          <div><?php the_field('email_newsletter'); ?></div>
        </div>
      </row>
    </div>
    <div class="col-xs-12 advertise-row">
      <row class="row">
        <img class="col-xs-4 col-md-2" src="<?php echo get_template_directory_uri() . '/img/advertising-page/icons/11.png' ?>" alt="">
        <div class="advertise-text-wrapper col-xs-8 col-md-10">
          <p class="advertise-header"><span>Sponsored</span> Email Release</p>
          <div><?php the_field('sponsored_email'); ?></div>
        </div>
      </row>
    </div>
    <div class="col-xs-12 advertise-row">
      <row class="row">
        <img class="col-xs-4 col-md-2" src="<?php echo get_template_directory_uri() . '/img/advertising-page/icons/12.png' ?>" alt="">
        <div class="advertise-text-wrapper col-xs-8 col-md-10">
          <p class="advertise-header"><span>Website</span> Ad Buyout</p>
          <div><?php the_field('website_ad'); ?></div>
        </div>
      </row>
    </div>
  </row>
</container>
</div>

<container id="events-container" class="container-fluid events-container">
  <row class="row">
  <div class="events-image" style="background-image: url(<?php echo get_template_directory_uri() . '/img/advertising-page/events-image-v3.jpg' ?>);">
    <div class="col-xs-12 col-md-5 col-md-offset-1">
      <h1>Events</h1>
      <?php the_field('events_paragraph'); ?>
    </div>
    <div class="col-xs-12 col-md-5 events-list">
      <h2>What we offer:</h2>

      <?php the_field('events_offer'); ?>

    </div>
  </div>
  </row>

</container>

<!-- use same css classes as advertise-container for polling -->
<container id="polling-container" class="container polling-container">
  <row class="row">
    <h1 class="col-xs-12">Polling & Market Research</h1>
    <div class="col-xs-12 advertise-row">
      <row class="row">
        <img class="col-xs-4 col-md-2" src="<?php echo get_template_directory_uri() . '/img/advertising-page/icons/5.png' ?>" alt="">
        <div class="advertise-text-wrapper col-xs-8 col-md-10">
          <p class="advertise-header"><span>National Public</span> Opinion Polling</p>
          <?php the_field('national_public'); ?>
        </div>
      </row>
    </div>
    <div class="col-xs-12 advertise-row">
      <row class="row">
        <img class="col-xs-4 col-md-2" src="<?php echo get_template_directory_uri() . '/img/advertising-page/icons/9.png' ?>" alt="">
        <div class="advertise-text-wrapper col-xs-8 col-md-10">
          <p class="advertise-header"><span>Public</span> Opinion Polling Subscriptions</p>
          <?php the_field('public_opinion'); ?>
        </div>
      </row>
    </div>
    <div class="col-xs-12 advertise-row">
      <row class="row">
        <img class="col-xs-4 col-md-2" src="<?php echo get_template_directory_uri() . '/img/advertising-page/icons/6.png' ?>" alt="">
        <div class="advertise-text-wrapper col-xs-8 col-md-10">
          <p class="advertise-header"><span>State Public</span> Opinion Polling</p>
          <?php the_field('state_public'); ?>
        </div>
      </row>
    </div>
    <div class="col-xs-12 advertise-row">
      <row class="row">
        <img class="col-xs-4 col-md-2" src="<?php echo get_template_directory_uri() . '/img/advertising-page/icons/7.png' ?>" alt="">
        <div class="advertise-text-wrapper col-xs-8 col-md-10">
          <p class="advertise-header"><span>Moment-To-Moment</span> Video Dial Testing</p>
          <?php the_field('moment_to_moment'); ?>
        </div>
      </row>
    </div>
    <div class="col-xs-12 advertise-row">
      <row class="row">
        <img class="col-xs-4 col-md-2" src="<?php echo get_template_directory_uri() . '/img/advertising-page/icons/8.png' ?>" alt="">
        <div class="advertise-text-wrapper col-xs-8 col-md-10">
          <p class="advertise-header"><span>Elite</span> Opinion Polling</p>
          <?php the_field('elite_opinion'); ?>
        </div>
      </row>
    </div>
  </row>
</container>

<container id="mci-container" class="container mci-container">
  <row class="row">
    <h1 class="mci-title col-xs-12"><a href="https://morningconsultintelligence.com" target="_blank">Morning Consult Intelligence</a></h1>
    <div class="mci-title-text col-xs-12">
      <?php the_field('mci_title_text'); ?>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3 mci-icons">
      <img src="<?php echo get_template_directory_uri() . '/img/advertising-page/icons/13.png' ?>" alt="">
      <h1>Search</h1>
      <?php the_field('mci_icon_search'); ?>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3 mci-icons">
      <img src="<?php echo get_template_directory_uri() . '/img/advertising-page/icons/14.png' ?>" alt="">
      <h1>Analyze</h1>
      <?php the_field('mci_icon_analyze'); ?>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3 mci-icons">
      <img src="<?php echo get_template_directory_uri() . '/img/advertising-page/icons/15.png' ?>" alt="">
      <h1>Share</h1>
      <?php the_field('mci_icon_share'); ?>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3 mci-icons">
      <img src="<?php echo get_template_directory_uri() . '/img/advertising-page/icons/16.png' ?>" alt="">
      <h1>Build</h1>
      <?php the_field('mci_icon_build'); ?>
    </div>
  </row>

</container>

<div class="container-wrapper blue">
  <container id="get-started" class="container get-started">
    <div class="row">
      <div class="text-container col-xs-12 col-md-6">
        <h1 class="get-title">Get Started!</h1>
        <?php the_field('get_started_contact'); ?>
        <p class="get-location-icon">729 15th Street NW, Washington DC 20005</p>
        <p class="get-tel-icon">202.506.1957</p>
      </div>
      <div class="form-container col-xs-12 col-md-6">
        <?php gravity_form('Advertise-Page', false, false, false, '', false); ?>
      </div>

    </div>
  </container>
</div>

<?php endwhile; ?>

<?php get_footer(); ?>
