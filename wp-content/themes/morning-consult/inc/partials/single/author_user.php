<?php
    $author_headshot = get_field('author_headshot', 'user_' . $author->ID);
?>


  <div class="author clearfix">
    <?php if ($author_headshot) : ?>
      <div class="author-photo col-xs-12 col-sm-4 col-md-3">
        <img src="<?php echo $author_headshot['sizes']['thumbnail']; ?>" alt="Author Photo">
      </div>
    <?php endif; ?>
    <div class="author-info col-xs-12 col-sm-8 col-md-9">
        <h4 class="name"><?php echo get_the_author_meta( 'first_name', $author->ID ); ?> <?php echo get_the_author_meta( 'last_name', $author->ID ); ?></h4>
        <?php
            $author_twitter = strtolower(get_the_author_meta( 'twitter', $author->ID ));
            $author_email = get_the_author_meta('staff_author_email', $author->ID);
            if($author_email): 
              echo '<a class="email" href="mailto:'.$author_email.'">'. $author_email. '</a>';
            endif;
            if ($author_twitter && strpos($author_twitter, 'twitter.com') !== false) :
              echo '<span class="slashes"> // </span><a class="twitter" target="_blank" href="' . $author_twitter . '">@' . substr($author_twitter, strlen('twitter.com/') + strpos($author_twitter, 'twitter.com/')) . '</a>';
            elseif ($author_twitter && strpos($author_twitter, '@') !== false) :
              echo '<span class="slashes"> // </span><a class="twitter" target="_blank" href="https://www.twitter.com/' . $author_twitter . '">@' . trim($author_twitter, '@') . '</a>';
            elseif ($author_twitter) :
              echo '<span class="slashes"> // </span><a class="twitter" target="_blank" href="https://www.twitter.com/' . $author_twitter . '">@' . $author_twitter . '</a>';
            endif;
        ?>
        <?php $author_email = get_the_author_meta('email', $author->ID); ?>
        <p><?php echo get_the_author_meta('description', $author->ID); ?></p>
        <a class="author-link" href="<?php echo get_author_posts_url($author->ID); ?>">See all posts by <?php echo get_the_author_meta( 'first_name', $author->ID ); ?></a>
    </div>
  </div>
