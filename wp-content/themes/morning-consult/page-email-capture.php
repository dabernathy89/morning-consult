<?php /*
* Template Name: Email Capture Page
*/
?>

<?php get_header(); ?>

<?php while(have_posts()) : the_post(); ?>

<container class="container">
	<row class="row">
		<main class="content-area col-xs-12 col-md-6 col-md-offset-3">
		<h1 class="content-title"><?php the_title(); ?></h1>
		<div><?php the_content(); ?></div>
		<div class="gravityform-container">
			<?php 
				gravity_form('Email Capture', false, false); 
			?>
		</div>
		</main>
	</row>
</container>

<?php endwhile; ?>

<?php get_footer(); ?>