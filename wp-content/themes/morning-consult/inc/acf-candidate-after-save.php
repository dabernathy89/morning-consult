<?php

function mc_after_candidate_save( $post_id ) {

    $bioguide_id = get_field('mc_candidate_bioguide_id',$post_id);

    if ($bioguide_id) {
        if (!get_field('mc_candidate_description',$post_id)) {

            $sunlight_api_key = get_option( 'mc_sunlight_congress_api_key', false );

            if ($sunlight_api_key) {
                $candidate = mc_after_candidate_save_get_legislator($sunlight_api_key, $bioguide_id);
                if ($candidate->chamber === 'house') {
                    $description = $candidate->state . "-" . str_pad($candidate->district, 2, "0", STR_PAD_LEFT);
                } else {
                    $description = $candidate->state_name;
                }

                update_field('mc_candidate_description', $description, $post_id);
            }
        }
    }

}

add_action('acf/save_post', 'mc_after_candidate_save', 20);

function mc_after_candidate_save_get_legislator($api_key, $bioguide_id) {
    $response = wp_remote_get(
        'http://congress.api.sunlightfoundation.com/legislators?apikey='
        . $api_key
        . '&bioguide_id=' . $bioguide_id
        . '&fields=first_name,last_name,nickname,name_suffix,party,state,district,title,chamber,bioguide_id,phone,office,website,contact_form,twitter_id,facebook_id,state_name',
        array()
    );

    $results = json_decode($response['body']);

    if (empty($results->results) || $results->page->count === 0) {
        return false;
    }

    return $results->results[0];
}