(function(w, d, $) {

    var fullStateList = {
      "AL": "Alabama",
      "AK": "Alaska",
      "AS": "American Samoa",
      "AZ": "Arizona",
      "AR": "Arkansas",
      "CA": "California",
      "CO": "Colorado",
      "CT": "Connecticut",
      "DE": "Delaware",
      "DC": "District Of Columbia",
      "FM": "Federated States Of Micronesia",
      "FL": "Florida",
      "GA": "Georgia",
      "GU": "Guam",
      "HI": "Hawaii",
      "ID": "Idaho",
      "IL": "Illinois",
      "IN": "Indiana",
      "IA": "Iowa",
      "KS": "Kansas",
      "KY": "Kentucky",
      "LA": "Louisiana",
      "ME": "Maine",
      "MH": "Marshall Islands",
      "MD": "Maryland",
      "MA": "Massachusetts",
      "MI": "Michigan",
      "MN": "Minnesota",
      "MS": "Mississippi",
      "MO": "Missouri",
      "MT": "Montana",
      "NE": "Nebraska",
      "NV": "Nevada",
      "NH": "New Hampshire",
      "NJ": "New Jersey",
      "NM": "New Mexico",
      "NY": "New York",
      "NC": "North Carolina",
      "ND": "North Dakota",
      "MP": "Northern Mariana Islands",
      "OH": "Ohio",
      "OK": "Oklahoma",
      "OR": "Oregon",
      "PW": "Palau",
      "PA": "Pennsylvania",
      "PR": "Puerto Rico",
      "RI": "Rhode Island",
      "SC": "South Carolina",
      "SD": "South Dakota",
      "TN": "Tennessee",
      "TX": "Texas",
      "UT": "Utah",
      "VT": "Vermont",
      "VI": "Virgin Islands",
      "VA": "Virginia",
      "WA": "Washington",
      "WV": "West Virginia",
      "WI": "Wisconsin",
      "WY": "Wyoming"
    };

    var winner_names_to_party = {
        "Hillary Clinton": "D",
        "Ted Cruz": "R",
        "John Kasich": "R",
        "Donald Trump": "R"
    };

    function getFullStateName(abbr) {
      return fullStateList[abbr];
    }

    function getMapData(stateData, matchup) {
      var mapData = [];
      mapData = loadMapDataIntoArray(stateData, matchup);
      loadMatchupMap(mapData, matchup);
    }

    var loadMapDataIntoArray = function(candidateMapData, matchup) {

      var arr = [],
      mapID = candidateMapData['table_ids'][matchup];
      var mcd = candidateMapData[mapID],
      colors = {
        colorD : candidateMapData['colors'][mapID][0]['color'],
        colorR : candidateMapData['colors'][mapID][1]['color']
      };


      function computeColor(name, colors) {
        if (name === "Hillary Clinton") {
          return colors.colorD;
        }
        else return colors.colorR;
      }

      for(var i = 1; i < mcd.length; i++) {
        arr[i-1] = {
          "hc-key" : "us-"+mcd[i][0].toLowerCase(),
          demPct: mcd[i][1],
          repPct : mcd[i][2],
          demName: mcd[0][1],
          repName: mcd[0][2],
          undecided: mcd[i][3],
          moe: mcd[i][4],
          withinMoe: mcd[i][5],
          value : mcd[i][6].split('%')[0],
          stateName : mcd[i][0],
          fullStateName : getFullStateName(mcd[i][0]),
          winnerName : mcd[i][7],
          winnerIndex : mcd[i][8],
          color: computeColor(mcd[i][7], colors),
          maybeAsterisk: ( mcd[i][5] === "TRUE" ) ? '*' : ''
        }

        if ( arr[i-1]['withinMoe'] === "TRUE" ) {
          arr[i-1]['color'] = adjustHexColor(arr[i-1]['color'], 50);
        }
      }
      arr.colors = colors;
      return arr;
    }

    var loadMatchupMap = function(matchupMapData, matchup) {
      var colorCodeD = matchup.split('_')[0];
      var colorCodeR = matchup.split('_')[1];

      $("map[data-map-matchup='"+matchup+"']").highcharts('Map', {
        title : {
          text: ''
        },
        series: [{
          data: matchupMapData,
          mapData: Highcharts.maps['countries/us/us-all'],
          joinBy: 'hc-key',
          tooltip: {
            useHTML : true,
            headerFormat: '',
            pointFormat: '<span><strong>{point.fullStateName}{point.maybeAsterisk}</strong></span><br/><span>{point.demName}: {point.demPct}</span><br/><span>{point.repName}: {point.repPct}</span><br/><span>Undecided/No Opinion: {point.undecided}</span>'
          }
        }],
        mapNavigation: {
          enabled: false,
        },
        colorAxis: {
          dataClassColor : 'category',
          dataClasses: [{
              color: matchupMapData.colors.colorD,
              name: colorCodeD
          },
          {
              color: matchupMapData.colors.colorR,
              name: colorCodeR
          },
          {
              color: adjustHexColor(matchupMapData.colors.colorD, 50),
              name: colorCodeD + ' (within margin of error)'
          },
          {
              color: adjustHexColor(matchupMapData.colors.colorR, 50),
              name: colorCodeR + ' (within margin of error)'
          }]
        },

      })
      w.matchupMapLoaded[matchup] = true;
    }

    function setupClickHandlers() {
      $(d).on('shown.bs.tab', function(e) {
        var matchup = $(e.target).attr('data-map-matchup');
        if(typeof matchup !== 'undefined') {
          matchup = matchup.split(' vs. ');
          matchup = matchup.join('_');

          if( !w.matchupMapLoaded[matchup] &&
            (typeof mc_fifty_states_matchup_data !== 'undefined' && typeof mc_fifty_states_electoral_votes !== 'undefined') ) {
            getMapData(mc_fifty_states_matchup_data, matchup);

            var target_table_id = parseInt( $(e.target).attr('data-matchup-table-id') );
            mc_fifty_states_update_electoral_count( target_table_id );
            setupBarColors( target_table_id );
          }
        }
      })

      $('.map-tabs a[data-toggle="tab"]').first().trigger('shown.bs.tab');
    }

    function setupBarColors( table_id ) {
        var bar = $('#electoral-votes-bar-' + table_id);
        var totalsDiv = $('#electoral-votes-totals-' + table_id);
        var colors = mc_fifty_states_matchup_data['colors'][table_id];
        var color;
        var color_index;
        var party;

        for (color_index in colors) {
            party = winner_names_to_party[colors[color_index]['column_name']];
            color = colors[color_index]['color'];
            bar.find(('.votes-' + party).toLowerCase()).css('background-color', color);
            totalsDiv.find(('.totals-' + party).toLowerCase()).css('color', color);

        }
    }

    function mc_fifty_states_update_electoral_count( table_id ) {
        var bar = $('#electoral-votes-bar-' + table_id);
        var totalsDiv = $('#electoral-votes-totals-' + table_id);
        var winner_name_column = mc_fifty_states_matchup_data[table_id][0].indexOf("Winner Name");
        var totals = {"D": 0, "R": 0};

        if ( !bar.length ) {
            return;
        }

        $.each(mc_fifty_states_matchup_data[table_id], function(index, row) {
            if (index === 0) {
                return;
            }
            totals[winner_names_to_party[row[winner_name_column]]] += parseInt(mc_fifty_states_electoral_votes[row[0]]);
        });


        $.each(totals, function(index, val) {
            var width = Math.round((val / 538) * 100) ;
            bar.find(('.votes-' + index).toLowerCase()).css('width', width.toString() + '%');
            totalsDiv.find(('.totals-' + index).toLowerCase()).html(totals[index]);


        });
    }

    function init() {
      w.matchupMapLoaded = {};
      setupClickHandlers();
    }

    $(d).ready(function() {
      init();
    });

})(window, document, jQuery);
