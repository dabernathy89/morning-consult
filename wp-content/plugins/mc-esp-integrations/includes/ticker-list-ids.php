<?php
return array(
	"Test_for_JB" => array(
		"listID" => 769,
		"senderProfile" => 999,
		"sendClassification" => 999
	),
	"Campaigns" => array(
		"listID" => 675,
		"senderProfile" => 224,
		"sendClassification" => 180
	),
	"Congress" => array(
		"listID" => 670,
		"senderProfile" => 223,
		"sendClassification" => 179
	),
	"Washington" => array(
		"listID" => 1464,
		"senderProfile" => 696,
		"sendClassification" => 411
	),
	"Energy" => array(
		"listID" => 666,
		"senderProfile" => 220,
		"sendClassification" => 176
	),
	"Finance" => array(
		"listID" => 672,
		"senderProfile" => 221,
		"sendClassification" => 177
	),
	"Health" => array(
		"listID" => 665,
		"senderProfile" => 219,
		"sendClassification" => 175
	),
	"Tech" => array(
		"listID" => 673,
		"senderProfile" => 222,
		"sendClassification" => 178
	)

);

?>
