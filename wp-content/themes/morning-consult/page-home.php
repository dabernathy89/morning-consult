<?php
/**
 * Template name: Home
 *
 * The home page template.
 *
 * @package Morning Consult 2015
 */
?>

<?php get_header(); ?>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-7 col-lg-8 stories">

                <?php include(locate_template( 'inc/partials/home/feature.php' )); ?>

                <?php include(locate_template( 'inc/partials/home/stories.php' )); ?>


            </div>
            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4 alerts-wrap">
                <?php get_template_part( 'inc/partials/alerts' ); ?>
            </div>
        </div>
    </div>

<?php get_footer(); ?>
