<?php
/**
 * Template name: Section Home
 *
 * The template for displaying a section home page (health, energy, finance, tech, congress, campaigns)
 *
 * @package Morning Consult 2015
 */
?>

<?php get_header(); ?>

    <?php
      $section_cat = get_field('section_home_category');
      if($section_cat->slug !== 'polling') :
    ?>

    <div class="container section-home">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-7 col-lg-8 stories">
                <?php include(locate_template( 'inc/partials/section/featured.php' )); ?>
                <?php include(locate_template( 'inc/partials/section/stories.php' )); ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4 alerts-wrap">
                <?php include(locate_template( 'inc/partials/alerts.php' )); ?>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <?php if($section_cat->slug === 'polling'): ?>
      <div class="container section-home">
        <div class="row">
          <div class="col-xs-12 stories">
            <?php include(locate_template( 'inc/partials/section/featured.php' )); ?>
            <?php include(locate_template( 'inc/partials/section/stories.php' )); ?>
          </div>
        </div>
      </div>
    <?php endif; ?>

<?php get_footer(); ?>
