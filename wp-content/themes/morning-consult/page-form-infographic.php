<?php /*
* Template Name: Form Infographic 2 Col
*/
?>

<?php get_header(); ?>

<?php while(have_posts()) : the_post(); ?>

<container class="container">
	<row class="row">
		<header class="page-title col-xs-12">
			<h1><?php the_field('page_title') ?></h1>
		</header>
	</row>

	<row class="row">
		<main class="main-content col-xs-12 col-md-7">
			<row class="row">
				<div class="main-paragraph col-xs-12">
					<?php the_field('main_content'); ?>
				</div>
			</row>

			<row class="row">
				<div class="form-wrapper col-xs-12">
					<?php $shortcode = get_field('form_shortcode');
						echo do_shortcode( $shortcode ); 
					?>
				</div>
			</row>

		</main>
		<div class="sidebar-content col-xs-12 col-md-5">
		<?php $image = get_field('sidebar_image'); ?>
		<?php $image_link = get_field('sidebar_image_clickthrough'); ?>
			<a href="<?php echo $image_link ?>" target="_blank">
				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
			</a>
		</div>
	</row>
</container>

<?php endwhile; ?>

<?php wp_footer(); ?>
</body>
</html>