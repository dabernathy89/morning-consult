<?php

// Register Custom Post Type
function morning_consult_post_types() {

    /**
     * Brief
     */
    $brief_labels = array(
        'name'                => 'Briefs',
        'singular_name'       => 'Brief',
        'menu_name'           => 'Briefs',
        'parent_item_colon'   => 'Parent Brief:',
        'all_items'           => 'All Briefs',
        'view_item'           => 'View Brief',
        'add_new_item'        => 'Add New Brief',
        'add_new'             => 'Add New',
        'edit_item'           => 'Edit Brief',
        'update_item'         => 'Update Brief',
        'search_items'        => 'Search Briefs',
        'not_found'           => 'Not found',
        'not_found_in_trash'  => 'Not found in Trash',
    );

    $brief_args = array(
        'labels'              => $brief_labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'custom-fields', ),
        'taxonomies'          => array( 'category', 'post_tag' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => 'briefs',
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'rewrite'             => array ('slug' => 'briefs')
    );

    register_post_type( 'mc_brief', $brief_args );

    /**
     * Polling
     */

    $poll_labels = array(
        'name'                => 'Polls',
        'singular_name'       => 'Poll',
        'menu_name'           => 'Polls',
        'parent_item_colon'   => 'Parent Poll:',
        'all_items'           => 'All Polls',
        'view_item'           => 'View Poll',
        'add_new_item'        => 'Add New Poll',
        'add_new'             => 'Add New',
        'edit_item'           => 'Edit Poll',
        'update_item'         => 'Update Poll',
        'search_items'        => 'Search Polls',
        'not_found'           => 'Not found',
        'not_found_in_trash'  => 'Not found in Trash',
    );

    $poll_args = array(
        'labels'              => $poll_labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'custom-fields', ),
        'taxonomies'          => array( 'category', 'post_tag' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => 'polls',
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'rewrite'             => array ('slug' => 'polls')
    );

    register_post_type( 'mc_poll', $poll_args );

    /**
     * Opinion
     */

    $opinion_labels = array(
        'name'                => 'Opinions',
        'singular_name'       => 'Opinion',
        'menu_name'           => 'Opinions',
        'parent_item_colon'   => 'Parent Opinion:',
        'all_items'           => 'All Opinions',
        'view_item'           => 'View Opinion',
        'add_new_item'        => 'Add New Opinion',
        'add_new'             => 'Add New',
        'edit_item'           => 'Edit Opinion',
        'update_item'         => 'Update Opinion',
        'search_items'        => 'Search Opinions',
        'not_found'           => 'Not found',
        'not_found_in_trash'  => 'Not found in Trash',
    );

    $opinion_args = array(
        'labels'              => $opinion_labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'custom-fields', ),
        'taxonomies'          => array( 'category', 'post_tag' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => 'opinions',
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'rewrite'             => array ('slug' => 'opinions')
    );

    register_post_type( 'mc_opinion', $opinion_args );

    $race_labels = array(
        'name'                => 'Races',
        'singular_name'       => 'Race',
        'menu_name'           => 'Races',
        'name_admin_bar'      => 'Races',
        'parent_item_colon'   => 'Parent Race:',
        'all_items'           => 'All Races',
        'add_new_item'        => 'Add New Race',
        'add_new'             => 'Add New',
        'new_item'            => 'New Race',
        'edit_item'           => 'Edit Race',
        'update_item'         => 'Update Race',
        'view_item'           => 'View Race',
        'search_items'        => 'Search Race',
        'not_found'           => 'Not found',
        'not_found_in_trash'  => 'Not found in Trash',
    );
    $race_rewrite = array(
        'slug'                => 'race',
        'with_front'          => true,
        'pages'               => true,
        'feeds'               => true,
    );
    $race_args = array(
        'label'               => 'Race',
        'labels'              => $race_labels,
        'supports'            => array( 'title', 'editor', 'thumbnail', ),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => 'races',
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'rewrite'             => $race_rewrite,
        'capability_type'     => 'page',
    );
    register_post_type( 'mc_race', $race_args );

    $race_cat_labels = array(
        'name'                       => 'Race Categories',
        'singular_name'              => 'Race Category',
        'menu_name'                  => 'Race Category',
        'all_items'                  => 'All Race Categories',
        'parent_item'                => 'Parent Race Category',
        'parent_item_colon'          => 'Parent Race Category:',
        'new_item_name'              => 'New Race Category Name',
        'add_new_item'               => 'Add New Race Category',
        'edit_item'                  => 'Edit Race Category',
        'update_item'                => 'Update Race Category',
        'view_item'                  => 'View Race Category',
        'separate_items_with_commas' => 'Separate items with commas',
        'add_or_remove_items'        => 'Add or remove items',
        'choose_from_most_used'      => 'Choose from the most used',
        'popular_items'              => 'Popular Race Categories',
        'search_items'               => 'Search Race Categories',
        'not_found'                  => 'Not Found',
    );
    $race_cat_rewrite = array(
        'slug'                       => 'race-categories',
        'with_front'                 => true,
        'hierarchical'               => false,
    );
    $race_cat_args = array(
        'labels'                     => $race_cat_labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'rewrite'                    => $race_cat_rewrite,
    );
    register_taxonomy( 'mc_race_category', array( 'mc_race' ), $race_cat_args );

    $rankings_table_labels = array(
        'name'                => 'Rankings Table',
        'singular_name'       => 'Rankings Table',
        'menu_name'           => 'Rankings Table',
        'name_admin_bar'      => 'Rankings Table',
        'parent_item_colon'   => 'Parent Rankings Table:',
        'all_items'           => 'All Rankings Tables',
        'add_new_item'        => 'Add New Rankings Table',
        'add_new'             => 'Add New',
        'new_item'            => 'New Rankings Table',
        'edit_item'           => 'Edit Rankings Table',
        'update_item'         => 'Update Rankings Table',
        'view_item'           => 'View Rankings Table',
        'search_items'        => 'Search Rankings Table',
        'not_found'           => 'Not found',
        'not_found_in_trash'  => 'Not found in Trash',
    );
    $rankings_table_rewrite = array(
        'slug'                => 'rankings-table',
        'with_front'          => true,
        'pages'               => true,
        'feeds'               => true,
    );
    $rankings_table_args = array(
        'label'               => 'Rankings Table',
        'labels'              => $rankings_table_labels,
        'supports'            => array( 'title', 'editor', 'thumbnail', ),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => 'rankings-tables',
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'rewrite'             => $rankings_table_rewrite,
        'capability_type'     => 'page',
    );
    register_post_type( 'mc_rankings_table', $rankings_table_args );

}

// Hook into the 'init' action
add_action( 'init', 'morning_consult_post_types', 0 );

function mc_alert_post_type() {

    $labels = array(
        'name'                  => 'Alerts',
        'singular_name'         => 'Alert',
        'menu_name'             => 'Alerts',
        'name_admin_bar'        => 'Alerts',
        'parent_item_colon'     => 'Parent Alert:',
        'all_items'             => 'All Alerts',
        'add_new_item'          => 'Add New Alert',
        'add_new'               => 'Add New',
        'new_item'              => 'New Alert',
        'edit_item'             => 'Edit Alert',
        'update_item'           => 'Update Alert',
        'view_item'             => 'View Alert',
        'search_items'          => 'Search Alert',
        'not_found'             => 'Not found',
        'not_found_in_trash'    => 'Not found in Trash',
        'items_list'            => 'Alerts list',
        'items_list_navigation' => 'Alerts list navigation',
        'filter_items_list'     => 'Filter items list',
    );
    $rewrite = array(
        'slug'                  => 'alert',
        'with_front'            => true,
        'pages'                 => true,
        'feeds'                 => true,
    );
    $args = array(
        'label'                 => 'Alert',
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
        'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => 'alerts',
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'               => $rewrite,
        'capability_type'       => 'page',
    );
    register_post_type( 'mc_alert', $args );

}
add_action( 'init', 'mc_alert_post_type', 0 );

function mc_daily_alerts_email_post_type() {

    $labels = array(
        'name'                  => 'Daily Alerts Emails',
        'singular_name'         => 'Daily Alerts Email',
        'menu_name'             => 'Daily Alerts Emails',
        'name_admin_bar'        => 'Daily Alerts Emails',
        'archives'              => 'Daily Alerts Email Archives',
        'parent_item_colon'     => 'Parent Daily Alerts Email:',
        'all_items'             => 'All Daily Alerts Emails',
        'add_new_item'          => 'Add New Daily Alerts Email',
        'add_new'               => 'Add New',
        'new_item'              => 'New Daily Alerts Email',
        'edit_item'             => 'Edit Daily Alerts Email',
        'update_item'           => 'Update Daily Alerts Email',
        'view_item'             => 'View Daily Alerts Email',
        'search_items'          => 'Search Daily Alerts Email',
        'not_found'             => 'Not found',
        'not_found_in_trash'    => 'Not found in Trash',
        'featured_image'        => 'Featured Image',
        'set_featured_image'    => 'Set featured image',
        'remove_featured_image' => 'Remove featured image',
        'use_featured_image'    => 'Use as featured image',
        'insert_into_item'      => 'Insert into Daily Alerts Email',
        'uploaded_to_this_item' => 'Uploaded to this Daily Alerts Email',
        'items_list'            => 'Daily Alerts Emails list',
        'items_list_navigation' => 'Daily Alerts Emails list navigation',
        'filter_items_list'     => 'Filter Daily Alerts Email list',
    );
    $args = array(
        'label'                 => 'Daily Alerts Email',
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'author', 'custom-fields', ),
        'hierarchical'          => false,
        'taxonomies'            => array( 'category' ),
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'mc_daily_alerts', $args );

}
add_action( 'init', 'mc_daily_alerts_email_post_type', 0 );

function mc_template_email_post_type() {
	$labels = array(
		'name'                  => 'Template Emails',
		'singular_name'         => 'Template Email',
		'menu_name'             => 'Template Emails',
		'name_admin_bar'        => 'Template Emails',
		'archives'              => 'Template Email Archives',
		'parent_item_colon'     => 'Parent Template Email:',
		'all_items'             => 'All Template Emails',
		'add_new_item'          => 'Add New Template Email',
		'add_new'               => 'Add New',
		'new_item'              => 'New Template Email',
		'edit_item'             => 'Edit Template Email',
		'update_item'           => 'Update Template Email',
		'view_item'             => 'View Template Email',
		'search_items'          => 'Search Template Email',
		'not_found'             => 'Not found',
		'not_found_in_trash'    => 'Not found in Trash',
		'featured_image'        => 'Featured Image',
		'set_featured_image'    => 'Set featured image',
		'remove_featured_image' => 'Remove featured image',
		'use_featured_image'    => 'Use as featured image',
		'insert_into_item'      => 'Insert into Template Email',
		'uploaded_to_this_item' => 'Uploaded to this Template Email',
		'items_list'            => 'Template Emails list',
		'items_list_navigation' => 'Template Emails list navigation',
		'filter_items_list'     => 'Filter Template Email list'
	);

	$args = array(
		'label'                 => 'Template Email',
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'custom-fields', ),
		'hierarchical'          => false,
		'taxonomies'            => array( 'category' ),
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => false,
		'capability_type'       => 'page'
	);

	register_post_type('template_email', $args);
}
add_action('init', 'mc_template_email_post_type', 0);
