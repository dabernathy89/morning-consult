<?php
/**
 * The template for displaying the footer.
 *
 * @package Morning Consult 2015
 */
?>

<footer id="footer-main" role="contentinfo">
  <?php if(is_active_sidebar('footer-main')) : ?>
    <div class="container">
      <?php dynamic_sidebar( 'footer-main' ); ?>

      <ul class="footer-social hidden-sm hidden-xs">
        <?php
          $url = get_permalink();
          $page_title = wp_title('', false);
          $page_title = rawurlencode($page_title);
        ?>
        <a href="https://facebook.com/sharer/sharer.php" target="_blank" aria-label=""><li><i class="fa fa-facebook fa-lg"></i></li></a>
        <a href="https://twitter.com/intent/tweet/?text=Morning%20Consult%Homepage&amp;url=<?php echo $url; ?>" target="_blank" aria-label=""><li><i class="fa fa-twitter fa-lg"></i></li></a>
        <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $url; ?>&amp;title=<?php echo $page_title; ?>&amp;source=<?php echo $url; ?>" target="_blank" aria-label=""><li><i class="fa fa-linkedin fa-lg"></i></li></a>
      </ul>
    </div>
  <?php endif; ?>
</footer><!-- #footer-main -->

<?php wp_reset_query(); ?>
<?php if (is_page('subscribe') === false) : ?>
<div class="modal fade subscribe-modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <?php the_field('subscribe_modal_content','option'); ?>
    </div>
  </div>
</div>
<?php endif; ?>

<?php wp_footer(); ?>


<script type="text/javascript">
  var _sf_async_config = { uid: 62308, domain: 'morningconsult.com', useCanonical: true };
  (function() {
    function loadChartbeat() {
      window._sf_endpt = (new Date()).getTime();
      var e = document.createElement('script');
      e.setAttribute('language', 'javascript');
      e.setAttribute('type', 'text/javascript');
      e.setAttribute('src','//static.chartbeat.com/js/chartbeat.js');
      document.body.appendChild(e);
    };
    var oldonload = window.onload;
    window.onload = (typeof window.onload != 'function') ?
      loadChartbeat : function() { oldonload(); loadChartbeat(); };
  })();
</script>

</body>
</html>
