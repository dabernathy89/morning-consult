(function(w, d, $) {

	function setDefaultOpts(data, mapHeight, title) {

		var defaultOpts = {
			title: {
        text: title,
        y: 30
			},
			chart: {
				className: "map-content",
				height: mapHeight
			},
      series: [{
        data: data,
				name: title,
        mapData: Highcharts.maps['countries/us/us-all'],
        joinBy: "hc-key",
        borderColor: '#CCCCCC',
        borderWidth: 0.5,
        tooltip: {
          headerFormat: '',
					pointFormat: '{point.name}: <strong>{point.value}%</strong><br/>'
        },
        states: {
          hover: {
            color: "#EFEFEF",
          },
          normal: {
            animation: false
          }
        }
      }],
      credits: {
        enabled: false
      },
      legend: {
				enabled: false,
      },
    };

		return defaultOpts;

	}

	function getScreenWidth() {
		if(window.innerWidth < 480) {
			return 300;
		}
		else return 500;

	}

	function tabClickHandlers() {
		$('#map-content-tab').click(function() {
			$('.map-tabs li').first().trigger('shown.bs.tab');
		})
		$('.map-tabs').on('shown.bs.tab', function(e) {
			window.setTimeout(function() {
				loadChart();
			}, 400);

		})

	}


	function loadChart() {
		var mapData = w.mc_presidential_candidate_table_data;
		var mapHeight = getScreenWidth();

		var clintonFavData = setDefaultOpts(mapData.clinton_fav, mapHeight, 'Clinton Favorability');
		var clintonUnfavData = setDefaultOpts(mapData.clinton_unfav, mapHeight, 'Clinton Unfavorability');
		var trumpFavData = setDefaultOpts(mapData.trump_fav, mapHeight, 'Trump Favorability');
		var trumpUnfavData = setDefaultOpts(mapData.trump_unfav, mapHeight, 'Trump Unfavorability');

		$('#map-content-1').highcharts('Map', clintonFavData);
		$('#map-content-2').highcharts('Map', trumpFavData);
		$('#map-content-3').highcharts('Map', clintonUnfavData);
		$('#map-content-4').highcharts('Map', trumpUnfavData);



	}

  function init() {
		tabClickHandlers();
		// loadChart();
  }

  $(d).ready(function() {
    init();
  });

})(window, document, jQuery);
