<?php
return array(
	'appsignature' => 'none',
	'clientid' => get_field( 'sfmc_client_id', 'options' ),
	'clientsecret' => get_field( 'sfmc_client_secret', 'options' ),
	'defaultwsdl' => 'https://webservice.exacttarget.com/etframework.wsdl',
	'xmlloc' =>  __DIR__ . '/ExactTargetWSDL.xml',
);
