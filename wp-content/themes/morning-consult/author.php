<?php
/**
 * The template for displaying author archives
 *
 * @package Morning Consult 2015
 */

get_header(); ?>

<?php
    global $authordata;
    if (is_object($authordata) && get_class($authordata) === "WP_User") {
        include "inc/partials/author/author_user.php";
    } else if (is_object($authordata) && get_class($authordata) === "stdClass") {
        include "inc/partials/author/author_guest.php";
    }
?>

    <div class="page archive author">

        <div class="container">
            <hr>

            <div class="row">
                <div class="col-xs-12">

                    <?php if ( have_posts() ) : ?>

                        <?php /* Start the Loop */ ?>
                        <?php while ( have_posts() ) : the_post(); ?>

                            <?php get_template_part( 'content', 'archive' ); ?>

                        <?php endwhile; ?>

                        <?php mc_the_posts_navigation(); ?>

                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>
