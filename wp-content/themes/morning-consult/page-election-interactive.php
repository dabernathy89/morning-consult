<?php /*
* Template Name: 2016 Election Interactive
*/ ?>

<?php get_header(); ?>

<?php 
	$tables = json_decode(get_option('tablepress_tables', array()), true);
	$table_id = get_field('tablepress_data_source');
	$tablepress_data = get_post_field('post_content', $tables['table_post'][$table_id], 'raw');
	$formatted_data = mc_election_interactive_format_data($tablepress_data);
	wp_localize_script( 'morning-consult-all-js', 'mc_election_interactive_data', $formatted_data);
	// wp_localize_script( 'morning-consult-all-js', 'mc_election_interactive_data', $tablepress_data);
?>

<?php while(have_posts()) : the_post(); ?>

<script src="<?php echo get_template_directory_uri() . '/js/handlebars/handlebars.min-latest.js'; ?>"></script>
<script src="<?php echo get_template_directory_uri() . '/js/chart.js/Chart.bundle.js'; ?>"></script>
<script src="<?php echo get_template_directory_uri() . '/js/election-interactive-2016.js'; ?>"></script>

<container class="container">
	<row class="row">
		<div class="col-xs-12 page-title"><h1><?php the_field('page_title'); ?></h1></div>
		<!--<div class="col-xs-12 intro-paragraph"><?php the_content(); ?></div>-->
		<main class="main-content col-xs-12">
			<row class="row">
				<div class="col-xs-12">
					<ul class="nav nav-pills nav-justified" role="tablist">
						<li role="presentation" class="active"><a href="#interactive" aria-controls="interactive" role="tab" data-toggle="pill">Interactive</a></li>
						<li role="presentation"><a href="#content1" aria-controls="content 1" role="tab" data-toggle="pill">Content 1</a></li>
						<li role="presentation"><a href="#content2" aria-controls="content 2" role="tab" data-toggle="pill">Content 2</a></li>
					</ul>

					<div class="tab-content">
						<div class="tab-pane fade in active" role="tabpanel" id="interactive"></div>
						<div class="tab-pane fade container-900 page-content" role="tabpanel" id="content1"><?php the_field('content_1'); ?></div>
						<div class="tab-pane fade container-900 page-content" role="tabpanel" id="content2"><?php the_field('content_2'); ?></div>
					</div>
				</div>
			</row>
		</main>

	</row>
</container>

<script id="partial-legend" type="text/x-handlebars-template">
	<div class="legend-container">
		<span class="legend-color blue"></span><span class="legend-text">Hillary Clinton</span>
		<span class="legend-color red"></span><span class="legend-text">Donald Trump</span>
	</div>
</script>

<script id="partial-gender-national" type="text/x-handlebars-template">
	<div id="partial-gender-national-wrapper" class="gender national horizontal noborder">
		<h2>Gender</h2>
		<h3>National</h3>
		{{#each this.genderNational}}
			<div class="horizontal-chart-wrapper">
				<span>{{this.title}}</span>
				<canvas id="gender-national-{{@index}}" class="horizontal-chart"></canvas>
			</div>
		{{/each}}
	</div>
</script>

<script id="partial-gender-state" type="text/x-handlebars-template">
	<div id="partial-gender-state-wrapper" class="clearfix state">
		{{#each this.genderState}}
			<div class="vertical-chart col-xs-12 col-sm-6 col-md-3">
				<canvas id="gender-state-{{@index}}"></canvas>
			</div>
		{{/each}}
	</div>
</script>

<script id="partial-party-national" type="text/x-handlebars-template">
	<div id="partial-party-national-wrapper" class="race national horizontal">
		<h2>Party</h2>
		<h3>National</h3>
		{{#each this.partyNational}}
		<div class="horizontal-chart-wrapper">
			<span>{{this.title}}</span>
			<canvas id="party-national-{{@index}}" class="horizontal-chart"></canvas>
		</div>
		{{/each}}
	</div>
</script>

<script id="partial-party-state" type="text/x-handlebars-template">
	<div id="partial-party-state-wrapper" class="clearfix state">
		{{#each this.partyState}}
			<div class="vertical-chart col-xs-12 col-sm-6 col-md-3">
				<canvas id="party-state-{{@index}}"></canvas>
			</div>
		{{/each}}
	</div>
</script>

<script id="partial-age-national" type="text/x-handlebars-template">
	<div id="partial-age-national-wrapper" class="age national horizontal">
		<h2>Age</h2>
		<h3>National</h3>
		{{#each this.ageNational}}
			<div class="horizontal-chart-wrapper">
				<span>{{removeAge this.title}}</span>
				<canvas id="age-national-{{@index}}" class="horizontal-chart"></canvas>
			</div>
		{{/each}}
	</div>
</script>

<script id="partial-age-state" type="text/x-handlebars-template">
	<div id="partial-age-state-wrapper" class="clearfix state">
		{{#each this.ageState}}
			<div class="vertical-chart col-xs-12 col-sm-6 col-md-4">
				<canvas id="age-state-{{@index}}"></canvas>
			</div>
		{{/each}}
	</div>
</script>

<script id="interactive-template" type="text/x-handlebars-template">
	{{> partial-legend}}
	{{> partial-gender-national}}
	{{> partial-gender-state}}
	{{> partial-party-national}}
	{{> partial-party-state}}
	{{> partial-age-national}}
	{{> partial-age-state}}
</script>


<?php endwhile; ?>

<?php get_footer(); ?>

</body>
</html>