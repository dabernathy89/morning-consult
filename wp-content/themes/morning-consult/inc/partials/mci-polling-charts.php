<?php
/**
 * Content part for "morning consult Polling" page.
 *
 * @package Morning Consult 2015
 */
?>

<section class="mci-charts">
  <div class="logo-container">
    <p>The Latest from <img src="<?php echo get_template_directory_uri() . '/img/logo_square_dark.svg' ?>" alt="logo" class="mc-logo" />
      <span>Intelligence</span>
    </p>
  </div>
  <?php $charts = mc_build_polling_page_iframe(6);
  foreach($charts as $chart) {
    $url = new DOMDocument();
    $url->preserveWhiteSpace = false;
    $url->loadHTML($chart['html']);
    $chart_title = $url->getElementsByTagName('h4')->item(0);
    $chart_title_str = $chart_title->nodeValue;
    $chart_title_str = substr($chart_title_str, 0, 90) . '...';
    $chart_button = $chart['button'];
    $chart_img = $chart['img'];

      $html = '<div class="mci-chart col-xs-12 col-sm-6 col-md-4">'.
      '<p class="mci-chart-title">'.$chart_title_str.'</p>'.
      '<img src="'.$chart['img'].'"/>'.
      '<a href="'.$chart['button'].'" target="_blank"><button>Analyze</button></a>'.
      '</div>';
      print_r($html);


  }
  ?>

</section>
