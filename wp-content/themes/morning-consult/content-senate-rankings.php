<?php $tablepress_tables = json_decode( get_option( 'tablepress_tables', array() ), true );
$table_id = get_field('table_data');
$table_data = json_decode(get_post_field('post_content', $tablepress_tables['table_post'][$table_id], 'raw'));
// print_r($table_data);
// var_dump($table_data);
$json_data = array();

foreach($table_data as $key => $val) {
  if($key === 0) {
    $headings = array(
      'State Name',
      'Approve',
      'Disapprove',
      'Undecided',
      'MOE',
      'Senator',
      'Party',
      'Changed'
    );
    $json_data[$key] = $headings;

  }
  if($key > 0) {
    $json_data[$key] = $val;
  }
}
//loaded from extras.php
$json_data['alltop'] = mc_senate_rankings_top10($table_data);

wp_localize_script('morning-consult-all-js', 'mc_senate_ranking_data', $json_data);
?>

<section class="senate-best-worst col-xs-12">
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
      <a href="#top10" aria-controls="top10" role="tab" id="tab-top10" data-toggle="tab">Top 10</a>
    </li>
    <li role="presentation">
      <a href="#bottom10" aria-controls="bottom10" role="tab" id="tab-bottom10" data-toggle="tab">Bottom 10</a>
    </li>
    <li role="presentation">
      <a href="#most-unknown" aria-controls="most-unknown" role="tab" id="tab-unknown" data-toggle="tab">Top 10 Unknown</a>
    </li>
  </ul>

  <div class="tab-content">
    <div id="top10" role="tabpanel" class="tab-pane fade in active senate-top-table"></div>
    <div id="bottom10" role="tabpanel" class="tab-pane fade senate-top-table"></div>
    <div id="most-unknown" role="tabpanel" class="tab-pane fade senate-top-table"></div>
  </div>
</section>

<section class="senate-middle-paragraph col-xs-12 col-md-10 col-md-offset-1">
  <?php the_field('middle_paragraph'); ?>
</section>

<section class="senate-table-data col-xs-12">
    <?php tablepress_print_table( array(
      'id' => $table_id,
      'use_datatables' => true,
      'print_name' => false
    )); ?>
</section>

<!-- handlebars template for senator table -->
<script id="hb-top10-template" type="text/x-handlebars-template">

  <div id="table-legend">
    <div class="col-xs-12 col-sm-4">
      <div class="legend-approve legend-color"></div>
      <span>Approve</span>
    </div>
    <div class="col-xs-12 col-sm-4">
      <div class="legend-disapprove legend-color"></div>
      <span>Disapprove</span>
    </div>
    <div class="col-xs-12 col-sm-4">
      <div class="legend-unknown legend-color"></div>
      <span>Don't Know/No Opinion</span>
    </div>
  </div>

  <table id="hb-top10 col-xs-12">
    <tr>
      <th>Rank</th>
      <th class="hb-table-senator">Senator</th>
    </tr>
    {{#each this}}
    <tr>
      <td class="hb-table-rank">{{indexPlusOne @index}}</td>
      <td class="hb-table-img">
        <div class="hb-table-img-wrapper col-xs-4 col-sm-4 col-md-2">
          <a href="{{getMCILink name}}" target="_blank"><img class="senate-headshot-img" src="{{photo}}" alt="" /></a>
        </div>
        <div class="hb-table-img-span col-xs-8 col-sm-8 col-md-4">
          <a href="{{getMCILink name}}" target="_blank"><p>{{nickname name}} <span>({{state}} - {{trimPartyName party}})<span></p></a>
        </div>
        <div class="hb-table-img-labels col-xs-2 col-sm-2 col-md-1">
          <span>{{approval}}%</span>
          <span>{{disapproval}}%</span>
          <span>{{unknown}}%</span>
        </div>
        <div class="hb-table-rating-wrapper col-xs-10 col-sm-10 col-md-5">
          <bar class="hb-table-rating-approval" style="width: {{approval}}%;"></bar>
          <bar class="hb-table-rating-disapproval" style="width: {{disapproval}}%;"></bar>
          <bar class="hb-table-rating-unknown" style="width: {{unknown}}%;"></bar>
        </div>
      </td>
    </tr>
    {{/each}}
  </table>
</script>
