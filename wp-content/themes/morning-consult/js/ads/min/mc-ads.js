(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _loader = require('./loader.js');

var _loader2 = _interopRequireDefault(_loader);

var _mcAds = require('./mc-ads.js');

var _mcAds2 = _interopRequireDefault(_mcAds);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

window.$ = window.jQuery;

//fire ad only when gpt API loaded
// runs initial setup
window.addEventListener('MCAdsReady', function () {
	initAllAdsOnPage();
});

function initAllAdsOnPage() {
	var initialAdsToLoad = _mcAds2.default.findAdsOnPage();
	var updatedAds = _mcAds2.default.defineAdSlots(initialAdsToLoad);

	_mcAds2.default.placeRegularAds(updatedAds.regularAds);
	_mcAds2.default.setupLazyAds(updatedAds.lazyLoadingAds);
}

},{"./loader.js":3,"./mc-ads.js":4}],2:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
var adSizes = {
	/* uncomment these if we need to drop jumpyield support
 "morningconsult_1" : {
 	sizes: [[160, 600], [300, 250], [300, 600]],
 	script: "<!-- /135698611/morningconsult_1 --><div id='div-gpt-ad-1467116217077-0'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1467116217077-0'); });</script></div>",
 	name: '/135698611/morningconsult_1',
 	adId: 'div-gpt-ad-1467116217077-0'
 },
 "morningconsult_2" : {
 	sizes: [[300, 600], [300, 250]],
 	script: "<!-- /135698611/morningconsult_2 --><div id='div-gpt-ad-1467116217077-1'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1467116217077-1'); });</script></div>",
 	name: '/135698611/morningconsult_2',
 	adId: 'div-gpt-ad-1467116217077-1'
 },
 "morningconsult_3" : {
 	sizes: [300, 250],
 	script: "<!-- /135698611/morningconsult_3 --><div id='div-gpt-ad-1467116217077-2' style='height:250px; width:300px;'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1467116217077-2'); });</script></div>",
 	name: '/135698611/morningconsult_3',
 	adId: 'div-gpt-ad-1467116217077-2'
 },
 "morningconsult_4" : {
 	sizes: [300, 250],
 	script: "<!-- /135698611/morningconsult_4 --><div id='div-gpt-ad-1467116217077-3' style='height:250px; width:300px;'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1467116217077-3'); });</script></div>",
 	name: '/135698611/morningconsult_4',
 	adId: 'div-gpt-ad-1467116217077-3'
 },
 "morningconsult_5" : {
 	sizes: [300, 250],
 	script: "<!-- /135698611/morningconsult_5 --><div id='div-gpt-ad-1467116217077-4' style='height:250px; width:300px;'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1467116217077-4'); });</script></div>",
 	name: '/135698611/morningconsult_5',
 	adId: 'div-gpt-ad-1467116217077-4'
 },
 */

	"jumpyield_300_600_Floor1": {
		sizes: [300, 600],
		script: "<!-- /101659253/morningconsult_newsticker_300x600_Floor1 --><div id='div-gpt-ad-8398195469801-0' style='height:600px; width:300px;'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-8398195469801-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_300x600_Floor1',
		adId: 'div-gpt-ad-8398195469801-0'
	},
	"jumpyield_300_600_Control": {
		sizes: [300, 600],
		script: "<!-- /101659253/morningconsult_newsticker_300x600_Control --><div id='div-gpt-ad-6639247135062-0' style='height:600px; width:300px;'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-6639247135062-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_300x600_Control',
		adId: 'div-gpt-ad-6639247135062-0',
		pct: 0.95,
		fallback: "jumpyield_300_600_Floor1"
	},
	"morningconsult_newsticker_s2_Control": {
		sizes: [[300, 600], [300, 250]],
		script: "<!-- /101659253/morningconsult_newsticker_s2_300x600_Control --><div id='div-gpt-ad-9911945492688-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-9911945492688-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_s2_300x600_Control',
		adId: 'div-gpt-ad-9911945492688-0',
		pct: 0.95,
		fallback: 'morningconsult_newsticker_s2_Floor1'
	},
	"morningconsult_newsticker_s2_Floor1": {
		sizes: [300, 600],
		script: "<!-- /101659253/morningconsult_newsticker_s2_300x600_Floor1 --><div id='div-gpt-ad-5579869292223-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-5579869292223-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_s2_300x600_Floor1',
		adId: 'div-gpt-ad-5579869292223-0'
	},
	"morningconsult_newsticker_s3_Control": {
		sizes: [300, 250],
		script: "<!-- /101659253/morningconsult_newsticker_s3_300x250_Control --><div id='div-gpt-ad-5849151333972-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-5849151333972-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_s3_300x250_Control',
		adId: 'div-gpt-ad-5849151333972-0',
		pct: 0.95,
		fallback: 'morningconsult_newsticker_s3_Floor1'
	},
	"morningconsult_newsticker_s3_Floor1": {
		sizes: [300, 250],
		script: "<!-- /101659253/morningconsult_newsticker_s3_300x250_Floor1 --><div id='div-gpt-ad-5291563273390-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-5291563273390-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_s3_300x250_Floor1',
		adId: 'div-gpt-ad-5291563273390-0'
	},
	"morningconsult_newsticker_s4_Control": {
		sizes: [300, 250],
		script: "<!-- /101659253/morningconsult_newsticker_s4_300x250_Control --><div id='div-gpt-ad-4583788211015-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-4583788211015-0'); });</script></div>",
		name: "/101659253/morningconsult_newsticker_s4_300x250_Control",
		adId: "div-gpt-ad-4583788211015-0",
		pct: 0.95,
		fallback: 'morningconsult_newsticker_s4_Floor1'
	},
	"morningconsult_newsticker_s4_Floor1": {
		sizes: [300, 250],
		script: "<!-- /101659253/morningconsult_newsticker_s4_300x250_Floor1 --><div id='div-gpt-ad-1546157942493-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1546157942493-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_s4_300x250_Floor1',
		adId: 'div-gpt-ad-1546157942493-0'
	},
	"morningconsult_newsticker_s5_Control": {
		sizes: [300, 250],
		script: "<!-- /101659253/morningconsult_newsticker_s5_300x250_Control --><div id='div-gpt-ad-5945178775929-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-5945178775929-0'); });</script></div>",
		name: "/101659253/morningconsult_newsticker_s5_300x250_Control",
		adId: "div-gpt-ad-5945178775929-0",
		pct: 0.95,
		fallback: 'morningconsult_newsticker_s5_Floor1'
	},
	"morningconsult_newsticker_s5_Floor1": {
		sizes: [300, 250],
		script: "<!-- /101659253/morningconsult_newsticker_s5_300x250_Floor1 --><div id='div-gpt-ad-6919211697350-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-6919211697350-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_s5_300x250_Floor1',
		adId: 'div-gpt-ad-6919211697350-0'
	}
};

exports.default = adSizes;

},{}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
// wait for gpt to be ready, then call init functions
var gptAdLoader = window.setInterval(function () {

	if (window.googletag && window.googletag.apiReady) {
		(function () {
			window.clearInterval(gptAdLoader);
			var MCAdsReady = new Event('MCAdsReady');

			// kicks off placement of ad scripts
			$(document).ready(function () {
				window.dispatchEvent(MCAdsReady);
			});
		})();
	}
}, 100);

exports.default = gptAdLoader;

},{}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _adSizes = require('./desktop/ad-sizes.js');

var _adSizes2 = _interopRequireDefault(_adSizes);

var _adSizes3 = require('./mobile/ad-sizes.js');

var _adSizes4 = _interopRequireDefault(_adSizes3);

var _utils = require('./utils.js');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var w = window;
var d = document;

w.MCAds = {};
w.MCAds.adsOnPage = [];

var MCAds = function () {
  function MCAds() {
    _classCallCheck(this, MCAds);
  }

  _createClass(MCAds, null, [{
    key: 'adOnPage',


    // Pushes ad id to global adsOnPage object
    value: function adOnPage(id) {
      w.MCAds.adsOnPage.push(id);
    }

    //checks to see if ad ID is already in the given array

  }, {
    key: 'containsAd',
    value: function containsAd(arr, value) {
      var contains = void 0;
      for (var i = 0; i < arr.length; i++) {
        if (arr[i].id === value) {
          contains = true;
        }
      }
      if (contains === true) {
        return true;
      } else return false;
    }
  }, {
    key: 'determineRandomFallback',
    value: function determineRandomFallback(cv, i, arr) {
      var screenWidth = this.getViewportWidth();
      var randomPct = Math.random(),
          adSizes = screenWidth < 768 ? _adSizes4.default : _adSizes2.default;

      if (cv.hasOwnProperty('pct') && randomPct >= cv.pct) {
        googletag.cmd.push(function () {
          googletag.defineSlot(adSizes[cv.adType].name, adSizes[cv.adType].sizes, adSizes[cv.adType].adId).addService(googletag.pubads());
        });
      } else if (cv.hasOwnProperty('pct') && randomPct <= cv.pct) {
        googletag.cmd.push(function () {
          googletag.defineSlot(adSizes[cv.fallback].name, adSizes[cv.fallback].sizes, adSizes[cv.fallback].adId).addService(googletag.pubads());
          //if fallback evaluates to true, then
          //overwrite current ad object with fallback values
          arr[i].adType = adSizes[cv.fallback].name;
          arr[i].script = adSizes[cv.fallback].script;
        });
      } else {
        googletag.cmd.push(function () {
          googletag.defineSlot(adSizes[cv.adType].name, adSizes[cv.adType].sizes, adSizes[cv.adType].adId).addService(googletag.pubads());
        });
      }
    }

    // handles GPT API
    // defines ad "slots" so they can be fired either immediately (for regular ads)
    // or fired on scroll (lazy loading ads)

  }, {
    key: 'defineAdSlots',
    value: function defineAdSlots(adsToLoad) {
      var regularAdsToLoad = adsToLoad.regularAds,
          lazyAdsToLoad = adsToLoad.lazyLoadingAds,
          fullurl = window.location != window.parent.location ? document.referrer : document.location,
          _self = this;

      regularAdsToLoad.map(function (cv, i, arr) {
        _self.determineRandomFallback(cv, i, arr);
      }, adsToLoad);
      lazyAdsToLoad.map(function (cv, i, arr) {
        _self.determineRandomFallback(cv, i, arr);
      }, adsToLoad);

      googletag.pubads().set('page_url', fullurl);
      googletag.pubads().setTargeting('urlvariable', fullurl);
      googletag.enableServices();

      return adsToLoad;
    }

    // debounced function that watches for ads scrolling into viewport
    // fires placeAd() when ad is visible

  }, {
    key: 'detectInViewport',
    value: function detectInViewport(adsToWatch) {
      var _self = this;
      var getCoords = _utils2.default.debounce(function () {
        adsToWatch.forEach(function (cv, i, arr) {
          var rect = cv.elm.getBoundingClientRect();

          if (rect.top >= 0 && rect.bottom <= w.innerHeight && rect.left >= 0 && rect.right <= w.innerWidth) {
            //if visible, fire ad placement
            _self.placeAd(arr[i].id, arr[i].script);
            //then remove from current array to not fire again
            arr.splice(i, 1);
          }
        });
      }, 250);
      w.addEventListener('scroll', getCoords);
    }

    // Get all desktop or mobile divs (ad spots) by data-ad-display attr

  }, {
    key: 'findAdsOnPage',
    value: function findAdsOnPage() {
      var screenWidth = this.getViewportWidth();
      var _self = this;
      var desktopOrMobile = screenWidth < 768 ? 'mobile' : 'desktop';
      var adsToFill = $('div[data-ad-display="' + desktopOrMobile + '"]'),
          adSizesToLoad = screenWidth < 768 ? _adSizes4.default : _adSizes2.default,
          lazyLoadingAds = [],
          regularAds = [];

      // Split up results into instantly served (regular) ads
      // and lazy loading ads, which only load when scrolled
      // into the viewport.
      // have to use $.each because its a jQuery object
      $.each(adsToFill, function () {
        for (var i = 0; i < this.attributes.length; i++) {
          if (this.attributes[i].name === 'data-ad-lazy' && !_self.containsAd(lazyLoadingAds, this.id)) {
            lazyLoadingAds.push({
              "id": this.id,
              "adType": this.attributes[i].nodeValue,
              "display": _self.findDisplayValue(this.attributes),
              "script": adSizesToLoad[this.attributes[i].nodeValue].script,
              "fallback": adSizesToLoad[this.attributes[i].nodeValue].fallback,
              "pct": adSizesToLoad[this.attributes[i].nodeValue].pct
            });
          }
        }

        for (var j = 0; j < this.attributes.length; j++) {
          if (this.attributes[j].name !== 'data-ad-lazy' && !_self.containsAd(lazyLoadingAds, this.id) && !_self.containsAd(regularAds, this.id)) {
            regularAds.push({
              "id": this.id,
              "pct": adSizesToLoad[this.dataset.adType]['pct'],
              "fallback": adSizesToLoad[this.dataset.adType]['fallback'],
              "adType": _self.findNodeValue(this.attributes),
              "display": _self.findDisplayValue(this.attributes),
              "script": adSizesToLoad[_self.findNodeValue(this.attributes)].script
            });
          }
        }
      });

      var allAdsToLoad = {
        regularAds: regularAds,
        lazyLoadingAds: lazyLoadingAds
      };
      return allAdsToLoad;
    }

    //gets display (desktop or mobile) value from html element's array of attributes

  }, {
    key: 'findDisplayValue',
    value: function findDisplayValue(arr, value) {
      for (var i = 0; i < arr.length; i++) {
        if (arr[i].name === 'data-ad-display') {
          return arr[i].nodeValue;
        }
      }
    }

    // Gets data-ad-type attr from html element's array of attributes

  }, {
    key: 'findNodeValue',
    value: function findNodeValue(arr, value) {
      for (var i = 0; i < arr.length; i++) {
        if (arr[i].name === 'data-ad-type') {
          return arr[i].nodeValue;
        }
      }
    }

    // self explanatory

  }, {
    key: 'getViewportWidth',
    value: function getViewportWidth() {
      return w.innerWidth;
    }

    // universal place ad function

  }, {
    key: 'placeAd',
    value: function placeAd(id, script) {
      if (id.indexOf('#') !== 0) {
        id = '#' + id;
      }
      $(id).append(script);
      $(id).css({ 'display': 'block' });
      this.adOnPage(id);
    }

    // fire ad placements for each non-lazy ad

  }, {
    key: 'placeRegularAds',
    value: function placeRegularAds(adList) {
      var _self = this;
      for (var i = 0; i < adList.length; i++) {
        _self.placeAd(adList[i].id, adList[i].script);
      }
    }

    // creates object of lazy loading ads
    // then passes them to detectInViewport()

  }, {
    key: 'setupLazyAds',
    value: function setupLazyAds(adList) {
      var adsToWatch = [];
      adList.forEach(function (cv) {
        var elm = d.getElementById(cv.id);
        adsToWatch.push({
          "elm": elm,
          "id": cv.id,
          "adType": cv.adType,
          "script": cv.script
        });
      });
      this.detectInViewport(adsToWatch);
    }
  }]);

  return MCAds;
}();

exports.default = MCAds;

},{"./desktop/ad-sizes.js":2,"./mobile/ad-sizes.js":5,"./utils.js":6}],5:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
var adSizes = {
	/* uncomment if we get rid of jumpyield.
 "morningconsult_1" : {
 	sizes: [300, 250],
 	script: "<!-- /135698611/morningconsult_1 --><div id='div-gpt-ad-1467116217077-0'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1467116217077-0'); });</script></div>",
 	name: '/135698611/morningconsult_1',
 	adId: 'div-gpt-ad-1467116217077-0'
 },
 "morningconsult_2" : {
 	sizes: [300, 250],
 	script: "<!-- /135698611/morningconsult_2 --><div id='div-gpt-ad-1467116217077-1'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1467116217077-1'); });</script></div>",
 	name: '/135698611/morningconsult_2',
 	adId: 'div-gpt-ad-1467116217077-1'
 },
 "morningconsult_3" : {
 	sizes: [300, 250],
 	script: "<!-- /135698611/morningconsult_3 --><div id='div-gpt-ad-1467116217077-2' style='height:250px; width:300px;'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1467116217077-2'); });</script></div>",
 	name: '/135698611/morningconsult_3',
 	adId: 'div-gpt-ad-1467116217077-2'
 },
 "morningconsult_4" : {
 	sizes: [300, 250],
 	script: "<!-- /135698611/morningconsult_4 --><div id='div-gpt-ad-1467116217077-3' style='height:250px; width:300px;'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1467116217077-3'); });</script></div>",
 	name: '/135698611/morningconsult_4',
 	adId: 'div-gpt-ad-1467116217077-3'
 },
 "morningconsult_5" : {
 	sizes: [300, 250],
 	script: "<!-- /135698611/morningconsult_5 --><div id='div-gpt-ad-1467116217077-4' style='height:250px; width:300px;'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1467116217077-4'); });</script></div>",
 	name: '/135698611/morningconsult_5',
 	adId: 'div-gpt-ad-1467116217077-4'
 },
 */
	"morningconsult_newsticker_s1_mobile_Control": {
		sizes: [300, 250],
		script: "<!-- /101659253/morningconsult_newsticker_mobile_s1_300x250_Control --><div id='div-gpt-ad-8401920148650-0' style='width:300px; height:250px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-8401920148650-0'); });</script></div>",
		name: "/101659253/morningconsult_newsticker_mobile_s1_300x250_Control",
		adId: "div-gpt-ad-8401920148650-0",
		pct: 0.95,
		fallback: "morningconsult_newsticker_s1_mobile_Floor1"
	},
	"morningconsult_newsticker_s1_mobile_Floor1": {
		sizes: [300, 250],
		script: "<!-- /101659253/morningconsult_newsticker_mobile_s1_300x250_Floor1 --><div id='div-gpt-ad-1083437300694-0' style='width:300px; height:250px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1083437300694-0'); });</script></div>",
		name: "/101659253/morningconsult_newsticker_mobile_s1_300x250_Floor1",
		adId: "div-gpt-ad-1083437300694-0"
	},
	"morningconsult_newsticker_s2_mobile_Control": {
		sizes: [300, 250],
		script: "<!-- /101659253/morningconsult_newsticker_mobile_s2_300x250_Control --><div id='div-gpt-ad-6538674496340-0' style='width:300px; height:250px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-6538674496340-0'); });</script></div>",
		name: "/101659253/morningconsult_newsticker_mobile_s2_300x250_Control",
		adId: "div-gpt-ad-6538674496340-0",
		pct: 0.95,
		fallback: "morningconsult_newsticker_s1_mobile_Floor1"
	},
	"morningconsult_newsticker_s2_mobile_Floor1": {
		sizes: [300, 250],
		script: "<!-- /101659253/morningconsult_newsticker_mobile_s2_300x250_Floor1 --><div id='div-gpt-ad-9678739223877-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-9678739223877-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_mobile_s2_300x250_Floor1',
		adId: 'div-gpt-ad-9678739223877-0'
	},
	"morningconsult_newsticker_s3_mobile_Control": {
		sizes: [300, 250],
		script: "<!-- /101659253/morningconsult_newsticker_s3_300x250_Control --><div id='div-gpt-ad-5849151333972-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-5849151333972-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_s3_300x250_Control',
		adId: 'div-gpt-ad-5849151333972-0',
		pct: 0.95,
		fallback: 'morningconsult_newsticker_s3_mobile_Floor1'
	},
	"morningconsult_newsticker_s3_mobile_Floor1": {
		sizes: [300, 250],
		script: "<!-- /101659253/morningconsult_newsticker_s3_300x250_Floor1 --><div id='div-gpt-ad-5291563273390-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-5291563273390-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_s3_300x250_Floor1',
		adId: 'div-gpt-ad-5291563273390-0'
	},
	"morningconsult_newsticker_s4_mobile_Control": {
		sizes: [300, 250],
		script: "<!-- /101659253/morningconsult_newsticker_s4_300x250_Control --><div id='div-gpt-ad-4583788211015-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-4583788211015-0'); });</script></div>",
		name: "/101659253/morningconsult_newsticker_s4_300x250_Control",
		adId: "div-gpt-ad-4583788211015-0",
		pct: 0.95,
		fallback: 'morningconsult_newsticker_s4_mobile_Floor1'
	},
	"morningconsult_newsticker_s4_mobile_Floor1": {
		sizes: [300, 250],
		script: "<!-- /101659253/morningconsult_newsticker_s4_300x250_Floor1 --><div id='div-gpt-ad-1546157942493-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1546157942493-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_s4_300x250_Floor1',
		adId: 'div-gpt-ad-1546157942493-0'
	},
	"morningconsult_newsticker_s5_mobile_Control": {
		sizes: [300, 250],
		script: "<!-- /101659253/morningconsult_newsticker_s5_300x250_Control --><div id='div-gpt-ad-5945178775929-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-5945178775929-0'); });</script></div>",
		name: "/101659253/morningconsult_newsticker_s5_300x250_Control",
		adId: "div-gpt-ad-5945178775929-0",
		pct: 0.95,
		fallback: 'morningconsult_newsticker_s5_mobile_Floor1'
	},
	"morningconsult_newsticker_s5_mobile_Floor1": {
		sizes: [300, 250],
		script: "<!-- /101659253/morningconsult_newsticker_s5_300x250_Floor1 --><div id='div-gpt-ad-6919211697350-0' style='width:300px;'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-6919211697350-0'); });</script></div>",
		name: '/101659253/morningconsult_newsticker_s5_300x250_Floor1',
		adId: 'div-gpt-ad-6919211697350-0'
	}
};

exports.default = adSizes;

},{}],6:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var _arguments = arguments;
var Utils = {

  debounce: function debounce(func, wait, immediate) {
    var timeout = void 0;
    return function () {
      var context = undefined,
          args = _arguments;
      var later = function later() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  }

};

exports.default = Utils;

},{}]},{},[1]);
