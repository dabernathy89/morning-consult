<?php
/**
 * Template name: About Page
 *
 * @package Morning Consult 2015
 */
?>

<?php get_header(); ?>


<?php
  $left_image = get_field('left_image');
  $right_image = get_field('right_image');
  $middle_image = get_field('middle_image');
?>
<section class="header">
  <div class="header-images">
    <div class="header-img-wrap" style="background-image: url(<?php echo get_template_directory_uri() . '/img/about-page/wall-street.png'; ?> );"></div>
    <div class="header-img-wrap" style="background-image: url(<?php echo get_template_directory_uri() . '/img/about-page/capitol.png'; ?> ); "></div>
    <div class="header-img-wrap" style="background-image: url(<?php echo get_template_directory_uri() . '/img/about-page/silicon-valley.png'; ?> );"></div>
  </div>
  <container class="container">
    <row class="row">
      <div class="col-xs-12">
        <div class="header-banner">
          <p class="larger-p">Morning Consult is a media and technology company</p>
          <p class="smaller-p">at the intersection of politics, policy, Wall Street, and business strategy</p>
        </div>
      </div>

    </row>
  </container>
</section>

<section class="what-we-do">
  <container class="container">
    <row class="row">
      <div class="col-xs-12 what-intro">We are committed to keeping the country's leaders informed. Our journalism seeks out the stories and news that are shaping industry and government. Our polling and market research analyzes trends and facts on the most important issues, and we use technology to bring powerful tools to the decision-makers who are shaping the future.</div>
      <div class="col-xs-12 col-md-4 what-description">
        <div class="img-container"><img src="<?php echo get_template_directory_uri() . '/img/about-page/media.png'; ?>" alt=""></div>
        <h1>Media</h1>
        <p>More than 200,000 government and industry leaders rely on Morning Consult's original reporting and morning email briefings to deliver comprehensive coverage of campaigns and Congress as well as the energy, finance, health, and tech industries.</p>
      </div>
      <div class="col-xs-12 col-md-4 what-description">
        <div class="img-container"><img src="<?php echo get_template_directory_uri() . '/img/about-page/research.png'; ?>" alt=""></div>
        <h1>Research</h1>
				<p>Morning Consult’s polling and market research division produces in-depth market research and public opinion data on everything from political campaigns to Fortune 500 companies. Morning Consult is the official polling partner of POLITICO, Vox, Fortune, and Bloomberg News.</p>
      </div>
      <div class="col-xs-12 col-md-4 what-description">
        <div class="img-container"><img src="<?php echo get_template_directory_uri() . '/img/about-page/intelligence.png'; ?>" alt=""></div>
        <h1>Intelligence</h1>
        <p>Tap into what the country is thinking, seeing, and saying with Morning Consult Intelligence. The database tracks polling, market research, media coverage and social analytics on the biggest companies, leaders, issues and ideas.</p>
      </div>
    </row>
  </container>
</section>

<section class="who-we-are">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h2 class="section-title">
          <?php the_field('about_page_who_we_are_section_title'); ?>
        </h2>
      </div>
    </div>

    <?php if( have_rows('about_page_employees') ): ?>

      <div class="row">
        <?php while ( have_rows('about_page_employees') ) : the_row(); ?>

          <div class="col-xs-6 col-sm-4 col-md-3 employee">
            <?php $employee = get_sub_field('employee'); ?>
            <?php $author_featured_image = get_field('author_archive_background_image', 'user_' . $employee['ID']); ?>
            <a href="<?php echo get_author_posts_url( $employee['ID'] ); ?>" target="_blank"> 
            <img src="<?php echo $author_featured_image['sizes']['thumbnail']; ?>">
            <p><strong>
              <?php the_author_meta('first_name', $employee['ID']); ?> <?php the_author_meta('last_name', $employee['ID']); ?>
            </strong></p></a
            <p><span><?php the_field('author_job_title', 'user_' . $employee['ID']); ?></span></p>
          </div>
        <?php endwhile; ?>
      </div>
    <?php endif; ?>
  </div>
</section>

<?php get_footer(); ?>
