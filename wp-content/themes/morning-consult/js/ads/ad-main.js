
import gptAdLoader from './loader.js'; // runs initial setup
import MCAds from './mc-ads.js';

window.$ = window.jQuery;

//fire ad only when gpt API loaded
window.addEventListener('MCAdsReady', () => {
	initAllAdsOnPage();
})

function initAllAdsOnPage() {
	let initialAdsToLoad = MCAds.findAdsOnPage();
	let updatedAds = MCAds.defineAdSlots(initialAdsToLoad);

	MCAds.placeRegularAds(updatedAds.regularAds);
	MCAds.setupLazyAds(updatedAds.lazyLoadingAds);

}
