<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; margin: 0px; padding: 0px; " xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta content="width=device-width" name="viewport" />
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
    <style type="text/css">
      img {
        outline:none;
        text-decoration:none;
        -ms-interpolation-mode: bicubic;
        max-width: 100%;
        float: left;
        clear: both;
        display: block;
      }    
      .content img {
        width: 100%;
      }
      img.inline {
        display: inline;
        clear: none;
        float: none;
      }
      p {
          color:#2B2B2B;
          font-family: helvetica, 'helvetica',arial,  'arial', sans-serif;
          line-height: 1.4;
      }
      h1 {
          color:#2B2B2B;
          font-family: helvetica, 'helvetica',arial,  'arial', sans-serif;
      }
      h2 {
          color:#2B2B2B;
          font-family: helvetica, 'helvetica',arial,  'arial', sans-serif;
      }
      h3 {
          color:#2B2B2B;
          font-family: helvetica, 'helvetica',arial,  'arial', sans-serif;
      }
      h4 {
          color:#2B2B2B;
          font-family: helvetica, 'helvetica',arial,  'arial', sans-serif;
      }
      .header.tech {
        background: #f39c12;
      }
      .header.health {
        background: #c63730;
      }
      .header.energy {
        background: #2980b9;
      }
      .header.finance {
        background: #51a03c;
      }
      .header.congress {
        background: #9236B8;
      }

      .tech a {
        color: #f39c12;
      }
      .health a {
        color: #c63730;
      }
      .energy a {
        color: #2980b9;
      }
      .finance a {
        color: #51a03c;
      }
      .congress a {
        color: #9236B8;
      }
      .social {
        text-align: center;
      }
      </style>
  </head>
  <body style="margin:0px; padding: 0;" class="{{mc_category_slug}}">
              <table class="header {{mc_category_slug}}" style="width:100%; padding-top:60px; padding-bottom:60px;" >
                <tr>
                    <td>
                        <table style="width:100%;max-width:580px; padding-left:10px; padding-right:10px;margin-left:auto; margin-right:auto;" >
                            <tr>
                                <td>
                                  <img id="header-logo" class="center logo" src="{{mc_theme_url}}/img/email/header_logo_{{mc_category_slug}}.png" style="width: 100%; height: auto;">                                
                                </td>
                            </tr>
                       </table>
                    </td>
                </tr>
              </table>
              <table style="width:100%;background:#fff; padding-top:10px; padding-bottom:10px;padding-left:10px;padding-right:10px;" >
                <tr>
                    <td>
                        <table class="content" style="width:100%;max-width:580px;margin-left:auto; margin-right:auto;" >
                            <tr>
                                <td>
                                    {{byline}}
                                    {{content}}
                                </td>
                            </tr>
                       </table>
                    </td>
                </tr>
              </table>
              <table style="width:100%;background:#f7f7f7; padding-top:60px; padding-bottom:60px;" >
                <tr>
                    <td>
                        <table style="width:100%;max-width:550px; padding-left:10px; padding-right:10px;margin-left:auto; margin-right:auto;" >
                            <tr>
                              <td>
                                <img id="footer-logo" class="center logo" src="{{mc_theme_url}}/img/email/footer_logo.png" style="width: 100%; height: auto;">        
                              </td>
                            </tr>
                            <tr>
                                <td>
                                  <br><br>
                                    <a href="{{site_url}}/subscribe/" style="color:#666666;text-decoration:none;"><div style="background:#f7f7f7;border:1px solid #666666;border-radius:4px;width:100%; max-width:580px;margin-left:auto;margin-right:auto;padding-top:15px;padding-bottom:15px;text-align:center;">SUBSCRIBE</div></a>
                                </td>
                            </tr>
                            <tr>
                              <td class="social">
                                  <br><br>
                                  <a class="facebook" href="https://www.facebook.com/pages/The-Morning-Consult/183517831660100"><img class="inline" src="{{mc_theme_url}}/img/email/facebook.png"></a>&nbsp;&nbsp;
                                  <a class="twitter" href="https://twitter.com/MorningConsult"><img class="inline" src="{{mc_theme_url}}/img/email/twitter.png"></a>&nbsp;&nbsp;&nbsp;
                                  <a class="linkedin" href="https://www.linkedin.com/company/the-morning-consult"><img class="inline" src="{{mc_theme_url}}/img/email/linkedin.png"></a>&nbsp;&nbsp;&nbsp;                              
                              </td>
                            </tr>
                       </table>
                    </td>
                </tr>
              </table>
  </body>
</html>