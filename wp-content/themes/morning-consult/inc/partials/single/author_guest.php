        <?php
            $author_headshot = get_field('author_headshot', $author->ID);
        ?>

        <div class="author">
            <?php if ($author_headshot) : ?>
                <div class="author-photo">
                    <img src="<?php echo $author_headshot['sizes']['thumbnail']; ?>" alt="Author Photo">
                </div>
            <?php endif; ?>
            <div class="author-info">
                <h4 class="name"><?php echo $author->first_name; ?> <?php echo $author->last_name; ?></h4>
                <?php
                    $author_twitter = strtolower(get_field( 'author_twitter_handle', $author->ID));
                    if ($author_twitter && strpos($author_twitter, 'twitter.com') !== false) :
                        echo '<a class="twitter" target="_blank" href="' . $author_twitter . '">@' . substr($author_twitter, strlen('twitter.com/') + strpos($author_twitter, 'twitter.com/')) . '</a>';
                    elseif ($author_twitter) :
                        echo '<a class="twitter" target="_blank" href="http://www.twitter.com/' . $author_twitter . '">@' . $author_twitter . '</a>';
                    endif;
                ?>
                <p><?php echo $author->description; ?></p>
                <a class="author-link" href="/author/<?php echo $author->user_nicename; ?>">See all posts by <?php echo $author->first_name; ?></a>
            </div>
        </div>