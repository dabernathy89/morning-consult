<?php
/**
 * @package Morning Consult 2015
 */
?>

<?php
    global $post;
    $author_id = get_the_author_meta('ID');
    $post_for_ad = $post;
    $all_authors = get_coauthors();
?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-8 col-sm-offset-2">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

					<div class="entry-meta">
						<span class="author vcard">
						<?php foreach ($all_authors as $authorkey => $author) : ?>
							<?php include "inc/partials/single/meta_author.php"; ?>
						<?php endforeach; ?>
<!--							<a class="url fn n" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>">
									<?php echo get_the_author_meta( 'first_name' ); ?> <?php echo get_the_author_meta( 'last_name' ); ?>
							</a>-->
						</span>&nbsp;&nbsp;<span class="separator">|</span>&nbsp;&nbsp;
						<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
							<time class="entry-date published updated" datetime="<?php echo get_the_date( 'c' ); ?>">
								<?php the_date(); ?>
							</time>
						</a>
					</div><!-- .entry-meta -->
				</div>
			</div>
		</div>
	</header><!-- .entry-header -->

	<div class="entry-content">
			<div class="container">
					<div class="row">
							<div class="col-xs-12 col-sm-8 col-sm-offset-2">
									<?php the_content(); ?>
							</div>
					</div>
			</div>
	</div><!-- .entry-content -->

	<footer class="single-footer">
			<div class="container">
					<div class="col-xs-12 col-sm-8 col-sm-offset-2">
							<div class="row share">
									<div class="col-sm-6">
											<a class="tweet-this" href="http://twitter.com/share?url=<?php echo urlencode(get_the_permalink()); ?>&text=<?php the_title(); ?>" title="Twitter" target="_blank">
													Tweet This
											</a>
									</div>
									<div class="col-sm-6">
											<a class="share-this" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(get_the_permalink()); ?>" title="Facebook" target="_blank">
													Share This
											</a>
									</div>
							</div>
					</div>
			</div>

			<?php
					if ($all_authors) :
							echo '<div class="authors">';
							foreach ($all_authors as $author) {
									$author_type = get_class($author);
									if ($author_type === "WP_User") {
											include "inc/partials/single/author_user.php";
									} elseif ($author_type === "stdClass") {
											include "inc/partials/single/author_guest.php";
									}
							}
							echo '</div>';
					endif;
			?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
