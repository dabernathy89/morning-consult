<?php
/**
 * The featured story on a section home page.
 *
 * @package Morning Consult 2015
 */

    $section_cat = get_field('section_home_category');

    $section_featured_post_id = get_field('section_home_featured_post');

    $section_home_featured_args = array(
        'posts_per_page' => '1',
        'post_type' => 'post',
    );

    if ($section_featured_post_id) {
        $section_home_featured_args['post__in'] = array($section_featured_post_id);
    }

    if ($section_cat) {
        $section_home_featured_args['cat'] = $section_cat->term_id;
    }

    $section_home_featured = new WP_Query($section_home_featured_args);

    if( $section_home_featured->have_posts() ): while( $section_home_featured->have_posts() ): $section_home_featured->the_post();

    $section_home_featured_post_id = get_the_ID();

    ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class('excerpt featured clearfix'); ?>>
        <header class="entry-header">
          <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
        </header><!-- .entry-header -->

        <div class="featured-image">
          <a href="<?php echo get_permalink(); ?>">
            <?php the_post_thumbnail('large'); ?>
          </a>
        </div>

        <div class="meta-primary clearfix">
          <span class="author vcard">
            <em>
              <?php $all_authors = get_coauthors(); ?>
              By <?php foreach ($all_authors as $authorkey => $author) : ?>
                <?php include get_stylesheet_directory() . "/inc/partials/single/meta_author.php"; ?>
              <?php endforeach; ?>
            </em>
          </span>
					<br/>
          <div class="category-tags">
            <?php morning_consult_category_links_square(); ?>
          </div>
        </div>

        <div class="entry-content">
					<?php echo mc_custom_excerpt( get_the_ID(), 30 ); ?>
        </div>
      </article>

    <?php endwhile; endif;
    wp_reset_postdata(); ?>
