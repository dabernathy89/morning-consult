<?php if( have_rows('policy_landing_topics') ): ?>
	<div class="policy-landing__topics">
  	<?php while( have_rows('policy_landing_topics') ): the_row();
  		$icon = get_sub_field('policy_landing_single_topic_icon');
      $title = get_sub_field('policy_landing_single_topic_title');
  	?>

  		<section class="policy-landing__single-topic policy-landing__single-topic--<?php echo $title; ?>">

				<div class="policy-landing__single-topic-row row">
					<div class="col-md-3">
						<div class="policy-landing__single-topic-col-inner">

						<div class="policy-landing__single-topic-icon-wrap">
			  			<img class="policy-landing__single-topic-icon" src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt'] ?>" />
						</div>

						</div>
					</div>
					<div class="col-md-9">
						<div class="policy-landing__single-topic-col-inner">

			  		  <h2 class="policy-landing__single-topic-title">
			          <?php echo $title; ?>
			        </h2>

			        <?php if( have_rows('policy_landing_single_topic_links') ): ?>
			        	<ul class="policy-landing__single-topic-links">
			          	<?php while( have_rows('policy_landing_single_topic_links') ): the_row();
										$link_tag = get_sub_field('policy_landing_link_tag');
										$link_post = get_sub_field('policy_landing_link');
			          	?>

			              <li class="policy-landing__single-topic-link">
			                <a href="<?php echo get_permalink($link_post); ?>">
												<span class="policy-landing__single-topic-link-tag">
													<?php echo $link_tag; ?>
												</span>
			                  <?php echo $link_post->post_title; ?>
			                </a>
			              </li>

			            <?php endwhile; ?>
			          </ul><!-- .policy-landing-single-topic-links -->
			        <?php endif; ?>

						</div>
					</div>
				</div>

  		</section>

  	<?php endwhile; ?>
	</div>
<?php endif; ?>
