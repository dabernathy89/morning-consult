<?php
/**
 * Morning Consult ESP Integrations Create Campaign Inboxfirst
 * @version 0.1.0
 * @package Morning Consult ESP Integrations
 */

class MCESPI_Create_Campaign_Inboxfirst implements MCESPI_Create_Campaign_Interface {
    /**
     * Push an HTML email up to InboxFirst
     *
     * @since  0.1.0
     * @return void
     */
    public function create_campaign( $post_id, $sender_email, $sender_name, $email_content, $meta_prefix ) {
        $api_key = get_field('brief_inboxfirst_api_key','option');
        $org_id = get_field('brief_inboxfirst_org_id','option');
        $list_id = get_field($meta_prefix . 'inboxfirst_list_id',$post_id);

        if (!$api_key || !$org_id) {
            wp_send_json_error( 'API credentials not set' );
        }

        if (!$list_id) {
            wp_send_json_error( 'No List ID specified' );
        }

        $http_args = array(
            'headers' => array(
                'Authorization' => 'Basic ' . base64_encode( $org_id . ':' . $api_key )
            ),
            'httpversion' => '1.1',
            'body' => json_encode(array(
                'campaign' => array(
                    'name' => substr(get_the_title($post_id),0,150),
                    'campaign_contents_attributes' => array(
                        array(
                            'content_attributes' => array(
                                'html' => $email_content,
                                'format' => 'html',
                                'subject' => substr(get_the_title($post_id),0,150)
                            )
                        )
                    ),
                    'dispatch_attributes' => array(
                        'from_name' => $sender_name,
                        'from_email' => $sender_email
                    )
                )
            ))
        );

        $response = wp_remote_post(
            "http://if.inboxfirst.com/ga/api/v2/mailing_lists/" . $list_id . "/campaigns",
            $http_args
        );

        if (!is_wp_error( $response )) {
            $response_body = json_decode($response['body']);
            if ($response_body->success) {
                wp_send_json_success( $response_body->data->id );
            } else {
                $error_message = $response_body->error_message;
                $error_message .= " (error code " . $response_body->error_code . ")";
                wp_send_json_error( $error_message );
            }
        } else {
            wp_send_json_error( $response->get_error_message() );
        }
    }
}
